# 通过配置自动插入模拟数据 version 1.4.1

## A、通过 addConnector 设定 connector 目前支持 mongodb、elasticsearch

- mongo：

```
1、addConnector(type="MONGO",arg="mongodb://mongos:27017/")
2、{"connector":{
      "MONGO":"mongodb://mongos:27017/"
      }
  }
```

- elasticsearch ：

```
1、addConnector(type="ES",arg=["es1:9200","es2:9200","es3:9200"])
2、{"connector":{
      "ES":["es1:9200","es2:9200","es3:9200"]
      }
  }
```

- 分别调用`saveMONGO` 和`saveES` 存储生成的数据，refresh 参数默认 False，如果为 True 则重新创建
- mongo 须指定 table 的形式为"database/collection"，而 es 指定 table 的形式为"index"。

## B、通过 addIterator 设定 iterator 迭代器。

- iterator 可以设定 rowScope（默认为 False），表示是否为每个文档重新迭代
- iterator 可以设定 cycle（默认为 False），表示迭代器是否是不断循环，非循环情况迭代器结束返回 None
- 通过 func="self.next('<name>')"来引用,如果返回的是一个字典，还可以直接设定引用，例如 self.next('id')['id']
- 语法如下：

```
1、addIterator(name,it,cycle=False,rowScope=False)
2、{"iterator":{"<name>":{"data":"<it>","cycle":true/false,"rowScope":true/false}}}
```

- iterator 可以以下几种模式：
  > 1.  iter([1,2,3])、iter(range(2,100))、map 或 filter 等 iterable 对象 用 iter 包装起来的数组或序列

```
addIterator("name1",iter(["apple","pear","banana","cherry",...],True))
addIterator("name2",map(lambda x:x+":hello",["apple","pear","banana","cherry",...]))
```

> 2.  self.faker.timestr_series(start,end,interval,format),start 到 end,步长为 interval 的日期序列，format 模式认为 yyyy-mm-dd 格式。start、end 可以是`today`、日期类型、日期型字符串(如：2019-03-05)、或以今天为基准的字串（如`+5d`、`-3d`、`+15h`、`-10m`等）。必须保证 start<=end；interval 为`+1d`、`+24h`等

```
addIterator("name2", self.faker.timestr_series("-365d","today","+1d),cycle=True)
```

> 3.  可以通过 mongo 查询定义一个迭代器。

```
addIterator("id",demo.connector["MONGO"]["test"]["demo"].find({},{"_id":0,"id":1})})
```

## C、通过 addResource 设定 resource 资源。

- resource 的数据可以是任何类型（数组、数字、字串、结构体等）。
- `data`,`url`,`fetch`,`file`必须指定其中一种，且只有一种。如果同时指定则按 data、url、fetch、file 顺序逐级覆盖
- 为方便引用，通常将 data 的数据定义为一个带有结构的数组。这样就可以通过`self.resource[<name>]`来应用这个资源了
- resource 仅在配置 config 时运行一次，所以是相对静态的资源，动态资源可以配置为一个隐形的 field，参见 config 部分
- 语法如下：

```
addResource(name,data=None,url=None,fetch=None,file=None)
```

- 一般情况下定义

```
1、
addResource("name1",data={"a":1,"b":2})
addResource("name2",data=[("a",1),("b",2)])
addResource("name3",data="self.faker.random_element[OrderedDict([self.faker.random_int(1,5),0.25]),
                           OrderedDict([self.random.int(5,7),0.50]),
                           OrderedDict([self.random.int(7,10),0.25]),
                          ]")
2、{"resource":{
      "name1":{"data":{"a":1,"b":2}}
      "name2":{"data":[("a",1),("b",2)]}
      "name3":"self.faker.random_element[OrderedDict([self.faker.random_int(1,5),0.25]),
                           OrderedDict([self.random.int(5,7),0.50]),
                           OrderedDict([self.random.int(7,10),0.25]),
                          ]"
    }
  }
```

- 可以定义`url`，系统将从 url 中获取数据并保存到 data 中。此时 data 由 url 所取得的数据替代，例如：

```
1、addResource("name",url="http://icfs:8081/icfs/bafk43jqbea2zyadn77himocjqamaw7nrgwaquetxtyqxcka45indm3dtvdljo")
2、{"resource":{"name":{"url":"http://icfs:8081/icfs/bafk43jqbea2zyadn77himocjqamaw7nrgwaquetxtyqxcka45indm3dtvdljo"}}}
```

- 也可以定义`fetch`，系统将从指定数据库中获取数据并保存到 data 中。此时 data 由数据库所取得的数据替代，例如：

```
1、addResource("food",fetch="demo1.connector["MONGO"]["food"]["food"].find({},{"_id":0})}")
2、{"resource":{"food":{"fetch":"demo1.connector["MONGO"]["food"]["food"].find({},{"_id":0})}"}}}
```

- 也可以定义`file`，系统将从指定本地文件系统获取数据并保存到 data 中。此时 data 由指定文件所取得的数据替代，例如：

```
假设food.json文件的内容为：["苹果","香蕉","梨"]
1、addResource("food",file="food.json")
 则：self.resource["food"]=["苹果","香蕉","梨"]
```

## D、通过 addStage 设定不同的阶段

- .addStage(name,rows,interval,total,callback)
  > 1.name 是一个表示阶段的字符串,如"step1"
  > 2.rows 表示要生成记录数,可以是一个确定的数值，也可以是一个表示区间的二元组或数组
  > 3.interval 是一个[min,max]的数组，表示随意 min 到 max 秒的等待时间(单位：秒）。也可以是一个确的数值（同 2）
  > 4.total 在 interval 有定义时有效，表示最多执行多次。如果有定义 interval，而没有定义 total，则 total 默认为 10 次
  > 5.callback 表示一组数据生成后要调用的语句字串，通常是存储数据或调用存储数据的 API。如："demo.saveES('step1',{'table':'test/demo','refresh':True})"，"demo.saveByAPI('step2','http://service.endpoint')" 6.如果采用 saveByAPI 模式，则模拟出来的数据实际是一个 json 格式的参数数组。数据的每一条参数将作为 postAPI 的 body 传入后台，并由后台生成数据或执行其他逻辑 7.支持保存到本地文件 saveFile(fileName,fieldName=None)。如果 fieldName 为 None 则保存为 json 的 dumps 格式,如果 fieldName 为结果数据的某个字段名则按行保存该字段的内容
  > 8.current 是动态的 loop 值。由于 self.stage 是一个 stage 的数组，所以可以使用 self.stage[1]['current']来代表第 2 个 stage 的当前轮次。
- 由于 callback 时一个动态的语句，所以包括存储的表名，是否刷新都可以时动态的设定

```
  .addStage("step1",rows=[10,20],callback="demo.saveMONGO('step1',{'table':'test/demo','refresh':True})"
  .addConfig({"key":"id","num":"self.faker.int(1,10)"})
  .addStage("step2",rows=2,interval=(3,10),total=3,\
              callback="demo.saveMONGO('step2',{'table':'test/demo','refresh':False})"
  .addConfig({"key":"id","num":"self.faker.int(5,15)"})
  .addStage("step3",rows=15,interval=5,\
              callback="demo.savebyAPI('step3','http://service.endpoint')"
  以下每10秒生成10条记录，并保存在当前轮次的文件中，由于total轮次为5次，所以将生成test1.txt ～ test5.txt
  .addStage("step4",rows=10,interval=10,total=5,callback="demo.saveFile(f\"test{self.stage[1]['current']}.txt\",'entities')")
```

## E、通过 addConfig 设定所生成模拟的字段类型和取值

- addConfig 是模拟器最主要的函数，通过这个函数（可以级连）
- addConfig 的第一个参数必须指定 stage 的 name，第二个参数时配置的 dict
- key:字段的名称，必须指定。
  > 1. 允许通过`func=`定义一个动态值,例如:

```
 .addConfig("step1",{"key":"func=self.next('riqi')","depend":"_diet1,_sport1",\
             "func":"{'diet':_['_diet1'],'sport':_['_sport1']}"})
```

> 2. 以`_`开头的字段不会作为结果，通常会定义这样的字段用于 depend 引用，例如:

```
 .addConfig("step1",{"key":"_diet1","type":"array","nums":[1,20],\
  "func":"{'engery':self.faker.random_int(10,100),'fat':self.faker.random_int(10,100),'etime':self.faker.time('%H:%M:%S')}"}) \
 .addConfig("step1",{"key":"_sport1","type":"array","nums":[1,3],\
  "func":"{'name':self.faker.random_beta(['快跑','走','骑自行车','快走','打篮球','踢足球']),'engery':self.faker.random_int(10,100),'etime':self.faker.time('%H:%M:%S')}"}) \
 .addConfig("step1",{"key":"func=self.next('riqi')","depend":"_diet1,_sport1",\
             "func":"{'diet':_['_diet1'],'sport':_['_sport1']}"})
```

- show: 为调试方便，'\_'开头的字段也可以通过定义 show 为 true 来强制显示

```
  .addConfig("step1",{"key":"_hide","show":true,"func":"'it must be show!'"})
```

- note:对该字段的文字说明
- type:可以是 text,keyword,date,array,geo_point 三种类型,主要用于给 es 定义 mapping

  > 1. text 表明为为可全文检索，keyword 表明不可全文检索
  > 2. date 通常格式为 yyyy-mm-dd
  > 3. geo_point 为地理格式`{"lat":***,"lon":***}`
  > 4. array 表明字段是一个数组或列表，由`'nums':[start,end]`定义数组的随机条目数区间。默认情况下随机生成 0-5 条记录，如果 nums 为单一数值 n 则表明随机生成 0-n 条数据。这里有一个小技巧：tick 变量可用来表示当前的循环索引。nums 允许为字符串表达式，从而动态设定。

- force: 见 func 字句之说明 9

- func:生成数据的脚本或函数
  > 1.  可以是`self.row['<field>']`或`_['<field>']`代表某个已经生成的 field 的值。如果这个 field 在所定义的 key 之后定义，则需要手动指定 key 的 depend 属性，并把这个<field>名字放在 depend 数组中。在这里,符号*是 self.row 的简写形式。注意，这与字段的前导*不是一个概念。
  > 2.  可以是`self.rows[<idx>]["<field>"]`代表本 stage 中（不包括本行记录）第 idx 条数据的 field 的值。如果要代表所有行，可以 self.rows+[_]来指代，它与 self.rows+[self.row]的含义是一样的。注意，self.rows 是 dict 的数组，而 self.row 或\_是一个 dict 类型。
  > 3.  可以是`self.docs["<stageID>"][idx]["<field>"]`代表 stageID 阶段中第 idx 条数据的 field 的值
  > 4.  可以是`self.next("<iterator_name>")`代表某个 iterator 的序列（见 B）
  > 5.  可以是`self.resource("<resource_name>")`代表某个 resource 的值
  > 6.  可以是`self.faker.<function>(...)` 代表任何可用的 faker 函数
  > 7.  可以是`self.httpGet(<url>)` 代表调用 url 所指向的后台获取数据,请确保后台返回的数据是标准 json 格式
  > 8.  可以是以上各种表示的可运行的任何组合
  >     例如：

```
.addConfig(<stage_name>,{"key":"sex","type":"keyword","depend":"id","func":"{0:'女',1:'男'}[self.faker.getSex(_['id'])]"}) \
.addConfig(<stage_name>,{"key":"name","type":"text","depend":"sex","func":"self.faker.nname(1) if _['sex']=='男' else self.faker.nname(0)"}) \
.addConfig(<stage_name>,{"key":"shenf","type":"keyword","depend":"id","func":"self.faker.getSF(_['id'])"}) \
.addConfig(<stage_name>,{"key":"address","type":"text","func":"self.faker.street_address()"}) \
.addConfig(<stage_name>,{"key":"email","type":"text","func":"self.faker.email()"}) \
.addConfig(<stage_name>,{"key":"comp","type":"text","func":"self.faker.company()"}) \
.addConfig(<stage_name>,{"key":"salary","type":"float","func":"self.faker.random_float(1000.00,19999.99)"}) \
.addConfig(<stage_name>,{"key":"birthday","type":"date","depend":"id","func":"self.faker.getBirthday(_['id'])"}) \
.addConfig(<stage_name>,{"key":"id","func":"self.faker.ssn()"}) \
.addConfig(<stage_name>,{"key":"location","type":"geo_point","func":"{'lat':self.faker.geo_lat(),'lon':self.faker.geo_lon()}"}) \
.addConfig(<stage_name>,{"key":"addtional","func":"{'seq':self.next('seq'),'alpha':self.next('alpha')}"}) \
```

> 9. 由于 func 基本是一个字符串语句，所以
>    （注:从版本 1.4.0 开始，"func":字面量 的方式【如:"func":"abc","func":123.23,"func":[1,2,3],"func":{"a":1,"b":2}】，从而方便配置。但是这种方法将消耗一定的运算量)
>    (注:从版本 1.4.0 开始，增加“force”字串，默认为 false。如果该字段设置"force":true,则为严格模式，以下检查将成立并报告错误原因。这在调试错误是非常有用)例如：

```
  {
    "key":"testForce",
    "force":true,
    "func":"self.faker.noThisMethod()"
  }
  因为该字段设置了force=true，所以系统不会让字段的值为"self.faker.noThisMethod()",而是汇报self.faker.noThisMethod函数不存在。这样就可以快速查找语法存在的错误。
```

. 当表示字符串的时候,正确："func":"'hello world'",错误: "func":"hello world" （版本 1.4.0 以上，允许这种表示方式）
但是直接"func":字符串 的表示方式是有风险和歧义的，要避免使用 python 类名，比如:

```
   {
     "key":"mode",
     "func":"json"  //由于json是内置的包名，所以执行后mode其实为json model而不是“json”字符串
   }
   正确的方式应该是1:
   {
     "key":"mode",
     "func":"'json'"  //表示json确实是一个字符串
   }
   正确的方式应该是2:
   {
     "key":"mode",
     "func":"json1"  //避免与系统json包或函数重名，让mode=“json1”
   }
```

类似 json 的关键字还有`self`、`_`、`faker`、`datetime`、`sys`、`math`、`random`等
. 当表示数组的时候,正确: "func":"[1,3,5]" , 错误: "func":[1,2,3] （版本 1.4.0 以上，允许这种表示方式）

. 当表示数值的时候,正确: "func":"123" , "func":"123.12", 错误: "func":123 , "func":"'123'" （版本 1.4.0 以上，允许这种表示方式）
. 当表示 dict 的时候,正确: "func":"{'a':1,'b':'abc'}" , 错误: "func":{"a":1,"b":"abc"} （版本 1.4.0 以上，允许这种表示方式）

. 由于配置文件是严格 json 格式,每个 key/value 不能有`'`包裹，只能由`"`包裹。所以很常用的表示方法是：
"func":"_['_abc']",这表示取'\_abc'这个字段的当前行的值。注意第一个`_`表示当前行，第二个`_`表示_abc这个字段是隐藏字段。这两个`_`是没有关联关系的。

. 实际应用中经常需要进行字符串的替换，使用 f 函数可以很好的处理，花括号`{}`中的内容是要替换的数据，但是要注意的是嵌套引号的处理，同时 f 函数的双引号做转义。例如: "func":"httpGet(f\"http://api.endpoint:18088?name={_['name']}\")",这条语句表示该字段执行一个httpGet的取数，其中url的name参数取自本行name字段的值

. 实际使用中还经常会用到多个数据的 map 映射，注意一下几点:
A. 遵循以下 list/map/lambda 语法: "func":"list(map(lambda item1,item2,...:item1+item2 , items1,items2,...))"
B. 由于 map 的闭包结构，所以`self`和`_`变量需要在 map 中赋值制定才可以引用，否则会报类似【执行 func 字句错误:("name '\_' is not defined",)】的错误.例如:

```
  ...
  {"key":"multiData",
   "type":"array",
   "nums":"[5,10]",
   "func":"self.faker.random_int(1,100)"
  },
  {"key":"columnA",
   "func":"self.faker.random_int(100,200)"
  },
  {
    "key":"mapData",
    "force":true,
    "func":"list(map(lambda x,self=self,_=_:f\"{x} - {self.faker.random_int(200,300)} - {_['columnA']} \" ,_['multiData']))"
  }
  ...
```

C、 使用 map、filter 等语句定义的字段本质是一个 array ，但是不要在这种情况下定义 type='array'。否则系统会按 array 的规则重复生成该字段下的多条数据。因为 type 为 array 是告诉后台数据库字段是列表类型，后台如果是 API 则由 API 逻辑处理。

D、再看一个高级的例子：

```
   {
     "resource":{
       "functions":{"data":["random.random()","self.faker.random_element(['a','b','c'])","lambda x,y:x+y"]},
       "another":{"data":"lambda self,start,end,mode:self.faker.random_number2(start,end,mode)"}
     },
     "config":{
       "table":[
         {
           "key":"col1",   //col1="random.random()" ，拿到函数的字符串
           "force":true,
           "func":"self.resource['functions'][0]"
         },
         {
           "key":"col2",  //col2="a" ，随机得到的"a"，拿到的是函数字符串必须通过eval来执行
           "force":false,
           "func":"eval(self.resource['functions'][1])"
         },
         {
           "key":"col3",  //col3=13 ，定义了一个lambda函数，带有两个参数。通过eval得到这个函数并传入5，8两个参数
           "force":true,
           "func":"eval(self.resource['functions'][2])(5,8)"
         },
         {
           "key":"col4",  //col4="self.resource['functions'][2](5,8)" ，拿到lambda函数的字符串，但没有eval构成函数而只是一个字符串。执行错误后由于force默认位false，所以不会弹出错误，而是把这个字符串直接返回
           "func":"self.resource['functions'][2](5,8)"
         },
         {
           "key":"col5", // functions数组越界，由于force=true所以将会报错
           "force":true,
           "func":"self.resource['functions'][3]"
         },
         {
           "key":"col6", // another并非一个字符串，执行后是一个匿名函数，这个函数需要4个参数，注意第一个参数是self。
           "force":true,
           "func":"self.resource['another'](self,0,100,'any')"
         }
       ]
     }
   }
```

- file: "func"没有指定的情况下，可以指定 file 参数用来从一个脚本文件中读取并执行代码。这种情况通常适用于逻辑非常复杂，需要多条语句来完成；或者需要定义一个函数甚至类到一个字段的情况。

  > 1.  "func"如果被指定，"file"字句将被忽略
  > 2.  "file"所指定的脚本文件，通常后缀名为 code.py 以方便识别。如 abc.code.py
  > 3.  脚本中可以使用`self`，`_`,`S`,`R`,`F`,`X`，与 func 字句的作用域相同,但在 config 设置里面才可以导入`_`
  > 4.  默认脚本不返回数据，如果需要返回需要在脚本中定义`__return__`变量
  >     举例:

  ```
    {
      "key":"_test1",
      "file":"file1.code.py"
    },
    {
      "key":"test2",
      "func":"_['_test1'].fun2(3,4)"
    },
    {
      "key":"test3",
      "func":"_['_test1'].fun3(3,4)"
    }

    file1.code的脚本为:
    class A:
      def __init__(self):
          self.version = "1.4.1"
          self.current =0
      def fun1(self):
          self.current +=1
          return self.current
      def fun2(self,x,y):
          return x+y
      def fun3(self,x,y):
          return x*y
    __return__=A()

    执行的结果：
      test1为class A的一个实例,是由file1.code.py返回的__return__
      test2为 7
      test3为 12
  ```

- maybe: 0-1 的百分数，决定该字段出现的概率。例如 0.5 意味有一半概率会出
  > 1. maybe 也可以是一个计算语句，根据条件计算可能出现的概率
  >    .addConfig(<stageNname>,{"key":"\_subtype1", "maybe":"1 if \*['type']=='diet' else 0","type":"keyword","depend":"type","func":"self.resource['foodlist'][self.faker.random_int(0,1)]"})
  > 2. maybe 可以直接指定概率数值，如 0.5，0 或 1
  > 3. maybe 也可以指定 true/false
  > 4. 2 和 3 的方法可以利用 maybe 字段作为调试，让 maybe=false 来起到暂时注释这个字段的作用
- depend: 逗号分隔的字段名称。
  > 1. 须确保逗号分隔的每个字段有定义
  > 2. depend 暂不支持级连，所以如果有级连依赖的情况，请确保被依赖的字段较早被 addConfig
  > 3. 通过`_["<field>"]`或`self.row["<filed>"]`的方式在 func 中引用相应字段，并确保该字段被定义在被引用的字段的 depend 属性中。
  > 4. 在某种特殊的场景下可以通过引用 self.row[-1]["<field>"]来引用前一条的对应数据项。但是这种方式下，由于第一条是没有前一条数据所以通常不能直接这样引用，而会采用类似这样的方式指定初始值，如'abc'：self.row[-1]['<field>'] if self.idx!=0 else 'abc'
- list : 生成的数据 list 数组内数据。会忽略 func 的定义，而直接从 list 随机取值。【注意：可能在后续版本弃用】

## F、通过 generate 生成模拟数据

- 开始生成模拟数据
- 根据 self.stage 的设定，分阶段插入模拟指定数量的模拟数据
- 一组 stage 生成的轮次由 repeat 指定，默认为 1
- 新一轮生成时，由 retain 参数确定是否在内存保留之前的模拟数据，默认为 False
- 如果需要保留每一轮生成的数据，在`self.docs["<stageID>"]`代表每个 stage 中历史生成的数据，将占用非常大的内存空间，所以默认 retain=False 以确保每个 stage 组轮次刷新
- 语法如下：

```
generate(repeat=1,retain=False,debug=True)
```

## G、系统依赖 faker，但同时扩展了 faker 以满足更多实际情况

- 需要安装 faker
- 系统还需要安装的库:elasticsearch，pymongo,flask,xeger,lunar_python,cn2an,eprogress

```
pip3 install faker
pip3 install elasticsearch
pip3 install pymongo
pip3 isntall flask
pip3 install xeger
pip3 install lunar_python
pip3 install cn2an
pip3 install eprogress
```

- faker 的函数参看 faker 文档`https://github.com/joke2k/faker/`
- faker 的 unique 用法
  . faker.unique 代表所运行的 faker 函数返回不重复的数据，如果已取的所有可能的数据，在执行则会报错。
  . faker.unique.clear() 可以清空之前的 unique 标记，重新判断
  . 例如 faker.unique.random_int(3,5) ,第一次执行可能为 4，第二次可能为 5，第三次只能为为 3（因为不能重复）
  第四次执行系统会报重复取值的错误，但是在第四次执行之前，调用 faker.unique.clear(),就可以重新执行了
- 常用的函数
  Faker(locale='zh_CN')

> `random_int(min,max,step)`:根据 step（默认为 1）或取 min 到 max 的整数

> `random_element(elements=('a','b','c'))`:随机获取 elements 的数据。如果把 elements 转换为 OrderedDict(item,rate)的类型,则可以根据 rate 的权重来取值。OrderedDict 是 python3 的 collections 的模块，系统已经通过 from collections import OrderedDict 先期导入了，可以直接调用。如：

```
OrderedDict([["苹果",3],["香蕉",4],["樱桃",5]]),这个对象作为elements参数传入，苹果出现几率将为(3/(3+4+5))=25%,香蕉的几率为(4/(3+4+5))=33.33%,樱桃的几率为(5/(3+4+5))=41.67%
```

> `random_sample(elements,length)`:随机取的 elements 数组中的 length 个元素。如果把 elements 转换为 OrderedDict(item,rate)的类型,则可以根据 rate 的权重来取值。

> `random_letters(length=16)`: 生成 length 长度的 ascii 字母（a-z，A-Z）

> `random_number(digits=None,fixlen=False)`:生成 digits 位的整数，fixlen 为 True 精确位数，fixlen 为 False 不超过 digits 位数

> `bothify(text="## ??",letters="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ“)`从 letters 中选取填充到 text 模版中，#为随机数字，？为随机 letters 字符。 例如:

```
 >>>faker.bothify(text="## ??? ==>???"，letters='abcde')
 >>>可能返回: 95 ccc ==>aae
```

> `image_url()`：随机图片的 URL 地址，所生成的 url 是可以取得网络图片的

> `ipv4()`：随机 IP4 地址

> `ipv6()`：随机 IP6 地址

> `sentence()`：随机生成一句话

> `future_date()`：未来日期

> `future_datetime()`：未来时间

> `past_date()`：随机生成已经过去的日期

> `past_datetime()`：随机生成已经过去的时间

> `date()`：随机日期

> `hex_color()`：随机 HEX 颜色

> `safe_hex_color()`：随机安全 HEX 颜色

> `lexify()`：替换所有问号（“？”）带有随机字母的事件。

> `job()`：随机职位

> `pyfloat(left_digits=5 #生成的整数位数,

        right_digits=2 #生成的小数位数,
        positive=True #是否只有正数)`

- 新增的函数

  > 1、cname(type==None) 根据 type 生成男名、女名

  > faker 中文名中 faker_male 和 faker_female 存在问题。调用 cname 默认 type=None 不限男女，type=1 男名、type=0 女名

  > 2、datestr_between(start,end,format='%Y-%m-%d') 生成 start 到 end 之间的日期字符串，格式由 format 指定

  > start、end 可以是`today`、日期类型、日期型字符串(如：2019-03-05)、或以今天为基准的字串（如`+5d`、`-3d`、`+15h`、`-10m`等）。必须保证 start<=end

  > 3、timestr_between(start,end,format='%H:%M:%S') 生成 start 到 end 之间的时间字符串，格式由 format 指定
  > start，end 可以为一个 tuple 类型
  > start、end 可以是`today`、日期时间类型、时间型字符串(如：03:30、03:30:10)、或以今天为基准的字串（如`+5d`、`-3d`、`+15h`、`-10m`等）。必须保证 start<=end

  > 4、random_float(start,end) 生成 start 到 end 之间的一个浮点数
  > 浮点数的小数位数有 start 和 end 小数精度的最大值决定。但是.00 或.000 只保留 1 位小数，应为它们与.0 是等价的。

  > 5、random_beta(arr,alpha=None,beta=None) 根据 alpha、beta 参数所确定 beta 分布来选择 arr 中的值
  > 常用的场景是 arr 列表采用正态分布采样，通常中间位置的值最多采样。alpha，beta 在没有设定的情况下默认为 arr 的长度
  > alpha/(alpha+beta)为 beta 分布的先验概率，具体参见https://www.zhihu.com/question/30269898

  > 6、geo_lat,get_lon 分别获得纬度和经度

  > 7、getBirthday、getSex、getSF 通过解析身份证号，分别获得出生日期、性别和省份

  > 8、timestr_series(start,end,interval='+1d',format='%Y-%m-%d') 获得 start 到 end，间隔为 interval，格式为 format 的迭代器
  > 通常给 addIterator 使用。参见 B.2

  > 9、random_element2(element1,elemate2)增强选取列表的函数。
  > 接受三种传参

        # 1. elements 为[[A,r1],[B,r2],[C,r3]...],其中r1,r2,r3,...为相应权重
        # 2. element 为[A,B,C...] , element2为[r1,r2,r3...]
        # 3. element为二维tuple ([A,B,C...],[r1,r2,r3...])

  > 9.1 random_element3(element,rate) 更常用的随机选取函数。需要确保 rate 数组长度与 element 长度一致，且 rate 中的数据和为 1。例如:

  ```
    # 一下语句4出现的概率为70%，其他数值的概率均为10%
    random_element3([1,2,3,4],[0.1,0.1,0.1,0.7])
  ```

  > 10、random_int_range(source,rate=None)随机整数区间值，rate=None 时等比的随机选取 source 中的随机区间的值。但当设定 rate 时，系统将根据 rate 对应概率选取相应的区间的随机整数值。要求 rate 长度与 source 长度相同，并且 rate 的值总和为 1(版本 1.4.0 以上不要求 rate 总和为 1)。例如 random_int_range([[2,4],[4,5],[7,10]],[0.4,0.1,0.5])，source 和 rate 也可在组成一个二元组参数，参见 G.9

  > 11、random_float_range(source,rate=None)随机浮点数区间值，rate=None 时等比的随机选取 source 中的随机区间的值。但当设定 rate 时，系统将根据 rate 对应概率选取相应的区间的随机浮点数值。要求 rate 长度与 source 长度相同，并且 rate 的值总和为 1(版本 1.4.0 以上不要求 rate 总和为 1)。例如 random_float_range([[2.1,4.3],[5.5,7.8]],[0.8,0.2])，source 和 rate 也可在组成一个二元组参数，参见 G.9

  > 12、geo_address(lat,lng=None)根据纬度、经度获取中文地址

  > 13、geo_location(address)根据中文地址反向获取纬度和经度元组

  > 14、geo_random_address(location,radius=0.001)根据（纬度，经度）元组和 radius 半径获取随机偏移的中文地址

  > 15、geo_random_location(address,radius=0.001)根据中文地址和 radius 半径，反向获取纬度、经度元组

  > 16、cn2num(cnStr) 将中文的数字字符串转换为阿拉伯数字，支持负数、支持浮点数

  > 17、num2cn(num,mode="low") 将数字转换为中文，mode 取值 low 为小写中文，up 为大写中文，rmb 为大写人民币，默认 mode=low。支持负数、支持浮点数

  > 18、random_percent(acc=None,mode="an") 生成随机百分比字符串(如 25%,33.34%等)，默认精度为 None 则随机保留 0 ～ 4 位，可取值范围 0-4，默认类型为阿拉伯数。如果中文形式 mode="cn",任意形式 mode="any"

  > 19、random_frac(fenzi=50,fenmu=100,mode="an") 生成随机分数字符串(如 2/3,25/39 等)，默认分子 1 ～ 50，默认分母 1 ～ 100，默认类型阿拉伯数。分母必须大于分子。如果中文形式 mode="cn",任意形式 mode="any"

  > 20、random_number2(self, start, end, mode="an") 生成随机整数或小数，精度为 0 到 start/end 的最大精度之间随机，默认类型为阿拉伯数。如果中文形式 mode="cn",任意形式 mode="any"
  > 21、random_number2 的升级版，根据 rangeRate 的比例选取 rangeArr 数组，在根据选定的 range 执行 rangdom_number2 操作

        sample:  random_number3([[0,20],[20.01,29.99],[29.99,100]],[0.1,0.8,0.1],"an") 系统将有80%的数据在20.01～29.99之间，其他区段各10%。精度由29.99指定最高小数后2位，仅生成数值形式

  > 22、num2cn(num, mode="low") # 数值转中文，mode 支持"low","up","rmb"，默认 mode=low
  > 23、cn2num(cnStr,mode="smart") #中文转数值，mode 支持"strict","normal","smart"，默认 mode=smart
  > 24、maybe(str,rate=0.5) #根据指定 rate 参数决定是否返回 s，不命中的情况返回空串
  > 25、randomMap(data, groupCnt=5, itemCnt=5, groupRate=[0.4, 0.4, 0.1, 0.07, 0.03], itemRate=[0.4, 0.4, 0.1, 0.07, 0.03])
  > #1. 将传入的 data 随机映射到随机的 group 组中，每组随机个 item。其中 group 不超过 groupCnt，每组 item 不超过 itemcnt 个。
  > #2. groupRate 指定 group 可能个数的分布，itemRate 指定 item 可能个数的分布

  ```
    randomMap(['a','b','c','d','e'],3,2,[0.3,0.4,0.3],[0.3,0.7])
    可能返回:
    {
     "group0":["a"],
     "group1":["b","c"]
    }
    也可能返回:
    {
     "group0":["a"]
    }
  ```

  > 26、random_select(lst,lstRate,min=None,max=None) # 根据lst数组对应的lstRate数组选取可能的元素，返回一个新的数组。同时要确保返回的数组至少有min个数据，min为None则表示不限制最少值，因此可能返回空数组
        # lstRate对应为1的数据为一定被选取的数据，而为0的则一定不被选去
        # 如果min=None则最小出现的个数为rate为1的个数,如果min小于rate为1的个数则min也为rate为1的个数
        # 如果max=None则最大出现的个数为lst数组的个数
  > 27、random_split(lst, groupNum,pos=None) # 根据lst数组随机分解到groupNum组中。结果是一个元素为groupNum的二维数组，每个元素是一个原数组lst的子集，可能为空数组。
        # pos是可选的二维数组，标识lst数组中每个对应的元素可以放置的位置。比如[[0,2],[1],[0,1,2]],表明第一个元素可以放在0，2组中，第二个元素只能放在1组中，第三个元素可以放在0，1，2中
        # pos如果指定，其长度应与lst长度一致;pos中元素如果为None或为空数组则表示不限定放在哪个组
  > 28、decode(data,list1,list2) #将data的值从list1映射到list2

- 其他相关函数

  > self.merge(\*d) 将传入的多个 dict 参数合并为一个 dict
  > self.convert(baseUnit, fromValue,fromUnit,toUnit=None) 将 fromValue 数值从 fromUnit 转换到 toUnit 数值，如果 toUnit 为 None,则转换到 baseUnit 单位
  > self.mark_entity(entityList,entityName,mode="json") 生成标注实体的结构,mode 可以为 json 格式和 normal 格式,和 txt 格式

  ```
    self.mark_entity(['abcd','12.34','xyz'],['e1','_e2','e3'])
    将返回标注实体的json结构：
    {
      "text":"abcd12.34xyz"
      "entities":[
          {"start":0,"end":4,"entity":"e1","value":"abcd"},
          {"start":9,"end":12,"entity":"e3","value":"xyz"},
      ]
    }
    注意：由于_e2是带前导_的，所以返回值将过滤掉这部分内容

    self.mark_entity(['abcd','12.34','xyz'],['e1','_e2','e3'],mode='normal')
    将返回标注实体的normal结构：
    {
      "text":"abcd12.34xyz"
      "entities":[
          "a B-e1","b I-e1","c I-e1","1 O","2 O",". O","3 O","4 O","x B-e3","y I-e3","z I-e3"
      ]
    }
  ```

  > self.getTimestamp() 获取当前的时间戳

  > self.xeger(patternStr) 根据给定 patternStr 随机生成对应的字符串(等同与 self.faker.xeger)
  > 部分相关函数例子:

  # self.xeger('(个|盘|块|碗|大杯|小杯|杯|根|包|盒|支|粒|片)?') 生成任意一个单位字串或一个空值

  # self.xeger('[\\d]{2,4}') 生成 2-4 位的数字

  # self.xeger("self.xeger('(跑)步\\\\1 了两公里')") 生成 "跑步跑了 2 公里"

  > self.lunar 阴历的软件包（等同于 self.faker.lunar,用法参见http://6tail.cn/calendar/api.html)
  > 部分相关函数例子,'20220101'是阳历日期:

  # self.faker.getLunar('20220101').getFestivals() 获得节日数组，如春节、中秋等，可以为空

  # self.faker.getLunar('20220101').getOtherFestivals() 获得其他节日数组，如寒衣节、下元节等，可以为空

  # self.faker.getLunar('20220101').getJieQi() 获得节气，如清明节

  # self.faker.getLunar('20220101').getDayYi() 获得宜数组，如裁衣、动土等，可以为空

  # self.faker.getLunar('20220101').getDayJi() 获得忌数组，如祭祀、祈福等，可以为空

  # self.faker.getLunar('20220101').getSolar() 转化为 solar 对象

  > self.solar 阳历的软件包 (等同于 self.faker.solar,用法参见http://6tail.cn/calendar/api.html)
  > 部分相关函数例子,'20220101'是阳历日期:

  # self.faker.getSolar('20220101').getXingZuo() 获得星座字符串，如水瓶

  # self.faker.getSolar('20220101').getFestivals() 获得节日数组,如国庆节等，可以为空

  # self.faker.getSolar('20220101').getOtherFestivals() 获得其他节日数组,如名人诞辰日等，可以为空

  # self.faker.getSolar('20220101').next(-3).toString() 获得前 3 天的日期字串

  # self.faker.getSolar('20220101').nextWorkday(2).toString() 获得后两个工作日的日期字串

  # self.faker.getSolar('20220101').getLunar() 转化为 lunar 对象

## H、show 调试

- generate 后可以通过 show 语句查看`self.docs`的数据
- rows 默认 10 条文档
- showAll 代表是否查看带`_`的字段，默认不查看。\_字段通常为隐藏字段，如果 showAll 关闭的情况下仍要调试部分隐藏字段，可以在设置字段的时候定义 shou 为 true。参见 config show
- indent 代表是否以 json 缩进方式查看，默认不以 json 带缩进方式

```

show(stage_name,rows=10,showAll=False,indent=False) #例如
show("genID",rows=5,showAll=True,indent=True)

```

## I、通过集成的 conf，并 Demo 创建时传入，系统将自动解析各配置的组件

```

conf={"connector":{
"ES":["es1:9200","es2:9200","es3:9200"],
"MONGO":"mongodb://mongos:27017/"
},
"resource":{
"r1":{"data":"[1,2,3]"},
"r2":{"url":"http://icfs:8081/icfs/bafk43jqbea2zyadn77himocjqamaw7nrgwaquetxtyqxcka45indm3dtvdljo"}
},
"iterator":{
"i1":{"data":"iter([1,2,3])","cycle":True},
"i2":{"data":"iter('.'.join('abcdefghijklmnopqrstuvwxyz').split('.'))","rowScope":True}
},
"stage":{
"s1":{"rows":[1,5],"total":2,"callback":"print(len(self.docs['s1']),'rows complete!')"},
"s2":{"rows":[10,15],"callback":"self.show('s2')"}
},
"config":{
"s1":[
{"key":"sex","type":"keyword","func":"self.faker.random_element(['男','女'])"},
{"key":"\_id","func":"self.faker.ssn()"}
],
"s2":[
{"key":"id","func":"self.faker.pystr(12,12)"},
{"key":"i1","func":"self.next('i1')"},
{"key":"i2","type":"array","func":"self.next('i2')"},
{"key":"r1","func":"self.faker.random_beta(self.resource['r1'])"},
{"key":"r2","func":"self.resource['r2']['b']['b2']"},
{"key":"tags","type":"array","nums":[3,5],"func":"self.faker.random_float(100.01,999.99)"}
]
}
}
demo=Demo(conf)
demo.generate(1)

```

## J、通过命令行（CLI）生成数据

- 语法： python3 demoData.py config.json
- config.json 的配置与 I 示例中 conf 类似，但需要注意的是 config.json 文件是一个 json 格式的文本，需要遵循 json 的规范例如 true/false 的表示。原则是在字串中定义的脚本要符合 python 的标准，bool 类型应该表示为 True/False；而 json 定义的部分则要符合 json 的标准，例如:

```

     ...
      "iterator":{
          "i1":{"data":"iter([1,2,3])","cycle":true},
          "i2":{"data":"iter('.'.join('abcdefghijklmnopqrstuvwxyz').split('.'))","rowScope":true}
      },
      ...
      "config":{
          "s1":[
            {"key":"male","type":"keyword","func":"self.faker.random_element(['True','False'])"},
            {"key":"_id","func":"self.faker.ssn()"}
            {"key":"}
          ],
      }
      ...

```

## K、通过 HTTP 服务生成数据

- 语法： python3 demoData.py
- 系统将在本机 5000 端口启动服务
- 调用方式：
  1.  发送 json 格式的配置文件获取生成的数据，并根据 stage 定义执行数据存储 [POST] http://0.0.0.0:5000/generate
  2.  查看配置文件样例 【GET】 http://0.0.0.0:5000/sample1/config
  3.  查看样例所生成的数据 【GET】 http://0.0.0.0:5000/sample1/generate

```

```

## L、简写配置的关键字

可以用一些大写字母代替常用的语句，但在 lambda 函数内部使用时必须传入参数。
R 替代 self.resource
S 替代 self.stage
F 替代 self.faker
X 替代 self.xrger
