'''
A、支持录入和查询语句的NLP数据标注生成
B、将句子分为主短语和子短语两部分，一个句子由一个主短语和多个子短语组成
C、input主短语由act和name组成，标识为：意图#act、意图#name。(例如：diet#act，sport#name等)。意图包含diet、sport、buy、behavior、sign。
D、input子短语由act、value、name组成，不同的子短语根据实际情况有所不同，总体上有amount、pay、duration、energy、distance、stime、etime、setime、person、place等
  D1、amount字句，表示数量的短语，比如"是3个","3盘","数量为3碗"等。
      "是3个":解析为amount_act="是",amount_value="3个"
      "3盘"解析为amount_value="3盘"
      "数量为3碗"解析为amount_act="为",amount_value="3碗",amount_name="数量"
  D2、pay、duration、energy、distance，分别表示钱、时间、热量、距离的短语例如:
      "赚了3块5:解析为pay_act=赚,pay_value="3块5"
      "花了2小时20分钟的时间":解析为duration_act=花,duration_value=2小时20分钟,duration_name=时间
      "2公里":解析为distance_value="2公里" ，通常在表示距离运动时出现
      "消耗了3千卡":解析为energy_act="消耗",energy_value="3千卡"
  D3、stime，etime，setime分别带别开始时间，截止时间和带开始截止的时间
      1、档时间时是多个的（比如昨天和今天），将标识在stime_name中，比如stime_name="昨天||今天"
      2、单一时间和起始时间的标识，一般解析为stime_name，例如"在上个礼拜","从早上8点开始","这个月","今天的下午3点半","近三天"
      3、截止时间的标识解析为etime_value，例如"直到晚上9点","至下午","～四季度"
      4、明确连写的时间标识为setime_value，例如"上午3点-5点","上周一开始到这周3"
      5、由于时间的多样性，解析时有可能把stime解析为setime或，所以后台要分别处理
  D4、person、place分别标识人物和地点
      1、目前为试验性的。解析为person_value 、place_value
      2、name和act通常省略
  D5、字句可以有一个特殊的标识来表示分别E或总共T，例如标识为amount_E,pay_T等
E、query主句短语由act和name组成，标识为：query#意图#act、query#意图#name。(例如：query#diet#act，query#sport#name等)。意图包含diet、sport、buy、behavior、sign。
F、query字短语由act、value、name组成，不同的子短语根据实际情况有所不同，总体上由amount、pay、duration、energy、distance、stime、etime、setime、person、place
   还有若干用于查询的标识，包括agg、groupby等。此外还有metricAmount字句代表查询的指标
   F1、字句标识与D类似，但前导会增加query。如queryAmount，queryPay等
   F2、agg标识聚合语句，例如：
      "最大多少卡":解析为 queryMetricAmount_value="多少卡",queryAgg_value="最大"
      "平均不超过300克":解析为 queryAmount_value="不超过300克",queryAgg_value="平均"
   F3、groupby标识分组语句，例如:
      "平均每天吃了多少热量":解析为 queryAgg_value="平均",queryGroupby_value="每天",query#diet#act="吃",queryEnergy_value="多少",queryEnergyAmount_name="热量"
   F4、metricAmount字句标识指标，例如:
      "吃面包吃了多少蛋白质和脂肪":解析为 query#diet#act="吃",query#diet#name="面包",queryMetricAmount_value="多少",queryMetricAmount_name="蛋白质||脂肪"
   F5、queryCommand帮助标识语句是一个query查询语句，例如：
      "请查一下这个礼拜吃了多少面包":解析为queryCommand_value="请查一下",queryStime_name="这个礼拜",query#diet#act="吃",queryAmount_value="多少",query#diet#name="面包"
   F6、compare标识比较语句，例如:
      "昨天比今天多吃了多少蛋糕":解析为queryStime_name="昨天||今天",compare="多",query#diet#act="吃",queryAmount_value="多少",query#diet#name="蛋糕"   
'''


from time import sleep


class Base:
    def __init__(self, this, R, F, X):
        self.current = 0
        self.this = this
        self.R = R
        self.F = F
        self.X = X
        self.init()

    def init(self):
        self.timerUnit = "(秒钟|秒|分钟|小时|钟头|天|日|号|周|星期|礼拜|月|季度|年|刻钟|刻|字|点)"
        self.cnYear = lambda self: self.X('((大前年|前年|去年|今年|本年|明年|后年|大后年)|#1年|#2年)') \
            .replace('#1', self.F.random_number2(1900, 2300, 'anyn')) \
            .replace('#2', self.F.random_number2(0, 99, 'anyn'))
        self.cnQuarter = lambda self: self.X('((这季度|这个季度|本季度|上季度|上个季度|下季度|下个季度)|第?#1个?季度)') \
            .replace('#1', self.F.random_number2(1, 4, 'any'))
        self.cnMonth = lambda self: self.X('((大前(个半?)?月|前(个半?)?月|上(个半?)?月|这(个半?)?月|本月|当月|下(个半?)?月|下下(个半?)?月)|第?#1个?月份?|#1月份?)') \
            .replace('#1', self.F.random_number2(1, 12, 'any'))
        self.cnWeek = lambda self: self.X(
            '((这周|本周|这星期|这礼拜|这个礼拜|上周|上礼拜|上星期|上个礼拜|上个星期|上上周|上上个星期|上上个礼拜|下周|下星期|下个周|下个星期|下礼拜|下个礼拜|下下周|下下个星期|下下个礼拜)([1-6一二三四五六日末])?|(周|星期|礼拜)[1-6一二三四五六日末])')
        self.cnDay = lambda self: self.X('((大前天|前天|昨天|昨|今天|当天|今|明天|明|后天|大后天)|#1[日号])') \
            .replace('#1', self.F.random_number2(1, 31, 'any'))
        self.cnHolidays = lambda self: self.X(
            '(元旦|新年|妇女节|三八节|38节|植树节|清明节|劳动节|五一节|儿童节|61儿童节|六一节|教师节|光棍节|双十一|双11|情人节|愚人节|万圣节|圣诞|春节|大年初一|元宵节|上元节|端午节|七夕|中秋|仲秋|中元节|重阳节|除夕|大年夜|除夕夜|小年|小年夜|小寒|大寒|立春|雨水|惊蛰|春分|清明|谷雨|小满|芒种|小暑|大暑|立秋|处暑|白露|秋分|寒露|立冬|小雪|大雪|冬至)')
        self.cnSegs = lambda self: self.X('(((凌晨|早晨|清晨|早上|清早|一早|一大早|一大清早|早|上午|中午|下午|晚上|傍晚|黄昏|晚|夜晚|子夜|午夜|半夜|现在|刚才|刚刚|子时|丑时|寅时|卯时|辰时|巳时|午时|申时|酉时|戌时|亥时)(#1[点]钟?(#2分|半)?|#2分半?|#3:#4)?)|#1[点]钟?(#2分|半)?)') \
            .replace('#1', self.F.random_number2(0, 24, 'any')) \
            .replace('#2', self.F.random_number2(0, 60, 'any')) \
            .replace('#3', self.F.random_number2(0, 24, 'an')) \
            .replace('#4', self.F.random_number2(0, 60, 'an'))
        self.dietAct = "(吃|喝)|(不|不(要|应|许|准|可以|可|能|应该|会|将|将会|可能|可能会))?(吃|喝|抽|吸|饮用|服用|服|进食|摄入|品|尝|品尝|食用|咬|嚼|吞下|咽下|啃|磕|饮|包含|含|含有|有)"
        self.payAct = "(花|用)|((花|付|用|支付|花费|浪费|费|耗|开销)(去|出|出去)?|(去|出|出去))"
        self.spendAct = "(花|用)|((花|用|花费|浪费|费|耗)(去|出|出去)?|用时|费时|计时|耗时)"
        self.energyAct = "(消耗|吸收|摄入|包含|含|含有|有)"
        self.signAct = "((测|称)?量?)"
        self.be = "(是|有|为)"
        # 是否需要把actLe设为可能为空？
        self.actLe = "(了|了的是|的是)?"
        self.actHua = "(花|用|浪费|费|耗|耗费)?(在|于)"
        self.actHuaAd = "(之|的)?(上|下|中|里|当中)(面)?"
        self.actDe = "(的)?"
        self.actFromPre = "(从|自从|打|在)?"
        self.actFromPer = "(开始|起)?"
        self.actToPre = "(-|——|直?至|直?到|一直到|一直至|～)"
        self.actToPer = "(结束|为止|止)?"
        self.actTo = "(-|直?至|直?到|一直到|一直至|～)"
        self.actAt = "(在)"
        self.der = "(的?)"
        self.punch = "(，|,|.|。|、)"
        self.breaker = "(。|,|，|\?|？|(嗯|啊)[～]{0,2}|，?这个，?|,?那个,?|,?大概|,?大约|,?也许|,?可能|,?好像|,?还有|,?另外|,然后|，而且)"
        self.decorate = "((那个|这个|差不多)｜(七分熟|5分熟|八分熟|过热|烤焦|高蛋白|低脂)|(不|很|特|超|有点|特别|太|不怎么|有够|够|有点)(高|低|矮|胖|瘦|大|小|长|短|酸|甜|苦|辣|咸|冷|热|坏|好|贵|便宜|难吃|美味|香甜|可以|那个|不错|愉快|开心|难|有价值|没意思|学问深广|学富五车|满腹经纶|才高八斗|学贯中西|博学多才|狼吞虎咽))(的|得|地)"
        self.link = "(,?以及|,?还有|和|、|,|，|。|及|加)"
        self.each = "(分别|各自|每个|各)"
        self.total = "(总共|总的|一共|合计|总计|一起是|共计|累计|合起来|加起来|汇总|共)"
        self.pos = "(,?在|,?于)"

        self.more = "((>|>=|大于|多于|超过|大于等于|高于|强于)#1)"
        self.less = "((<|<=|小于|少于|不足|不到|小于等于|低于|弱于|控制在)#1)"
        self.between = "((在|介于)#1(和|与|-|~|到|至)#2(之间)?)"
        self.equal = "((=|==|在|是|为|有|等于)#1(以内|之内|以下|之下|以外|之外|以上|之上))"
        self.the_and = "(且|并且)"
        self.the_or = "(或|或者)"
        self.the_not = "((不要|不应该|不应|不可以|不可|不|并非|除非|没有|没)?)"

        self.agg = "(平均|最大|最小|最多|最少|最高|最低|汇总|总共|总的|合计|总计|一共|共计|累计|合起来|加起来|共)"
        self.groupby = "(每天|每个?月|每(周|个?星期|个?礼拜)[1234567一二三四五末六日]?|每个?季度|每半?年|每个?小时)"
        self.that = "(那天|那个?月|那个?(周|星期|礼拜)|那个?季度|那半?年|那个?小时)"
        self.howmany = "(多少|几|哪(几|些))"

        self.relation = "(而且|并且|且|又?或|又?或者)"
        self.compare1 = "(比|较|比较|相较|相比较|相比|相比|对比)(于)?"
        self.compare2 = "(同比|同期|等比|环比)?(多(出|于)?|少(于)?|减少|增加|高(出|过)?|降低|超(过|出)?|重|轻|大|小|涨|跌|延?长|缩?短)(了)?"
        self.queryCommand = "((请|可否|是否|帮忙|帮我)?(告诉我|告知|问|查|列出|查询|记录|给出|检索|查看|看|找|比|对比|比较)(出|一下|下)?|情况|吗|怎么样|啥|什么|如何|什么时候|什么时间|何时)"

        self.planCommand = "不?(计划|打算|准备|拟|要|想要|还要|还想要|将|将要|想|希望|提醒|记得|开始|可以|可)"
        self.feelCommand = "(感觉|奇怪|发现|感到|发觉|觉得)"

        self.nutrition = ["营养(素|成分)?", "脂肪", "蛋白质?", "碳水(化合?物)?", "盐(份)?|钠", "膳食纤维", "糖份?", "胆固醇", "(单|多)?饱和脂肪", "纤维素", "(a|b|阿尔法|贝塔)胡萝卜素",
                          "生物素", "胆碱", "维生素", "[Bb]族", "维生素[abcdekABCDEK][1-9]?", "[vV][abcdekABCDEK][1-9]?", "视黄素|视黄醇", "硫胺素", "核黄素", "盐酸", "泛酸", "叶酸", "钙", "磷", "钾", "锰", "铁", "锌", "铜", "碘", "硒", "镁",
                          ]
        self.body = "((((左|右|上|下|里|外|前|后)?半?[边部]?)|整个)(头|眼|眼睛|鼻子|嘴|嘴巴|舌头|喉咙|耳|耳朵|牙|大牙|门牙|肩|肩膀|臂|胳膊|手|拇指|小拇指|手背|手心|指关节|胸|乳房|背|腰|心|心脏|肺|胃|肠|肝|肋骨|肚子|肘关节|腕|屁股|阴部|腿|大腿|膝盖|小腿|脚|脚踝|脚背|脚趾))"
        self.fuzzyDuration = "(一|整个|一整个|整整一|大半个|半个|一个)(早上|下午|中午|晚上|晚|天|夜|宿)|一上午|一下午|一晚上|一会儿?|一阵儿?|片刻|一直"
        self._anyTimeList()
        self._anyNumList()

    def addCurrent(self):
        self.current += 1
        return self.current

    def anyTagFun(self):
        return self.X("(((早|午|下午|晚)(餐|饭|茶))|零食|加餐|夜宵|宵夜|饮料|饮品|中餐|西餐|蔬菜|保健品|肉类|主食)|运动|活动|锻炼|服装|办公用品|差旅费|日常开销")

    def _anyTimeList(self):
        # 时间生成器数组
        self.anyTimeList = [
            [
                lambda self:self.X(
                    f"(再)?(过|待)?(#1(个半?)?{self.timerUnit})(零?#2{self.timerUnit}(半?))?(之|以)?(前|后)")
                .replace('#1', self.F.random_number2(0.1, 9.99, 'any'))
                .replace('#2', self.F.random_number2(0.1, 9.99, 'any')),
                lambda self:self.X(
                    '((#Y的?)?((#Q的?)?(#M的?))?(((#H)(那天)?|(#D))的?(#T)?)?)|(#T)?|(#W的?)?(#T)?')
                .replace('#W', self.cnWeek(self))
                .replace('#H', self.cnHolidays(self))
                .replace('#Y', self.cnYear(self))
                .replace('#Q', self.cnQuarter(self))
                .replace('#M', self.cnMonth(self))
                .replace('#T', self.cnSegs(self))
                .replace('#D', self.cnDay(self)),
                lambda self:self.X("(#Y(-|/|\.)#M(-|/|\.)#D)")
                .replace('#Y', self.X("[12][0-9][0-9][0-9]"))
                .replace('#M', self.X("[0][1-9]|[1][0-2]"))
                .replace('#D', self.X("[0][1-9]|[1-2][0-9]|[3][0-1]")),
                lambda self:self.X(
                    f"((上|过去|最近这|最近|近|接下来这|接下来|这|下)个?的?(#1)个?(半)?({self.timerUnit}))")
                .replace('#1', self.F.random_number2(0.1, 9.99, 'any')),
                lambda self:self.F.random_element(
                    ['3点多', '5点左右', '9点多', '下午6点左右']),
                lambda self:self.F.random_element(
                    ['3-5点', '14~17点', '9到12点', '下午2至7点']),
            ],
            [0.3, 0.3, 0.2, 0.2, 0.05, 0.05]
        ]

    def anyTimeFun(self):
        # 时间生成器
        fun = self.F.random_element3(self.anyTimeList[0], self.anyTimeList[1])
        return fun(self)

    def _anyNumList(self):
        # 数字生成器数组
        self.anyNumList = [
            [
                lambda self:self.F.random_frac(9, 10, 'any'),
                lambda self:self.F.random_percent(2, 'any'),
                lambda self:self.F.random_number2(0.0001, 9999.9999, 'any'),
                lambda self:self.F.random_number2(0, 9999, 'any'),
                lambda self:self.X(
                    "半|1|2|3|4|5|6|7|8|9|一|二|三|四|五|六|七|八|九|十")
            ],
            [0.05, 0.05, 0.2, 0.2, 0.5]
        ]

    def anyNumFun(self):
        # 数字生成器
        fun = self.F.random_element3(
            self.anyNumList[0], self.anyNumList[1])
        return fun(self)

    def anyNumRangeFun(self):
        return f"{self.anyNumFun()}{self.X('(-|至|到|～)')}{self.anyNumFun()}"

    def payNumFun(self, n=None):
        # n为1或None时返回单个数据，超过1时返回数组
        if (n == None or n == 0):
            n = 1
        res = []
        for i in range(n):
            # 金钱生成器
            temp1 = self.X('((#1[块元](#2[毛角](#3[分])?)?)|(#2[毛角](#3[分])?)|(#3[分]))(半|一|二|三|四|五|六|七|八|九|1|2|3|4|5|6|7|8|9)?').replace(
                '#1', self.anyNumFun()).replace('#2', self.anyNumFun()).replace('#3', self.anyNumFun())
            temp2 = "#1万|#1千|#1百".replace('#1', self.anyNumFun())
            temp3 = self.F.random_element3([temp1, temp2], [0.95, 0.05])
            res.append(temp3)
        if len(res) < 2:
            return res[0]
        else:
            return res

    def queryPayNumFun(self):
        # 数字单位生成器
        return self.X(f"({self.howmany}(块|元|大洋|毛|角|分))|((多少)(钱))")

    def conditionPayNumFun(self):
        return self.X(f"{self.the_not}({self.equal}|{self.more}|{self.less}|{self.between})") \
            .replace('#1', self.payNumFun())  \
            .replace('#2', self.payNumFun())

    def payNumRangeFun(self):
        return f"{self.payNumFun()}{self.X('(-|至|到|～)')}{self.payNumFun()}"

    def durationFun(self, n=None):
        # 时段生成器
        if (n == None or n == 0):
            n = 1
        res = []
        for i in range(n):
            if self.F.maybe(True, 0.05):
                temp = self.X(self.fuzzyDuration)
                res.append(temp)
            else:
                temp = self.X(
                    '((#1(个|个半)?(小时|时辰|钟头)((零|又)?#2(分|分钟|刻钟))?((零|又)?#3(秒|秒钟))?)|(#2(个|个半)?(分钟|刻钟)((零|又)?#3(秒|秒钟))?)|(#3(个|个半)?(秒|秒钟)))半?')
                temp = temp.replace('#1', self.anyNumFun())
                temp = temp.replace('#2', self.anyNumFun())
                temp = temp.replace('#3', self.anyNumFun())
                res.append(temp)
        if len(res) < 2:
            return res[0]
        else:
            return res

    def queryDurationFun(self):
        # 数字单位生成器
        return self.X(f"({self.howmany}(个)?(小时|钟头|分钟|秒))|((多|多少)(久|时间))")

    def conditionDurationFun(self):
        return self.X(f"{self.the_not}({self.equal}|{self.more}|{self.less}|{self.between})") \
            .replace('#1', self.durationFun())  \
            .replace('#2', self.durationFun())

    def durationRangeFun(self):
        return f"{self.durationFun()}{self.X('(-|至|到|～)')}{self.durationFun()}"

    def distanceFun(self, n=None):
        # 距离生成器
        if (n == None or n == 0):
            n = 1
        res = []
        for i in range(n):
            temp = self.X('((#1(个|个半)?(公里|里)((零|又)?#2(千米|米))?)|(#2(个|个半)?(千米|米)))半?')\
                .replace('#1', self.anyNumFun())\
                .replace('#2', self.anyNumFun())
            res.append(temp)
        if len(res) < 2:
            return res[0]
        else:
            return res

    def queryDistanceFun(self):
        # 数字单位生成器
        return self.X(f"({self.howmany}(公里|km|里|千米|米|m))|((多|多少)(长距离|高|远|距离|里程))")

    def conditionDistanceFun(self):
        return self.X(f"{self.the_not}({self.equal}|{self.more}|{self.less}|{self.between})") \
            .replace('#1', self.distanceFun())  \
            .replace('#2', self.distanceFun())

    def distanceRangeFun(self):
        return f"{self.distanceFun()}{self.X('(-|至|到|～)')}{self.distanceFun()}"

    def energyFun(self, n=None):
        # 热量生成器
        if (n == None or n == 0):
            n = 1
        res = []
        for i in range(n):
            res.append(
                f"{self.anyNumFun()}{self.X('(卡|卡路里|千卡|大卡|千焦|焦[耳尔]?|Kcal|KCal)')}")
        if len(res) < 2:
            return res[0]
        else:
            return res

    def queryEnergyFun(self):
        # 数字单位生成器
        queryNum = self.X(self.howmany)
        unit = self.F.maybe(self.X('(卡|卡路里|千卡|大卡|千焦|焦[耳尔]?|Kcal|KCal)'), 0.7)
        return f"{queryNum}{unit}"

    def conditionEnergyFun(self):
        return self.X(f"{self.the_not}({self.equal}|{self.more}|{self.less}|{self.between})") \
            .replace('#1', self.energyFun())  \
            .replace('#2', self.energyFun())

    def energyRangeFun(self):
        # 热量生成器
        return f"{self.energyFun()}{self.X('(-|至|到|～)')}{self.energyFun()}"

    def numUnitFun(self, onlyUnit=0.2, onlyNum=0.1, unitList=None, n=None):
        # n代表生成几个。为None或1时返回一个数据，超过1时返回数组
        # 默认有20%的可能仅出现unit
        # 数字单位生成器
        if (n == None or n == 0):
            n = 1
        res = []
        for i in range(n):
            num = self.anyNumFun()
            if (unitList == None):
                unit = self.F.maybe(self.F.random_element3(
                    self.R['dietUnitList']), 1 - onlyNum)
            else:
                unit = self.F.maybe(self.F.random_element3(
                    unitList), 1 - onlyNum)
            semi = self.F.maybe('半', 0.1)
            res.append(self.F.maybe(
                f"{num}{unit}{semi}", 1 - onlyUnit, f"{unit}"))
        if len(res) < 2:
            return res[0]
        else:
            return res

    def queryNumUnitFun(self):
        # 数字单位生成器
        queryNum = self.X(self.howmany)
        unit = self.F.maybe(self.F.random_element3(
            self.R['dietUnitList']), 0.7)
        return f"{queryNum}{unit}"

    def conditionNumUnitFun(self):
        return self.X(f"{self.the_not}({self.equal}|{self.more}|{self.less}|{self.between})") \
            .replace('#1', self.numUnitFun())  \
            .replace('#2', self.numUnitFun())

    def numUnitRangeFun(self):
        return f"{self.numUnitFun(0)}{self.X('(-|至|到|～)')}{self.numUnitFun(0)}"

    def tagFun(self, n=None):
        if n == None:
            tag = self.anyTagFun()
            return tag
        else:
            tags = []
            for i in range(n):
                tag = self.anyTagFun()
                if tag != "":
                    tags.append(tag)
            return tags

    def stimeFun(self, n=None):
        # n 代表生成多少个
        if n == None:
            time = self.anyTimeFun()
            stime = ""
            if time != "":
                stime = f"{self.X(self.actFromPre)}{time}{self.X(self.actFromPer)}"
        else:
            stime = []
            for i in range(n):
                time = self.anyTimeFun()
                if time != "":
                    stime.append(time)
        return stime

    def etimeFun(self):
        time = self.anyTimeFun()
        etime = ""
        if time != "":
            etime = f"{self.X(self.actToPre)}{time}{self.X(self.actToPer)}"
        return etime

    # def fuzzyTimeFun(self):
    #     return self.X("(过|待)(一)?(会|阵)|(过|待)?(几)个?(小时|分钟|天|日|周|礼拜|星期|月|年)|回头|找时间")

    # def personFun(self):
    #     data = list(map(lambda key: self.R['personMap'][key], self.F.random_sample(
    #         list(self.R['personMap'].keys()), 1)))
    #     personWe = self.X("((我们?)?(和|与|、)?)?")
    #     personOther = "、".join(
    #         list(map(lambda x: self.X(data[0]['name'].replace('#xing', self.F.name()[0]).replace('#ming', (lambda x=self.F.name(): x[1:] if len(x) > 2 else x)())), range(self.F.random_int(0, 4)))))
    #     personNumber = self.X("((一共|总共)?等?#1(个|口)?(人|#2))?")
    #     personAdd = self.X("(一起)?")
    #     person = personWe+personOther+personNumber+personAdd
    #     personEnd = self.X(person).replace('#1', self.X("[0-9][0-9]")) \
    #         .replace('#2', self.X(data[0]['name'] if personOther == "" else "人"))
    #     return self.F.maybe(personEnd, 0.5, "")
    def personNameFun(self):
        data = list(map(lambda key: self.R['personMap'][key], self.F.random_sample(
            list(self.R['personMap'].keys()), 1)))
        groupNum = self.F.random_int(1, 3)
        personNameList = []
        for i in range(groupNum):
            personName0 = self.X(data[0]['name'].replace('#xing', self.F.name()[0])
                                 .replace('#ming', (lambda x=self.F.name(): x[1:] if len(x) > 2 else x)()))
            personName1 = self.X("(#1(个|名|口|位)?(人|#2))|(#2#1(个人|人|名|口|位))|(#2)").replace('#1', self.X("[1-9][0-9]?")) \
                .replace('#2', personName0)
            personNameList.append(personName1)
            if self.F.maybe(True, 0.5, False):
                personNameList.insert(0, self.X('我(自己)?'))
            else:
                personNameList[0] = self.F.maybe(
                    self.X('和|与'), 0.5, '')+personNameList[0]
        return self.F.maybe(self.X('我(自己)?'), 0.5, personNameList)

    def personEndFun(self):
        data = list(map(lambda key: self.R['personMap'][key], self.F.random_sample(
            list(self.R['personMap'].keys()), 1)))
        personName = self.X(data[0]['name'].replace('#xing', self.F.name()[0])
                            .replace('#ming', (lambda x=self.F.name(): x[1:] if len(x) > 2 else x)()))
        personEnd = self.X(
            "((一共|总共|合计|总计|小计|共计|共|计|等)#1(个|名|口|位)?(人|#2)(一起|一块儿|一道)?)?")
        personEnd = personEnd.replace('#1', self.X("[1-9][0-9]?")) \
            .replace('#2', personName)
        return self.F.maybe(personEnd, 0.5, "")

    def placeFun(self):
        data = list(map(lambda key: self.R['placeMap'][key], self.F.random_sample(
            list(self.R['placeMap'].keys()), 1)))
        place = f"{self.X(data[0]['name'])}{self.X('(里|上|旁|里面|旁边|附近|边上|外面|下面|当中|中)?')}"
        return place

    def priceFun(self):
        payNum = self.payNumFun()
        numUnit = self.numUnitFun(onlyUnit=0.7, onlyNum=0)
        qian = self.F.maybe("钱", 0.5, "")
        each = self.F.maybe(self.X("(每|/)"), 0.5, "")
        price = self.F.maybe(f"{payNum}{qian}{each}{numUnit}",
                             0.5, f"{each}{numUnit}{payNum}{qian}")
        return price

    def queryCommandFun(self):
        return self.F.maybe(self.X(self.queryCommand), 0.5, "")

    def planCommandFun(self):
        return self.F.maybe(self.X(self.planCommand), 0.5, "")

    def stopwordFun(self):
        # 从数组中随机选取一个数据
        return self.F.random_element(self.R['stopword'])

    def companyFun(self):
        # 从数组中随机选取一个数据
        lst = self.F.random_element3(list(map(lambda x: x[1]['list'], self.R['companyMap'].items())), list(
            map(lambda x: x[1]['rate'], self.R['companyMap'].items())))
        item = self.F.random_element(lst)
        return self.X(item['stockName'])

    def genSubActPattern(self, subactMap, itemMap, hasAct, groupIdx, pos=None, isEach=None, isRevert=None, onlyValue=False):
        # subactMap是一个字句定义的数组，作用于每个group下的所有item，结构如下
        # {    "nameFun": lambda x, pos: "",    #subact name function
        # "valueFun": lambda x, pos: self.F.maybe(self.X(self.agg),0.5,""), #subact value function
        # "actFun": lambda x, pos: "",     #subact act function
        # "groupName": "queryDietAgg",     #subact 前缀名字
        # "rate": 1,                       #可能被选中的比率
        # "revertRate":0                   #可能倒装的几率
        # "nocomma": self.F.maybe(True, 0.5),    #前导是否有，、等分割标点
        # "noeach":True,                   #是否允许有each的语句
        # "noLe":True,                     #act后面是否有“了”，“了的”的助词,默认没有定义
        # "noDe":True,                     #name前面是否有“的”的助词,默认没有定义
        # "pos": [],                        #分能放在A、B、C的位置
        # "subCompareRate":0                #定义字句出现compare的几率，默认为0
        # },
        ##
        # itemMap是该组的item结构数据
        # hasAct 标识本句中是否已经标识了act
        # groupIdx 标识当前group的序号，从0开始
        # pos 取值0-2，表明该字句map将在主句的什么位置
        # isEach 标识是否分别，如果为None则由本函数随机生成。如果itemMap中只有一个item则不会生成该标识
        # isRevert 标识是否倒序，如果为None则由本函数根据subact的定义几率生成
        # onlyValue 表明是否强制不理会subactMap 中的actFun和nameFun中的定义，默认为False
        s = 0
        pairs = []
        for k, x in enumerate(subactMap):
            if s != 0:
                if x.get("nocomma") == None or x.get("nocomman") == False:
                    pairs.append([self.X("(,|、|的|，|\?|？)?"), "_and"])
            groupValue = x["valueFun"](itemMap, pos)

            # if type(groupValue)==list and len(itemMap)>1:
            #     print(groupValue)

            isEach = self.F.maybe(True, 0.5) if isEach == None else isEach
            mode = "E" if isEach else "T"
            actMode = self.F.maybe(self.X(self.each), 1, "") if isEach else self.F.maybe(
                self.X(self.total), 1, "")
            subact = x['actFun'](itemMap, pos) if x["actFun"] != None else ""
            subname = x['nameFun'](
                itemMap, pos) if x["nameFun"] != None else ""
            isRevert = self.F.maybe(True, x.get("revertRate")) if x.get(
                "revertRate") != None else self.F.maybe(True, 0.5)
            #print(subact, subname, k, isRevert)
            # isRevert = self.F.maybe(
            #    True, 0.5) if isRevert == None else isRevert
            if onlyValue:
                if type(groupValue) == list:
                    # 保持与subname个数一致
                    if type(itemMap) == list:
                        for t, subvalueItem in enumerate(groupValue):
                            if (t <= len(itemMap)-1):
                                if (t != 0):
                                    pairs.append([self.X(self.link), "_link"])
                                pairs.append(
                                    [subvalueItem, f"{x['groupName']}_value_{groupIdx}"])
                    else:
                        pairs.append(
                            [groupValue[0], f"{x['groupName']}_value_{groupIdx}"])
                else:
                    pairs.append(
                        [groupValue, f"{x['groupName']}_value_{groupIdx}"])
            else:
                if not isRevert:  # 处理正装字句
                    if x.get('noeach', False) == False and type(subname) == list and len(subname) > 1:
                        # 仅在x.get('noeach')==False且在item数超过1个的时候启动_total及_each的可能性
                        pairs.append(
                            [actMode, f"{x['groupName']}_{mode}_{groupIdx}"])
                    # if (s == 0 and hasAct == False) or s != 0:
                    if (s == 0 and self.F.maybe(True, 0.5)) or s != 0:
                        pairs.append(
                            [subact, f"{x['groupName']}_act_{groupIdx}"])
                        if (x.get('noLe', False) == False):
                            pairs.append(
                                [self.X(self.actLe) if subact != "" else "", "_actle"])
                    if type(groupValue) == list:
                        # 保持与subname个数一致
                        if type(itemMap) == list:
                            for t, subvalueItem in enumerate(groupValue):
                                if (t <= len(itemMap)-1):
                                    if (t != 0):
                                        pairs.append(
                                            [self.X(self.link), "_link"])
                                    pairs.append(
                                        [subvalueItem, f"{x['groupName']}_value_{groupIdx}"])
                        else:
                            pairs.append(
                                [groupValue[0], f"{x['groupName']}_value_{groupIdx}"])
                    else:
                        pairs.append(
                            [groupValue, f"{x['groupName']}_value_{groupIdx}"])
                    if x.get('noDe', False) == False:
                        # 在noDe被设为True时不出现“的”
                        pairs.append(
                            [self.X(self.der) if subname != "" else "", "_der"]
                        )
                    if type(subname) == list:
                        for t, subnameItem in enumerate(subname):
                            if (t != 0):
                                pairs.append([self.X(self.link), "_link"])
                            pairs.append(
                                [subnameItem, f"{x['groupName']}_name_{groupIdx}"])
                    else:
                        pairs.append(
                            [subname, f"{x['groupName']}_name_{groupIdx}"])
                else:  # 处理倒装字句
                    compareEnd = None
                    isCompare = self.F.maybe(True, x.get('compareRate', 0))
                    if type(subname) == list:
                        for t, subnameItem in enumerate(subname):
                            if (subnameItem != "" and k == 0 and groupIdx != 0 and t == 0):
                                pairs.append([self.X("的|,|，"), "_"])
                            if (t != 0):
                                if (isCompare and t == 1):
                                    compareEnd = self.F.maybe(True, 0.5, False)
                                    if compareEnd:
                                        pairs.append(
                                            [self.X(self.compare2), f"{x['groupName']}_compare_{groupIdx}"])
                                    else:
                                        pairs.append(
                                            [self.X(self.compare1), f"{x['groupName']}_compare_{groupIdx}"])
                                else:
                                    pairs.append([self.X(self.link), "_link"])
                            pairs.append(
                                [subnameItem, f"{x['groupName']}_name_{groupIdx}"])
                    else:
                        if (subname != "" and k == 0 and groupIdx != 0):
                            pairs.append([self.X("的|,|，"), "_"])
                        pairs.append(
                            [subname, f"{x['groupName']}_name_{groupIdx}"])
                        if (isCompare):
                            compareEnd = self.F.maybe(True, 0.5, False)
                            if compareEnd:
                                pairs.append(
                                    [self.X(self.compare2), f"{x['groupName']}_compare_{groupIdx}"])
                            else:
                                pairs.append(
                                    [self.X(self.compare1), f"{x['groupName']}_compare_{groupIdx}"])
                    if (isCompare and compareEnd == False):
                        pairs.append(
                            [self.X(self.compare2), f"{x['groupName']}_compareAdd_{groupIdx}"])

                    if x.get('noeach', False) == False and type(subname) == list and len(subname) > 1:
                        # 仅x.get('noeach')==False且在item数超过1个的时候启动_total及_each的可能性
                        pairs.append(
                            [actMode, f"{x['groupName']}_{mode}_{groupIdx}"])
                    if (s == 0) or s != 0:
                        pairs.append(
                            [subact, f"{x['groupName']}_act_{groupIdx}"])
                        if (x.get('noLe', False) == False):
                            pairs.append(
                                [self.X(self.actLe) if subact != "" else "", "_actle"])
                    if type(groupValue) == list:
                        # 保持与subname个数一致
                        if type(itemMap) == list:
                            for t, subvalueItem in enumerate(groupValue):
                                if (t <= len(itemMap)-1):
                                    if (t != 0):
                                        pairs.append(
                                            [self.X(self.link), "_link"])
                                    pairs.append(
                                        [subvalueItem, f"{x['groupName']}_value_{groupIdx}"])
                        else:
                            pairs.append(
                                [groupValue[0], f"{x['groupName']}_value_{groupIdx}"])
                    else:
                        pairs.append(
                            [groupValue, f"{x['groupName']}_value_{groupIdx}"])
            s += 1
        return pairs

    def genMainActPattern(self, dataMap, subactMap, subactNum=None, mainCompareRate=0, isRelation=False, actRate=0.2, revertRate=0.5, derRate=0.2, itemFun=None, itemName="name", actFun=None, actName="act"):
        # 根据dataMap生成实体标注模版
        # 1、dataMap 是数据的结构。每一组group由一些列item组成，用来生成一个短语,最后将把所有组合成一个句子，例如
        # {"group0":["苹果","香蕉","面包"],
        #  "group1":["咖啡"],
        #  "group2":["蛋糕","可乐"]
        # }
        # 2、subactMap 的定义是以下的结构：
        # [  {"nameFun":计算字句name的函数,函数接受一个参数是该group下的所有items
        #     "valueFun":计算数据的函数,函数接受一个参数是该group下的所有items
        #     "actFun":计算动词的函数,函数接受一个参数是该group下的所有items
        #     "groupName":表示这组数据的名字前缀，结果为groupName+(E/T)+i)。比如numUnitE0表示第一组each类型，unitUnitT1表示第二组total类型
        #     "rate":0-1 表示出现的概率 ，而出现的顺序是随机的
        #     "revertRate" 表示字句出现倒装的几率，如果没有指定则默认为0.5
        #     "nocomma": self.F.maybe(True, 0.5),    #前导是否有，、等分割标点
        #     "noeach":True,                   #是否允许有each的语句
        #     "noLe":True,                     #是否act后面有"了"，默认没有定义，为不限定
        #     "noDe":True,                     #是否name前面有"的"，默认没有定义，为不限定
        #     "pos": [],                        #分能放在A、B、C的位置
        #     "rank": -1                         #在每个pos分组里的排序,默认没有排序。正数排在最前面，负数排在最后面
        #    }
        # ]
        # [{"name":lambda x:"","value":lambda x:self.numUnitFun(),"act":lambda x:"","groupName":"numUnit","rate":1},
        #  {"name":lambda x:self.X('(时间)?'),"value":lambda x:self.durationFun(),"act":lambda x:self.X("花"),"groupName":"duration","rate":0.5},
        #  {"name":lambda x:self.X('(钱|钞票)?'),"value":lambda x:self.payNumFun(),"act":lambda x:self.X("支付"),"groupName":"paynum","rate":0.5}]
        # actMap出现与否由rate决定，出现顺序随机
        # subactNum指定每组actMap最多和最少出现几个。通常是数组形式如[1,3]，如果为None或大于actMap的个数则为actMap的个数。可以由这个参数指定只出现一个
        # 3、已废弃。isMapping 生成诸如“吃了蛋糕、香蕉、苹果分别1个、2个、3个”这样的语句对应的语句。actMap仅对第一项处理，且不管rate
        #    isMapping subactMap的处理规则，如果最后选中2中subact，可能生成买蛋糕、雪糕分别是2个和3个，分别花了2块和3块
        # 4、actRate为在正序情况，第二个group以后的仍出现act的频率，例如：吃了1个蛋糕，【吃了】2个香蕉，第二个group出现吃了的几率
        # 5、revertRate为出现倒装的几率。正装句式吃了一个苹果、香蕉，对应的倒装句式吃苹果、香蕉吃了一个。统一正装revertRate=0，反之revertRate=1
        # 6、derRate为正装句情况下item出现之前的"的"的几率。例如：打了2个小时【的】篮球
        # 7、itemFun 定义item的计算函数，默认情况为每个group的item
        # 8、itemName 定义每个item的Name，结构为itemName+group的序号
        # 9、actFun 定义主语句的动词
        # 10、actName 定义主语句的动词的名字
        # 11、mainCompareRate 定义主句出现compare的几率。默认为0不出现
        # groupMap 定义语句结束或语句开头时出现的act子句，格式及含义与actMap一致，区别valueFun函数接受的一个参数是整个group的所有items。为None的时候不起作用

        groupCnt = len(dataMap.keys())
        if subactNum == None:
            subactMin = 1
            subactMax = len(subactMap)
        elif type(subactNum) == list:
            subactMin = subactNum[0]
            subactMax = subactNum[1]
        else:
            subactMin = 1
            subactMax = subactNum
        if (subactMax > len(subactMap)):
            subactMax = len(subactMap)
        pairs = []
        for i, groupName in enumerate(dataMap.keys()):
            act = actFun(dataMap[groupName]) if actFun != None else ""
            actLe = self.X(self.actLe) if act != "" else ""
            hasActHua = False
            actHua = self.X(self.actHua) if act != "" else ""
            actHuaAd = self.X(self.actHuaAd) if act != "" else ""
            isRevert = self.F.maybe(True, revertRate)
            # 根据rate选取若干字句配置
            sampleSubactMap = self.F.random_select(
                subactMap,
                list(map(lambda x: x['rate'], subactMap)),
                min=subactMin,
                max=subactMax)
            # 根据pos分解A、B、C三组
            sampleSubactMap1 = self.F.random_split(sampleSubactMap, 3, list(
                map(lambda x: x.get('pos'), sampleSubactMap)))
            # 根据rank在A、B、C三组中分别排序
            subactSegMap = []
            for x in sampleSubactMap1:
                sampleSubactMap2 = []
                sampleSubactMap2.extend(sorted(filter(lambda y: y.get(
                    'rank', -1) >= 0, x), key=lambda y: y.get("rank")))
                sampleSubactMap2.extend(
                    list(filter(lambda y: y.get('rank') == None, x)))
                sampleSubactMap2.extend(
                    sorted(filter(lambda y: y.get('rank', 1) < 0, x), key=lambda y: y.get("rank")))
                subactSegMap.append(sampleSubactMap2)
            # 前导段的subactMap处理
            if len(subactSegMap[0]) != 0:
                pairs.extend(self.genSubActPattern(
                    subactSegMap[0], dataMap[groupName], False, i, pos=0))
            # 中心段的subactMap处理
            if not isRevert:
                # 正序情况下
                # ???or len(subactSegMap[1]) == 0:
                if i == 0 or self.F.maybe(True, actRate):
                    # 在非第一组的时候，多数情况下不需要动词及动词的修饰,如<吃了>一个蛋糕、[吃了]2个面包和[吃了]3个鸡蛋
                    # 但是运动、行为的时候应当有这个动词，如打了2个小时的篮球，踢了1个小时的足球。所以用actRate来控制
                    # actRate表示出现动词的几率，默认0.2
                    hasActHua = self.F.maybe(True, 0.2, False)
                    if hasActHua:
                        pairs.append([actHua, f"_actHua"])
                    pairs.append([act, f"{actName}{i}"])
                    pairs.append([actLe, f"_actle"])
                    hasAct = True
                else:
                    hasAct = False
                    hasActHua = False
                # 生成subAct
                pairs.extend(self.genSubActPattern(
                    subactSegMap[1], dataMap[groupName], hasAct, i, pos=1))
                # 设定一定比率出现“的”，如吃了一个的面包、跑了3公里的步
                # if len(subactSegMap[1]) > 0:
                #    pairs.append(["的", "_der"])
                # else:
                if self.F.maybe(True, derRate):
                    pairs.append(["的", "_der"])
            else:
                # 倒装情况下可能: [吃]面包<吃了>一个,所以此处处理第一个吃的act。如果存在这个act，则后面的act是辅助的，否则后面act是主要的，这由hasAct标识
                if self.F.maybe(True, 0.5):
                    hasActHua = self.F.maybe(True, 0.2, False)
                    if hasActHua:
                        pairs.append([actHua, f"_actHua"])
                    pairs.append([act, f"{actName}{i}"])
                    hasAct = True
                else:
                    hasActHua = False
                    hasAct = False
            itemCnt = len(dataMap[groupName])
            compareEnd = None
            isCompare = self.F.maybe(True, mainCompareRate)
            for j, item in enumerate(dataMap[groupName]):
                itemValue = itemFun(item) if itemFun != None else item
                pairs.append([itemValue, f"{itemName}{i}"])
                if (isCompare and itemCnt > 1 and j == 0):
                    compareEnd = self.F.maybe(True, 0.5, False)
                    if compareEnd:
                        pairs.append(
                            [self.X(self.compare2), f"compare{i}"])
                    else:
                        pairs.append(
                            [self.X(self.compare1), f"compare{i}"])
                elif j < itemCnt - 1:
                    pairs.append([self.X(self.link), "_link"])
            if (isCompare and itemCnt > 1 and compareEnd == False):
                pairs.append([self.X(self.compare2), f"compareAdd{i}"])
            if hasActHua:
                pairs.append([actHuaAd, "_actHuaAd"])
            if isRevert:
                pairs.extend(
                    self.genSubActPattern(
                        subactSegMap[1], dataMap[groupName], hasAct, i, pos=1)
                )
                # 倒装情况下都需要动词,例如 面包吃了2个，西瓜汁<喝了>一杯
                if (hasAct):  # 倒装句已经有一个主句act了
                    pairs.append([act, f"_{actName}{i}"])
                else:
                    pairs.append([act, f"{actName}{i}"])
                    hasAct = True
                pairs.append([actLe, f"_actle"])

            # 后续段的subactMap处理
            pairs.extend(self.genSubActPattern(
                subactSegMap[2], dataMap[groupName], hasAct, i, pos=2))
            if i < groupCnt - 1:
                if (isRelation):
                    pairs.append(
                        [self.X(self.relation), f"{actName}{i}_relation"])
                else:
                    pairs.append([self.X(self.link), "_link"])

        return pairs

    def randomExtend(self, lst, pattern):
        # 从lst列表中随机选取0-len(lst)个项目，顺序也不确定
        num = self.F.random_int(0, len(lst))
        randomLst = self.F.random_sample(lst, num)
        for item in randomLst:
            pattern.extend(item)
        return pattern

    def assembleCore(self, core):
        # 废弃
        breaker = [
            self.X(self.breaker),
            self.X(self.breaker),
            self.X(self.breaker)
        ]
        person = self.personFun()
        place = self.placeFun()
        time = self.anyTimeFun()
        pos = self.X(self.pos) if time != "" else ""
        stime = self.anyTimeFun()
        actFromPre = self.X(self.actFromPre) if stime != "" else ""
        actFromPer = self.X(self.actFromPer) if stime != "" else ""
        etime = self.anyTimeFun()
        actToPre = self.X(self.actToPre) if etime != "" else ""
        actToPer = self.X(self.actToPer) if etime != "" else ""

        arg = {
            "headTimeMap": [[time, "time"]],
            "tailTimeMap": [[pos, "_pos"], [time, "time"]],
            "sTimeMap": [[actFromPre, "_actFromPre"], [stime, "stime"], [actFromPer, "_actFromPer"]],
            "eTimeMap": [[actToPre, "_actToPre"], [etime, "etime"], [actToPer, "_actToPer"]],
            "seTimeMap": [[actFromPre, "_actFromPre"], [stime, "stime"], [actFromPer, "_actFromPer"],
                          [actToPre, "_actToPre"], [
                              etime, "etime"], [actToPer, "_actToPer"]
                          ],
            "breakerMap": [[breaker[0], "_breaker"]],
            "personMap": [[breaker[1] if person != "" else "", "_breaker"], [person, "person"], [self.X("(一起|一块儿)?") if person != "" else "", "_personAd"]],
            "placeMap": [[breaker[2] if place != "" else "", "_breaker"], [place, "place"]]
        }

        pattern = []
        type = self.F.random_element3([0, 1, 2, 3, 4, 5], [1, 1, 1, 1, 1, 1])
        if type == 0:
            self.randomExtend(
                [arg["headTimeMap"], arg["personMap"], arg["placeMap"], arg["breakerMap"]], pattern)
            pattern.extend(core)
        if type == 1:
            pattern.extend(core)
            self.randomExtend(
                [arg["tailTimeMap"], arg["personMap"], arg["placeMap"], arg["breakerMap"]], pattern)
        if type == 2:
            self.randomExtend(
                [arg["sTimeMap"], arg["personMap"], arg["placeMap"], arg["breakerMap"]], pattern)
            pattern.extend(core)
            pattern.extend(arg["eTimeMap"])
        if type == 3:
            pattern.extend(arg["sTimeMap"])
            pattern.extend(core)
            self.randomExtend(
                [arg["eTimeMap"], arg["personMap"], arg["placeMap"], arg["breakerMap"]], pattern)
        if type == 4:
            self.randomExtend(
                [arg["seTimeMap"], arg["personMap"], arg["placeMap"], arg["breakerMap"]], pattern)
            pattern.extend(core)
        if type == 5:
            pattern.extend(core)
            self.randomExtend(
                [arg["seTimeMap"], arg["personMap"], arg["placeMap"], arg["breakerMap"]], pattern)
        return pattern

    def getDescript(self):
        self.descript = """
1、增加descript说明文件
2、允许sub字句的name返回数组，改数组的每一项将标注同样的名字（主要用于查询，例如吃包子摄入了多少蛋白质、脂肪和维生素)
3、no mapping
4、允许'花在打篮球上的时间'这类的表达
5、时间加'的'的可能性
# 2022.03.11
6、anyNumList的数字模式的比例调整
7、payNumFun允许3块5这种说法
# 2022.03.12
8、禁止group超过1个的情况
# 2022.03.14
9、增加消化系统行为
10、允许dietMetricAmount和dietAmount两种情况
11、子句增加agg的标识
12、字句增加groupby的标识
13、增加place to assembleCore
14、合并assembleCore
15、只有diet和buy允许多个item，sport、behavior、sign只有一个item
16、condition增加equal的情况，the_not增加"没",equal增加"有"
17、dietUnit增加百分比、百分点等，将用于查询的单位
18、增加queryCommand ，含"比较"
# 2022.03.15
19、增加测试饮食主句比较的案例
# 2022.03.16
20、增加运动"乒乓球"
21、增加查询"啥"
22、增加食品"东西"、"东东"、"午饭"等通用类
# 2022.03.18
23、energyFun增加"卡路里"识别
24、fix bug about compare,允许比较出现comapre+compareAdd的复合语句 
25、fix bug about 视黄素
26、取消sub字句关于s索引的标记，相应位置统一都为0 （重大改动）
27、增加planCommand，包含"应该"
28、处理stime，etime，estime，person，place (重大改动)
29、归集函数 （重大改动)
# 2022.03.21
30、sign也归集subact
31、各类别stime、etime与setime互斥
# 2022.03.23
32、取消字句的意图标识
33、主句用#分隔单词
34、anmount类的动词增加"是|为"
35、sub字句的name多个时用link连接，link="(,?以及|,?还有|和|、|,|，|及)"
36、planCommand增加"记得"
37、actLe更新为"(了|了的是|的是)?"
# 2022.03.24
38、启用planCommand
# 2022.03.25
39、planCommand增加"打算"
# 2022.03.27
40、less 增加 "不到"
41、less 增加 "以内|之内|以下|之下" 、 "以外|之外|以上|之上"
# 2022.03.30
42、去掉person的"一块"
43、全面允许condition
# 2022.03.31
44、remove setime
# 2022.04.01
45、尝试多个amount，例如1个蛋糕、香蕉，2个面包
# 2022.04.02
46、duration、pay、energy字句的动词增加"控制",例句:打篮球控制在3个小时以内、这周花费控制在1000块
47、place的动词增加“去”、“到”、“至”等，例句：走路去图书馆花了20分钟
48、正式去除字句第二个index
49、增加判断字句的倒装几率，并配置place字句为非倒装形式
50、stime、etime、place统一把具体值放在name中，而非value中。主要是因为这类语句可能有act，同时act-name的形式是固定的，不会出现name-act的形式。因此同时用revertRate=0限定
51、condition字句增加"不要"、"不应"、"控制在"等修饰
52、增加字句rank的控制，规则是默认rank为None则本组随机。然后按rank为0，1，2...顺序放在最前面，rank为-3，-2，-1...的顺序放在最后面
53、把52应用到第二个饮食或购买
# 2022.04.03
54、提高sportDistance比率到0.8，并尝试取消querySport的subactNum控制。
55、增加dietPattern1，指定有10%几率生成特定的吃了"一块包子花了一块"这类语句
# 2022.04.04
56、数量还有一个名词叫"结果",特别是sign的字句
57、修正subQueryAmount的动词都是用dietAct的bug
58、子句是否有E/T改为根据subactName的数量来确定
59、子句增加noLe的定义，默认情况下不限定。如果设定为True，在subact后面不会有"了"的助词.结果应用在place子句上
60、子句增加noDe的定义，默认情况下不限定。如果设定为True，在subname前面不会有"的"的助词.结果应用在place子句上
61、self.der允许为空，从而避免总是出现吃了3个的面包,这里“的”其实是多余的
62、尝试person的解构,(暂时不处理)
# 2022.04.05
63、主句name前的'的'采用不确定做法
64、取消amount1、amount2
# 2022.04.06
65、fixUnit增加"两"
66、食品增加"梨","泸州老窖"
67、55应用到buy意图上
68、完成62功能，person_name为多个，person_value为汇总
69、实现主句和字句互斥地增加compare定义(重要改动)
# 2022.04.07
70、agg增加"累计","总计"
71、测试mix input & query
# 2022.04.08
72、增加tag的字句，用于识别早餐、运动等tag内容
73、增加fuzzyTime的字句,用于识别"过一会","待会","改天","找时间"等模糊时间
74、anyTimeList增加“过两个小时后“，”再待2个小时后“这类的时间生成
75、适度增加subAmountFixUnit关于块的出现频率
# 2022.04.19 - 2022.04.20
76、howmany增加"哪(几|些)(个|笔|条"
77、payAct增加"开销"
# 2022.04.25
78、foodMap增加炒菜根、炒豆角
79、dietPatten3去掉名词花，生成比例下降到5%
80、queryCommand加"吗","呢"
90、dietPatten0的pay比率从0.5提升到0.9以应对饮食花费的数量单位没被解析为pay_value
91、link增加"。"以防止名词解析时连带。的情况
92、agg、groupby在input和query语句中都可能出现 （重大改动)
93、去掉对字句的个数的现实，完全随机
94、placeFun增加"当中"，"中"
# 2022.04.27
95、foodMap中增加单独的"水",sportMap去掉跳水,应对水被解析为sport#name
# 2022.04.28
96、signPattern0改为70%为数值，30%为range
97、timeUnit多几个“点”，foodUnit多几个"个“。应对3点被解析为数值，“个"经常与名字粘在一起
98、把“情况"加入queryCommand
99、diet pattern3模版增加pay、duration、energy字句
100、增加groupValue在字句的list的可能性，确保小于itemMap的个数。用于处理吃了包子、面包分别是1个和2个这种语句 (重大改动)
101、数字生成器调整为任意分数、任意百分数、任意小数和0～10任意整数的比例为0.5%,0.5%,1%,98%
102、person由原来可能5个降为1-3个
103、调整所有意图的各字句生成比例，原则本意图的字句概率为1，辅助字句0.5，不常用或实验性(tag等)的0.03-0.05
104、查询语句限制每次仅查询一个指标数据
# 2022.04.29
105、修改cnSegs，允许出现”3点“，”3点5分","3点钟" 等的情况
106、stime增加50%几率出现"在｜于"
107、foodName、buyname增加五花肉、三明治等数字开头的
108、subact在正装第一个子句时有一半的机会出现act
109、duration、pay提供subact的比例从10%到90%，应对吃了一块蛋糕被理解为1块花费
110、subAmountFixUnit的unitList调整“块”仍为一个
111、person增加“和”，“与”的act
112、调整subNum 为3～6
# 2022.04.30
113、101改动不成功，导致浮点数、大整数都被拆分。增加0-9999任意整数的生成,比率20%,0-9999任意浮点数比率20%,各位整数50%
114、降低子句必选比例,应对由于input必须选定amount,pay等导致输入语句没有对应的字句出现会倾向解析为query类的字段的问题
115、dietAct、payAct、spendAct等吃、花等选取的几率应远高于其他动词
116、diet和buy增加pattern用于独立生成2个或3个numUnit-name形式。如吃了一个蛋糕、二个面包、三个苹果
# 2022.04.30A
117、训练参数如果设置zero=True，会出现阿拉伯数字解析不正常，应让zero=False
118、*!! 4.28设立104后出现录入解析为查询,原因是虽然目的想明确查询只有一个疑问，但造成录入有多特征而查询特征单一，从而当简单录入时倾向查询
    所以改回系统自由随机，每个字句一般自动取3-9个自己组合（重要改动）
# 2022.05.01
119、为diet、buy增加price子句
120、queryCommand增加“什么时候”，“何时“等表示时间的询问
121、增加stopword机制，目前stopword.json还比较少
122、增加queryTime子句, query的个数从3～9改为3～10
123、”什么、如何、啥“ howmany移到queryCommand
# 2022.05.02
124、扩充停用词到400个,"分别"移入停用词,停用词比例从5%扩充到10%
125、取消fuzzyTime
126、compareAdd增加“同期”
127、增加“开始"到planCommand
# 2022.05.04
128、增加“定、订”动词到buy，增加票证的buy#name
129、增加companyMap代表公司名称
130、增加stock类别（重大改动)
# 2022.05.05
131、增加placeMap的国内、国外城市
132、增加修饰的状语"太|不怎么|有够|够|有点"
133、增加大盘指数；company的stockName可配置多种名称及股票代码
# 2022.05.05 A
134、调整person策略，50%是我，另外50%是我和...
135、饮食增加5%几率的pattern4,专门指定"吃了个蛋糕“，”吃了盘西红柿炒蛋“这类语句
136、饮食库增加李子（因为可能与小李子人名混淆），增加千张（因为可能与数字单位混淆）
# 2022.05.08
137、personNameFun有50%机会在人名前加'我'
138、fix subQueryTime bug
139、单位增加"双、架、辆"
140、暂时去掉subQueryTime
# 2022.05.12
141、metric增加纯净水等
142、tag增加宵夜
143、buyMap增加租、租赁的act
144、增加feel主句 (重大改动)
# 2022.05.13
145、queryMetricAmount改为dietMetricAmount (重大改动)
146、fix feel bug
147、durationFun加入5%的fuzzyDuration （重大改动)
148、feel增加duration子句
# 2022.05.13 A
149、timer增加“近三天”，"解析来的一个礼拜"等
# 2022.05.16
150、fix feelState
151、停用词'另一方面'影响了‘方便面’的解析，在foodMap，和buyMap中增加
152、feelState增加'很厉害'
# 2022.05.17
153、fuzzyDuration增加'夜','宿'
# 2022.05.18
154、buyMap增加'包'
155、behavior增加个人卫生:"起床|刷牙|洗脸|洗脚|洗手|洗澡|(洗|做)头发|(做|修)指甲"
155、dietUnit增加'束'、‘壶’、‘头’
156、fuzzyDuration增加一会、一阵、片刻
157、behaviorMap增加(想|思考|交流|讨论|争吵|沟通|反思|阅读)(问题)
158、personNameFun把“我”替换成self.X("我(自己)?")；增加"和"的前导
159、planCommand增加“还要”，"还想要"
160、dietAct增加“食用","咬","嚼","吞下","咽下“,"啃"
161、foodMap增加“口香糖"、“口口酥"、“窝窝头"
162、decorate增加“狼吞虎咽"
163、behaviorMap增加“和"的动作，比如和面，和水泥
164、behaviorMap增加"打、捉、抓、捕、放“各种动物，如蚊子、苍蝇、蟑螂、蜻蜓等
165、behaviorMap增加劳作，"掰|剥|削|摘|采"等
# 2022.05.18 A
166、self.breaker增加“？”，“\?"
167、behaviorMap增加"打架"
168、单位增加拳，拳头
# 2022.05.19
169、foodMap、buyMap增加"花蛤"
170、dietAct增加'磕'
171、sportMap增加做有氧运动，做无氧运动，登山跑,卷腹,热身,下腰
172、behaviorMap增加冥想
173、behaviorMap增加吹风扇、吹空调、吹牛、侃大山、聊天
174、signMap增加身体长度，测量臂展、肩宽、臂长、腿长
175、personNameFun的首个元素50%几率加和/与
# 2022.05.19 A
176、buyMap增加包包、箱包
177、unit增加“团","卷","提","加仑","棵","节","堂"
178、behavior增加“聊","侃","系","搜索","找"等act，增加“qq”，“百度","微信"，“头条"等name
179、behavior增加"上课、上培训班、上班"系列,增加"下棋"系列,增加考试系列
180、behavior增加约会、面试等
181、去掉behavior中的买卖主题
# 2022.05.20 
182、dietMetricName增加B族
183、训练总数据增加到60w
184、增加很小比率时间为 *点多的形式,避免被解析成query语句
# 2022.05.23
185、behaviorMap增加“搬” act
186、sportMapMap增加"三级跳远"
187、behaviorMap增加"比赛"、“工作"、“提肛"
188、buyMap增加“器械”
189、behaviorMap增加“炒菜"
190、payNumFun增加5%的可能性出现2万、3千、4百等不带块的钱数表达
# 2022.05.24
191、unit增加'朵','袋','袋子'
192、place识别‘回’的act
193、feel增加"酸痛","麻","肿"
# 2022.05.25
194、queryCommand中祈使请求句应可选,避免单独说"查看"被解析到behavior中
# 2022.05.26
195、buyName增加"洗面奶"
196、anyTimeFun增加5%可能出现“3-5点这种情况"
197、behavior分离更多act和name,比如个人卫生部分等
198、food、buyMap增加“三五"
199、fuzzyDuration增加“一直"
# 2022.05.27
200、 dietPattern3增加"包","大盘鸡","卷心菜"
201、feel增加"觉得"
202、foodMap增加“干煸四季豆"
# 2022.05.31
203、热量单位增加焦[耳尔]?
# 2022.06.01
204、在diet act上尝试否定用语(重大改动)
# 2022.06.02
205、planCommand增加“可”，“可以"
206、agg去掉“一起"
207、buy和behavior(部分)增加“不”的否定用语(重大改动)
208、subQueryAmount补上subact,subname
# 2022.06.08
209、增加diet#drugName,diet#smokeName (重大改动)
210、diet/buy增加diet#name增加brand的可能性 (重大改动)
# 2022.06.09
211、smoke增加品类和中/细支
212、behavior增加"逗"
# 2022.06.10
213、groupby增加允许”每个礼拜五“的说法
214、生成50万数据，test改为2条，避免验证费时
215、foodMap去掉与tag冲突的部分
216、foodMap增加"小笼包"
217、planCommand去掉"应该"
218、增加diet#waterName
219、dietMetric去掉水和烟的表达,对应增加218
# 2022.06.14
220、dietMetric去掉酒，对应的增加到foodMap中
221、foodMap增加冰摩卡
222、decorate增加5分熟、七分熟、过热、烤焦等
223、花费增加act“点、办”,buyMap增加健身卡、美容卡等
224、行为增加act“编、织、敲"
225、drugName增加莫西沙星等
226、subPerson增加actFun,可以含“帮|给|为|替”
227、brand增加“百岁山”，“85度c"
# 2022.06.16 
228、food增加千层蛋糕，千尊披萨
229、behavior增加出差/出警/出游等,增加打毛衣，打针线等
230、buy增加餐饮费、交通费、住宿费等各种费
240、训练数据50w，验证与测试集暂时降为100和2
# 2022.06.17
241、food增加小番茄、荷包蛋、千禧果、豆浆、普洱、鸡块、卤面等
242、drug增加100多项
# 2022.06.19
243、food增加拉面、牛肉丸、芝士丸等
244、unit增加串
# 2022.06.20
245、增加homedishMap(重大改动)
# 2022.06.22
246、修饰语增加高蛋白/低脂
247、fooddish增加煲类、红油类
248、energyAct,dietAct增加包含|含|含有|有 (重大改动)
249、增加smoke金砂
# 2022.06.23
250、qureyCommand去掉呢，增加怎么样；stopWord增加“然后呢"
251、增加queryDietPattern1，生成1个面包多少卡的句子(重大改动)
252、homedish增加粉/面/汤/沙拉
# 2022.06.24
253、dietAct增加"会|将|将会|可能|可能会"
254、food增加小杨生煎，homddish增加"包/饭/卷/饼"
255、stopWord增加"这个|那个"
# 2022.06.26
256、drug增加整肠生
# 2022.06.30
257、food增加藜麦小米粥
258、decorate增加差不多
259、训练数据增加到70w
260、homedish增加碳烤
261、self.link增加“加"
262、homedish增加枸杞叶瘦肉汤
# 2022.07.01
263、food增加韩式炸鸡、美式咖啡、日式烤肉等
264、place增加场景一
# 2022.07.02
265、增加若干带“叶”的homedish
# 2022.07.06
266、stopWord增加"还"
267、homedish增加“拌面扁食"

        """
        return self.descript

    def subAmount(self, name, rate, n=None):
        n = self.F.random_int(1, 3)
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(结果|量|数量)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.numUnitFun(n=n), 0.5, self.conditionNumUnitFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.1, ""),
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "pos": []
                }

    def subAmountFixUnit(self, name, rate):
        n = self.F.random_int(1, 3)
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(结果|量|数量)"), 0.1, ""),
                "valueFun": lambda x, pos: self.numUnitFun(n=n, onlyUnit=0.2, onlyNum=0, unitList=["包", "钱", "个", "块", "两"]),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.1, ""),
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "pos": []
                }

    def subAmountMore(self, name, rate, rank=None):
        return {"nameFun": lambda x, pos: list(map(lambda k, x=x: self.X(x[0]['name']), range(self.F.random_int(1, 3)))),
                "valueFun": lambda x, pos: self.F.maybe(self.numUnitFun(), 0.5, self.conditionNumUnitFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X(self.dietAct), 0.5, ""),
                "groupName": name,
                "rate": rate,
                "rank": rank,
                "nocomma": self.F.maybe(True, 0.5),
                "pos": [2]
                }

    def subPay(self, name, rate):
        n = self.F.random_int(1, 3)
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("钱"), 0.2, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.payNumFun(n=n), 0.5, self.conditionPayNumFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X(self.payAct), 0.9, ""),
                "groupName": name,
                "nocomma": self.F.maybe(True, 0.5),
                "rate": rate,
                "pos": []
                }

    def subDuration(self, name, rate, n=None):
        n = self.F.random_int(1, 3)
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(时间|光景)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.durationFun(n=n), 0.5, self.conditionDurationFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X(self.spendAct), 0.9, ""),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "rate": rate,
                "pos": []
                }

    def subEnergy(self, name, rate, n=None):
        n = self.F.random_int(1, 3)
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("热量|能量"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.energyFun(n=n), 0.5, self.conditionEnergyFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X(self.energyAct), 0.5, ""),
                "groupName": name,
                "nocomma": self.F.maybe(True, 0.5),
                "rate": rate,
                "pos": []
                }

    def subDistance(self, name, rate, n=None):
        n = self.F.random_int(1, 3)
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(距离|里程)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.distanceFun(n=n), 0.5, self.conditionEnergyFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.05, ""),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "rate": rate
                }

    def subDecorate(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.X(self.decorate),
                "actFun": lambda x, pos: "",
                "nocomma": self.F.maybe(True, 1),
                "groupName": name,
                "noeach": True,
                "rate": rate,
                "pos": []
                }

    def subStime(self, name, rate, compareRate=0):
        return {"nameFun": lambda x, pos: self.stimeFun(self.F.random_int(1, 3)),
                "valueFun": lambda x, pos: "",
                "actFun": lambda x, pos: self.F.maybe(self.X("在|于"), 0.5, ""),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "noeach": True,
                "rate": rate,
                "pos": [0, 2],
                "noLe": True,
                "noDe": True,
                "compareRate": compareRate
                }

    def subEtime(self, name, rate):
        return {"nameFun": lambda x, pos: self.etimeFun(),
                "valueFun": lambda x, pos: "",
                "actFun": lambda x, pos: "",
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "noeach": True,
                "rate": rate,
                "pos": [0, 2],
                "noLe": True,
                "noDe": True,
                }

    def subPerson(self, name, rate):
        return {"nameFun": lambda x, pos: self.personNameFun(),
                "valueFun": lambda x, pos: self.personEndFun(),
                "actFun": lambda x, pos: self.X("(帮|给|为|替)?"),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "noeach": True,
                "rate": rate,
                "pos": [0, 2],
                "noLe": True,
                "noDe": True,
                "revertRate": 0,  # 确保nameFun在valueFun之前
                }

    def subPlace(self, name, rate):
        return {"nameFun": lambda x, pos: self.placeFun(),
                "valueFun": lambda x, pos: "",
                "actFun": lambda x, pos: self.X("(去|到|回|去到|前往|至|在|从|自|途经|经过|路过)"),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "noeach": True,
                "noLe": True,
                "noDe": True,
                "rate": rate,
                "pos": [0, 2],
                "revertRate": 0  # 确保actFun在nameFun之前
                }

    def subPlanCommand(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.planCommandFun(),
                "actFun": lambda x, pos: "",
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "noeach": True,
                "pos": [0, 2]
                }

    def subAgg(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.X(self.agg),
                "actFun": lambda x, pos: "",
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "noeach": True,
                "pos": []
                }

    def subQueryCommand(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.queryCommandFun(),
                "actFun": lambda x, pos: "",
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "noeach": True,
                "pos": [0, 2]
                }

    def subGroupby(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.X(self.groupby),
                "actFun": lambda x, pos: "",
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "noeach": True,
                "pos": []
                }

    def subQueryAmount(self, name, rate):
        return {"nameFun": lambda x, pos: lambda x, pos: self.F.maybe(self.X("(结果|量|数量)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.queryNumUnitFun(), 0.5, self.conditionNumUnitFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.1, ""),
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "pos": []
                }

    def subQueryMetricAmount(self, name, rate, compareRate=0):
        return {"nameFun": lambda x, pos: list(map(lambda x: self.X(x), self.F.random_sample(self.nutrition, self.F.random_int(1, 3)))),
                "valueFun": lambda x, pos: self.F.maybe(self.queryNumUnitFun(), 0.5, self.conditionNumUnitFun()),
                "actFun": lambda x, pos: self.X(self.dietAct),
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "pos": [],
                "compareRate": compareRate
                }

    def subQueryDuration(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(时间|光景)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.queryDurationFun(), 0.5, self.conditionDurationFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X(self.spendAct), 0.9, ""),
                "groupName": name,
                "rate": rate
                }

    def subQueryPay(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("钱"), 0.2, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.queryPayNumFun(), 0.5, self.conditionPayNumFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X(self.payAct), 0.9, ""),
                "groupName": name,
                "nocomma": self.F.maybe(True, 0.5),
                "rate": rate,
                "pos": []
                }

    def subQueryEnergy(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("热量|能量"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.queryEnergyFun(), 0.5, self.conditionEnergyFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X(self.energyAct), 0.5, ""),
                "groupName": name,
                "nocomma": self.F.maybe(True, 0.5),
                "rate": rate,
                "pos": []
                }

    def subQueryTime(self, name, rate):
        nameT1 = "(点|小时|天|号|日|礼拜|星期|周|月|季度|年)"
        value1 = "(哪(一|几|些)?(个)?)"
        nameT2 = "(点|号)"
        value2 = "(几|多少)"
        x = self.F.maybe(True, 0.7, False)
        nameT = ""
        value = ""
        if x:
            nameT = nameT1
            value = value1
        else:
            nameT = nameT2
            value = value2
        return {"nameFun": lambda x, pos: self.X(nameT),
                "valueFun": lambda x, pos: self.X(value),
                "actFun": lambda x, pos: self.F.maybe(self.X("在|于|是|为"), 0.5, ""),
                "groupName": name,
                "nocomma": self.F.maybe(True, 0.5),
                "rate": rate,
                "pos": []
                }

    def subQueryDistance(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(距离|里程)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.queryDistanceFun(), 0.5, self.conditionDistanceFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.05, ""),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "rate": rate
                }

    def subQueryTime(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(距离|里程)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.queryDistanceFun(), 0.5, self.conditionDistanceFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.05, ""),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "rate": rate
                }

    def subTag(self, name, rate, compareRate=0):
        return {"nameFun": lambda x, pos: self.tagFun(self.F.random_int(1, 3)),
                "valueFun": lambda x, pos: "",
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为|做为|当做|做)"), 0.5, ""),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "noeach": True,
                "rate": rate,
                "pos": [0, 1, 2],
                "noLe": True,
                "noDe": True,
                "compareRate": compareRate
                }

    # def subFuzzyTime(self, name, rate, compareRate=0):
    #     return {"nameFun": lambda x, pos: self.fuzzyTimeFun(),
    #             "valueFun": lambda x, pos: "",
    #             "actFun": lambda x, pos: "",
    #             "nocomma": self.F.maybe(True, 0.5),
    #             "groupName": name,
    #             "noeach": True,
    #             "rate": rate,
    #             "pos": [0, 2],
    #             "noLe": True,
    #             "noDe": True,
    #             "compareRate": compareRate
    #             }

    def subPrice(self, name, rate, compareRate=0):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(价格|价钱|单价|(销)?售价|要价|叫价|友情价|租金)"), 0.5, ""),
                "valueFun": lambda x, pos: self.priceFun(),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为|高达|低至)"), 0.1, ""),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "noeach": True,
                "rate": rate,
                "pos": [],
                "noLe": None,
                "noDe": None,
                "compareRate": compareRate
                }

    def subStopword(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.stopwordFun(),
                "actFun": lambda x, pos: "",
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "rate": rate
                }

    def subCompany(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.companyFun(),
                "actFun": lambda x, pos: "",
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "rate": rate
                }


class Diet(Base):
    def __init__(self, this, R, F, X):
        super().__init__(this, R, F, X)

    def randomData(self):
        type = self.F.random_element3(
            ['food', 'homedish', 'drug', 'smoke', 'water'], [5, 5, 3, 3, 1])
        if type == 'food':
            data = list(map(lambda key: self.R['foodMap'][key], self.F.random_choices(
                list(self.R['foodMap'].keys()), 25)))
            self.randomMap = self.F.randomMap(data, groupCnt=1, itemCnt=5, groupRate=[
                1], itemRate=[0.5, 0.3, 0.1, 0.05, 0.05])
            self.name = 'diet#name'
        if type == 'homedish':
            data = list(map(lambda key: self.R['homedishMap'][key], self.F.random_choices(
                list(self.R['homedishMap'].keys()), 25)))
            self.randomMap = self.F.randomMap(data, groupCnt=1, itemCnt=5, groupRate=[
                1], itemRate=[0.5, 0.3, 0.1, 0.05, 0.05])
            self.name = 'diet#homedishName'
        if type == 'drug':
            data = list(map(lambda key: self.R['drugNameMap'][key], self.F.random_choices(
                list(self.R['drugNameMap'].keys()), 25)))
            self.randomMap = self.F.randomMap(data, groupCnt=1, itemCnt=5, groupRate=[
                1], itemRate=[0.5, 0.3, 0.1, 0.05, 0.05])
            self.name = 'diet#drugName'
        if type == 'smoke':
            data = list(map(lambda key: self.R['smokeNameMap'][key], self.F.random_choices(
                list(self.R['smokeNameMap'].keys()), 25)))
            self.randomMap = self.F.randomMap(data, groupCnt=1, itemCnt=5, groupRate=[
                1], itemRate=[0.5, 0.3, 0.1, 0.05, 0.05])
            self.name = 'diet#smokeName'
        if type == 'water':
            data = list(map(lambda key: self.R['waterNameMap'][key], self.F.random_choices(
                list(self.R['waterNameMap'].keys()), 25)))
            self.randomMap = self.F.randomMap(data, groupCnt=1, itemCnt=5, groupRate=[
                1], itemRate=[0.5, 0.3, 0.1, 0.05, 0.05])
            self.name = 'diet#waterName'
        return self

    def dietPattern0(self):
        actMap = [self.subAmount(name="amount", rate=0.8),
                  self.subPay(name="pay", rate=0.5),
                  self.subDuration(name="duration", rate=0.2),
                  self.subEnergy(name="energy", rate=0.2),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subPerson(name="person", rate=0.3),
                  self.subPlace(name="place", rate=0.2),
                  self.subPlanCommand(name="planCommand", rate=0.5),
                  self.subStime(name="stime", rate=0.5),
                  self.subEtime(name="etime", rate=0.3),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.05),
                  self.subTag(name="tag", rate=0.05),
                  self.subAgg(name="agg", rate=0.3),
                  self.subGroupby(name="groupby", rate=0.3),
                  self.subPrice(name="price", rate=0.3),
                  self.subStopword(name="_stopword", rate=0.1),
                  ]
        subactNum = [3, 9]  # 8

        core = self.genMainActPattern(
            self.randomMap,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.F.maybe(self.F.random_element(
                self.R['brand'])+self.X("(牌?的?)"), 0.1, '')+self.X(x["name"]),
            actFun=lambda x: self.X(self.dietAct),
            itemName=self.name, actName="diet#act")
        return core

    def dietPattern1(self):
        # 吃了一个蛋糕和2根香蕉
        core = [
            [self.X(self.dietAct), "diet#act0"],
            [self.X(self.actLe), "_actle"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMap["group0"][0]["name"]), self.name+'0'],
            [self.X(self.link), "_link"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMap["group0"][0]["name"]), self.name+'0']
        ]
        return core

    def dietPattern2(self):
        # 吃了一个蛋糕和2根香蕉以及3个苹果
        core = [
            [self.X(self.dietAct), "diet#act0"],
            [self.X(self.actLe), "_actle"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMap["group0"][0]["name"]), self.name+'0'],
            [self.X(self.link), "_link"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMap["group0"][0]["name"]), self.name+'0'],
            [self.X(self.link), "_link"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMap["group0"][0]["name"]), self.name+'0']
        ]
        return core

    def dietPattern3(self):
        # 特定食物,如 吃了一个包子
        actMap = [self.subAmountFixUnit(name="amount", rate=1),
                  self.subPay(name="pay", rate=0.5),
                  self.subDuration(name="duration", rate=0.5),
                  self.subEnergy(name="energy", rate=0.5),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subPerson(name="person", rate=0.5),
                  self.subPlace(name="place", rate=0.3),
                  self.subPlanCommand(name="planCommand", rate=0.3),
                  self.subStime(name="stime", rate=0.5),
                  self.subEtime(name="etime", rate=0.3),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.05),
                  self.subTag(name="tag", rate=0.05),
                  self.subAgg(name="agg", rate=0.2),
                  self.subGroupby(name="groupby", rate=0.2)]
        subactNum = [3, 9]
        core = self.genMainActPattern(
            self.randomMap,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X("(包子|面包|花卷|包|卷心菜|大盘鸡)"),
            actFun=lambda x: self.X(self.dietAct),
            itemName=self.name, actName="diet#act")
        return core

    def dietPattern4(self):
        # 强化仅有单位没有数值的表达，如：吃了个包子
        core = [
            [self.X(self.dietAct), "diet#act0"],
            [self.X(self.actLe), "_actle"],
            [self.numUnitFun(onlyUnit=1, onlyNum=0), "amount_value_0"],
            [self.X(self.randomMap["group0"][0]["name"]), self.name+'0'],
        ]
        return core

    def queryDietPattern0(self):
        mainCompareRate = 0
        metricCompareRate = 0
        timeCompareRate = 0
        compareFlag = self.F.random_int(0, 3)
        if compareFlag == 0:
            mainCompareRate = 0
            metricCompareRate = 0
            timeCompareRate = 0
        elif compareFlag == 1:
            mainCompareRate = 1
            metricCompareRate = 0
            timeCompareRate = 0
        elif compareFlag == 2:
            mainCompareRate = 0
            metricCompareRate = 1
            timeCompareRate = 0
        else:
            mainCompareRate = 0
            metricCompareRate = 0
            timeCompareRate = 1

        actMap = [self.subAgg(name="agg", rate=0.5),
                  self.subQueryCommand(name="queryCommand", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subQueryAmount(name="queryAmount", rate=0.5),
                  self.subQueryMetricAmount(
            name="dietMetricAmount", rate=1, compareRate=metricCompareRate),
            self.subQueryDuration(name="queryDuration", rate=0.5),
            self.subQueryPay(name="queryPay", rate=0.5),
            self.subQueryEnergy(name="queryEnergy", rate=0.5),
            self.subDecorate(name="_decorate", rate=0.05),
            self.subPerson(name="person", rate=0.3),
            self.subPlace(name="place", rate=0.3),
            self.subStime(name="stime", rate=0.5,
                          compareRate=timeCompareRate),
            self.subEtime(name="etime", rate=0.3),
            #self.subFuzzyTime(name="fuzzyTime", rate=0.05),
            self.subTag(name="tag", rate=0.05),
            self.subStopword(name="_stopword", rate=0.1),
            #self.subQueryTime(name="queryTime", rate=0.5),
        ]
        subactNum = [3, 10]  # [9,9]
        core = self.genMainActPattern(
            self.randomMap,
            mainCompareRate=mainCompareRate,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.F.maybe(self.F.random_element(
                self.R['brand'])+self.X("(牌?的?)"), 0.1, '')+self.X(x["name"]),
            actFun=lambda x: self.X(self.dietAct),
            itemName="query#"+self.name, actName="query#diet#act")

        return core

    def queryDietPattern1(self):
        actMap = [self.subQueryCommand(name="queryCommand", rate=0.5),
                  self.subAmount(name="amount", rate=1),
                  self.subQueryMetricAmount(
            name="dietMetricAmount", rate=1, compareRate=0),
            self.subQueryEnergy(name="queryEnergy", rate=0.5),
            self.subDecorate(name="_decorate", rate=0.05),
            self.subTag(name="tag", rate=0.05),
            self.subStopword(name="_stopword", rate=0.1),
            #self.subQueryTime(name="queryTime", rate=0.5),
        ]
        subactNum = [3, 7]  # [9,9]
        core = self.genMainActPattern(
            self.randomMap,
            mainCompareRate=0,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.F.maybe(self.F.random_element(
                self.R['brand'])+self.X("(牌?的?)"), 0.1, '')+self.X(x["name"]),
            actFun=lambda x: self.F.maybe(self.X(self.dietAct), 0.5, ""),
            itemName="query#"+self.name, actName="query#diet#act")

        return core

    def mark(self, mode):
        pattern = self.F.random_element3(
            [self.dietPattern0, self.dietPattern1,
                self.dietPattern2, self.dietPattern3, self.dietPattern4],
            [65, 10, 10, 5, 5])()
        # [95, 0, 0, 5])()
        try:
            marked = self.this.mark_entity(
                list(map(lambda x: x[0], pattern)),
                list(map(lambda x: x[1], pattern)), mode)
        except:
            print("pattern:", pattern)
        return marked

    def queryMark(self, mode):
        pattern = self.F.random_element3(
            [self.queryDietPattern0, self.queryDietPattern1],
            [0.7, 0.3])()
        marked = self.this.mark_entity(
            list(map(lambda x: x[0], pattern)),
            list(map(lambda x: x[1], pattern)), mode)
        return marked


class Sport(Base):
    def randomData(self):
        data = list(map(lambda key: self.R['sportMap'][key], self.F.random_sample(
            list(self.R['sportMap'].keys()), 5)))
        self.randomMaps = self.F.randomMap(data, groupCnt=1, itemCnt=1, groupRate=[
            1], itemRate=[0.1])
        return self

    def sportPattern0(self):
        actMap = [self.subAmount(name="amount", rate=0.5),
                  self.subPay(name="pay", rate=0.5),
                  self.subDuration(name="duration", rate=0.8),
                  self.subEnergy(name="energy", rate=0.5),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subPerson(name="person", rate=0.5),
                  self.subPlace(name="place", rate=0.3),
                  self.subDistance(name="distance", rate=0.8),
                  self.subPlanCommand(name="planCommand", rate=0.5),
                  self.subStime(name="stime", rate=0.5),
                  self.subEtime(name="etime", rate=0.5),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.05),
                  self.subTag(name="tag", rate=0.05),
                  self.subAgg(name="agg", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subStopword(name="_stopword", rate=0.1),
                  ]
        subactNum = [3, 9]  # 10

        core = self.genMainActPattern(
            self.randomMaps, actRate=1, derRate=0.7,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="sport#name", actName="sport#act")

        return core

    def querySportPattern0(self):
        mainCompareRate = 0
        timeCompareRate = 0
        compareFlag = self.F.random_int(0, 2)
        if compareFlag == 0:
            mainCompareRate = 0
            timeCompareRate = 0
        elif compareFlag == 1:
            mainCompareRate = 1
            timeCompareRate = 0
        else:
            mainCompareRate = 0
            timeCompareRate = 1
        actMap = [self.subAgg(name="agg", rate=0.5),
                  self.subQueryCommand(name="queryCommand", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subQueryAmount(name="queryAmount", rate=0.8),
                  self.subQueryDuration(
            name="queryDuration", rate=0.8),
            self.subQueryPay(name="queryPay", rate=0.8),
            self.subQueryEnergy(name="queryEnergy", rate=0.8),
            self.subQueryDistance(
            name="queryDistance", rate=0.8),
            self.subDecorate(name="_decorate", rate=0.05),
            self.subPerson(name="person", rate=0.5),
            self.subPlace(name="place", rate=0.3),
            self.subStime(name="stime", rate=0.5,
                          compareRate=timeCompareRate),
            self.subEtime(name="etime", rate=0.5),
            #self.subFuzzyTime(name="fuzzyTime", rate=0.03),
            self.subTag(name="tag", rate=0.05),
            self.subStopword(name="_stopword", rate=0.1),
            #self.subQueryTime(name="queryTime", rate=0.5),
        ]
        subactNum = [3, 10]
        core = self.genMainActPattern(
            self.randomMaps, actRate=1, derRate=0.7,
            mainCompareRate=mainCompareRate,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(x[0]['act']),
            itemName="query#sport#name", actName="query#sport#act")

        return core

    def mark(self, mode):
        pattern = self.F.random_element3(
            [self.sportPattern0
             ],
            [1])()
        try:
            marked = self.this.mark_entity(
                list(map(lambda x: x[0], pattern)),
                list(map(lambda x: x[1], pattern)), mode)
        except:
            print("pattern:", pattern)
        return marked

    def queryMark(self, mode):
        pattern = self.F.random_element3(
            [self.querySportPattern0
             ],
            [1])()
        marked = self.this.mark_entity(
            list(map(lambda x: x[0], pattern)),
            list(map(lambda x: x[1], pattern)), mode)
        return marked


class Sign(Base):
    def __init__(self, this, R, F, X):
        super().__init__(this, R, F, X)

    def randomData(self):
        data = list(map(lambda key: self.R['signMap'][key], self.F.random_sample(
            list(self.R['signMap'].keys()), 5)))
        self.randomMaps = self.F.randomMap(data, groupCnt=1, itemCnt=1, groupRate=[
            1], itemRate=[1])
        return self

    def signNumUnit(self, x):
        unit = self.F.maybe(self.X(x['unit']), 0.5)
        num = self.F.random_number3(
            x['num'][0], x['num'][1], 'any')
        numUnit = f"{num}{unit}{self.X('(半|[0-9零一二三四五六七八九][0-9零一二三四五六七八九]?)?')}"
        return numUnit

    def querySignNumUnit(self, x):
        unit = self.F.maybe(self.X(x['unit']), 0.5)
        num = self.X(f"({self.howmany})")
        numUnit = f"{num}{unit}"
        return numUnit

    def subSignAmount(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(结果|数值|数量)"), 0.1, ""),
                "valueFun": lambda x, pos: f"{self.signNumUnit(x[0])}",
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.1, ""),
                "groupName": name,
                "rate": rate
                }

    def subSignAmountRange(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(结果|数值|数量)"), 0.1, ""),
                "valueFun": lambda x, pos: f"{self.signNumUnit(x[0])}{self.X('-|到|至|~|～')}{self.signNumUnit(x[0])}",
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.1, ""),
                "groupName": name,
                "rate": rate
                }

    def subQuerySignAmount(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(结果|数值|数量)"), 0.1, ""),
                "valueFun": lambda x, pos: f"{self.querySignNumUnit(x[0])}",
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.1, ""),
                "groupName": name,
                "rate": rate
                }

    def _sign(self):
        timeNo = self.F.random_element3([1, 2], [0.7, 0.3])
        if (timeNo == 1):
            time1 = self.anyTimeFun()
            time2 = ""
        else:
            time1 = ""
            time2 = self.anyTimeFun()
        pos = self.X(self.pos) if time2 != "" else ""
        breaker = [self.X(self.breaker),
                   self.X(self.breaker),
                   self.X(self.breaker)
                   ]
        der = self.X(self.der)
        return {
            "time1": time1,
            "time2": time2,
            "pos": pos,
            "breaker": breaker,
            "der": der
        }

    def signPattern0(self):
        # 模版1:下午三点体重77.7公斤 || 下午三点77公斤的体重
        core = self.genMainActPattern(
            self.randomMaps, actRate=0.2, derRate=1, subactNum=[3, 3],
            subactMap=[
                self.F.maybe(
                    self.subSignAmount(name="amount", rate=1),
                    0.7,
                    self.subSignAmountRange(name="amountRange", rate=1)
                ),
                self.subStime(name="stime", rate=0.5),
                self.subPlanCommand(name="planCommand", rate=0.5),
                self.subAgg(name="agg", rate=0.5),
                self.subGroupby(name="groupby", rate=0.5),
                self.subStopword(name="_stopword", rate=0.1),
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(self.signAct),
            itemName="sign#name", actName="sign#act")

        return core

    def querySignPattern0(self):
        mainCompareRate = 0
        timeCompareRate = 0
        compareFlag = self.F.random_int(0, 3)
        if compareFlag == 0:
            mainCompareRate = 0
            timeCompareRate = 0
        elif compareFlag == 1:
            mainCompareRate = 1
            timeCompareRate = 0
        else:
            mainCompareRate = 0
            timeCompareRate = 1
        actMap = [self.subQueryCommand(name="queryCommand", rate=0.5),
                  self.subAgg(name="agg", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subQuerySignAmount(name="queryAmount", rate=1),
                  self.subPerson(name="person", rate=0.5),
                  self.subPlace(name="place", rate=0.3),
                  self.subStime(name="stime", rate=0.5,
                                compareRate=timeCompareRate),
                  self.subEtime(name="etime", rate=0.5),
                  self.subStopword(name="_stopword", rate=0.1),
                  #self.subQueryTime(name="queryTime", rate=0.5),
                  ]
        subactNum = None  # [8, 8]

        core = self.genMainActPattern(
            self.randomMaps, actRate=0.2, derRate=1,
            mainCompareRate=mainCompareRate,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(self.signAct),
            itemName="query#sign#name", actName="query#sign#act")
        return core

    def mark(self, mode):
        pattern = self.F.random_element3(
            [self.signPattern0],
            [1])()
        marked = self.this.mark_entity(
            list(map(lambda x: x[0], pattern)),
            list(map(lambda x: x[1], pattern)), mode)
        return marked

    def queryMark(self, mode):
        pattern = self.F.random_element3(
            [self.querySignPattern0],
            [1])()
        marked = self.this.mark_entity(
            list(map(lambda x: x[0], pattern)),
            list(map(lambda x: x[1], pattern)), mode)
        return marked


class Buy(Base):
    def __init__(self, this, R, F, X):
        super().__init__(this, R, F, X)

    def randomData(self):
        data = list(map(lambda key: self.R['buyMap'][key], self.F.random_sample(
            list(self.R['buyMap'].keys()), 5)))
        self.randomMaps = self.F.randomMap(data, groupCnt=1, itemCnt=5, groupRate=[
            1], itemRate=[0.5, 0.3, 0.1, 0.05, 0.05])
        return self

    def buyPattern0(self):
        actMap = [self.subAmount(name="amount", rate=0.7),
                  self.subPay(name="pay", rate=0.8),
                  self.subDuration(name="duration", rate=0.5),
                  self.subEnergy(name="energy", rate=0.5),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subPerson(name="person", rate=0.5),
                  self.subPlace(name="place", rate=0.3),
                  self.subPlanCommand(name="planCommand", rate=0.5),
                  self.subStime(name="stime", rate=0.5),
                  self.subEtime(name="etime", rate=0.5),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.03),
                  self.subTag(name="tag", rate=0.05),
                  self.subAgg(name="agg", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subPrice(name="price", rate=0.3),
                  self.subStopword(name="_stopword", rate=0.1),
                  ]
        subactNum = [3, 9]  # 9

        core = self.genMainActPattern(
            self.randomMaps, actRate=0.2, derRate=0.7,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.F.maybe(self.F.random_element(
                self.R['brand'])+self.X("(牌?的?)"), 0.1, '')+self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="buy#name", actName="buy#act")

        return core

    def buyPattern1(self):
        core = [
            [self.X(self.payAct), "buy#act0"],
            [self.X(self.actLe), "_actle"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMaps["group0"][0]["name"]), "buy#name0"],
            [self.X(self.link), "_link"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMaps["group0"][0]["name"]), "buy#name0"]
        ]
        return core

    def buyPattern2(self):
        core = [
            [self.X(self.payAct), "buy#act0"],
            [self.X(self.actLe), "_actle"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMaps["group0"][0]["name"]), "buy#name0"],
            [self.X(self.link), "_link"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMaps["group0"][0]["name"]), "buy#name0"],
            [self.X(self.link), "_link"],
            [self.numUnitFun(), "amount_value_0"],
            [self.X(self.randomMaps["group0"][0]["name"]), "buy#name0"]
        ]
        return core

    def buyPattern3(self):
        actMap = [self.subAmountFixUnit(name="amount", rate=0.8),
                  self.subPay(name="pay", rate=0.8),
                  self.subDuration(name="duration", rate=0.5),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subPerson(name="person", rate=0.5),
                  self.subPlace(name="place", rate=0.3),
                  self.subPlanCommand(name="planCommand", rate=0.5),
                  self.subStime(name="stime", rate=0.5),
                  self.subEtime(name="etime", rate=0.3),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.03),
                  self.subTag(name="tag", rate=0.05),
                  self.subAgg(name="agg", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5)
                  ]
        subactNum = [3, 9]
        core = self.genMainActPattern(
            self.randomMaps, actRate=0.2, derRate=0.7,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X("(包子|面包|花卷)"),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="buy#name", actName="buy#act")
        return core

    def queryBuyPattern0(self):
        mainCompareRate = 0
        timeCompareRate = 0
        compareFlag = self.F.random_int(0, 3)
        if compareFlag == 0:
            mainCompareRate = 0
            timeCompareRate = 0
        elif compareFlag == 1:
            mainCompareRate = 1
            timeCompareRate = 0
        else:
            mainCompareRate = 0
            timeCompareRate = 1
        actMap = [self.subAgg(name="agg", rate=0.5),
                  self.subQueryCommand(name="queryCommand", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subQueryAmount(name="queryAmount", rate=0.8),
                  self.subQueryDuration(name="queryDuration", rate=0.8),
                  self.subQueryPay(name="queryPay", rate=0.8),
                  self.subQueryEnergy(name="queryEnergy", rate=0.8),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subPerson(name="person", rate=0.5),
                  self.subPlace(name="place", rate=0.3),
                  self.subStime(name="stime", rate=0.5,
                                compareRate=timeCompareRate),
                  self.subEtime(name="etime", rate=0.5),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.03),
                  self.subTag(name="tag", rate=0.05),
                  self.subStopword(name="_stopword", rate=0.1),
                  #self.subQueryTime(name="queryTime", rate=0.5),
                  ]
        subactNum = [3, 10]  # [8, 8]

        core = self.genMainActPattern(
            self.randomMaps,  actRate=0.2, derRate=0.7,
            mainCompareRate=mainCompareRate,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.F.maybe(self.F.random_element(
                self.R['brand'])+self.X("(牌?的?)"), 0.1, '')+self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="query#buy#name", actName="query#buy#act")

        return core

    def mark(self, mode):
        pattern = self.F.random_element3(
            [
                self.buyPattern0,
                self.buyPattern1,
                self.buyPattern2,
                self.buyPattern3
            ],
            [75, 10, 10, 5])()
        try:
            marked = self.this.mark_entity(
                list(map(lambda x: x[0], pattern)),
                list(map(lambda x: x[1], pattern)), mode)
        except:
            print("pattern:", pattern)
        return marked

    def queryMark(self, mode):
        pattern = self.F.random_element3(
            [
                self.queryBuyPattern0
            ],
            [1])()
        marked = self.this.mark_entity(
            list(map(lambda x: x[0], pattern)),
            list(map(lambda x: x[1], pattern)), mode)
        return marked


class Behavior(Base):
    def __init__(self, this, R, F, X):
        super().__init__(this, R, F, X)

    def randomData(self):
        try:
            data = list(map(lambda key: self.R['behaviorMap'][key], self.F.random_sample(
                list(self.R['behaviorMap'].keys()), 5)))
            self.randomMaps = self.F.randomMap(data, groupCnt=1, itemCnt=1, groupRate=[
                1], itemRate=[1])
            return self
        except:
            print("????", data)

    def behaviorPattern0(self):
        actMap = [self.subAmount(name="amount", rate=0.7),
                  self.subPay(name="pay", rate=0.5),
                  self.subDuration(name="duration", rate=0.8),
                  self.subEnergy(name="energy", rate=0.5),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subPerson(name="person", rate=0.5),
                  self.subPlace(name="place", rate=0.3),
                  self.subPlanCommand(name="planCommand", rate=0.5),
                  self.subStime(name="stime", rate=0.5),
                  self.subEtime(name="etime", rate=0.5),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.3),
                  self.subTag(name="tag", rate=0.05),
                  self.subAgg(name="agg", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subStopword(name="_stopword", rate=0.1),
                  ]
        subactNum = [3, 9]  # 9

        core = self.genMainActPattern(
            self.randomMaps, actRate=1, derRate=0.7,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="behavior#name", actName="behavior#act")
        return core

    def queryBehaviorPattern0(self):
        mainCompareRate = 0
        timeCompareRate = 0
        compareFlag = self.F.random_int(0, 3)
        if compareFlag == 0:
            mainCompareRate = 0
            timeCompareRate = 0
        elif compareFlag == 1:
            mainCompareRate = 1
            timeCompareRate = 0
        else:
            mainCompareRate = 0
            timeCompareRate = 1
        actMap = [self.subAgg(name="agg", rate=0.5),
                  self.subQueryCommand(name="queryCommand", rate=0.5),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subQueryAmount(name="queryAmount", rate=0.5),
                  self.subQueryDuration(name="queryDuration", rate=0.5),
                  self.subQueryPay(name="queryPay", rate=0.5),
                  self.subQueryEnergy(name="queryEnergy", rate=0.5),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subPerson(name="person", rate=0.5),
                  self.subPlace(name="place", rate=0.3),
                  self.subStime(name="stime", rate=0.5,
                                compareRate=timeCompareRate),
                  self.subEtime(name="etime", rate=0.5),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.03),
                  self.subTag(name="tag", rate=0.05),
                  self.subStopword(name="_stopword", rate=0.1),
                  #self.subQueryTime(name="queryTime", rate=0.5),
                  ]
        subactNum = [3, 10]  # [9, 14]
        core = self.genMainActPattern(
            self.randomMaps, actRate=1, derRate=0.7,
            mainCompareRate=mainCompareRate,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="query#behavior#name", actName="query#behavior#act")
        return core

    def mark(self, mode):
        pattern = self.F.random_element3(
            [
                self.behaviorPattern0
            ],
            [1])()
        try:
            marked = self.this.mark_entity(
                list(map(lambda x: x[0], pattern)),
                list(map(lambda x: x[1], pattern)), mode)
        except:
            print("pattern:", pattern)
        return marked

    def queryMark(self, mode):
        pattern = self.F.random_element3(
            [
                self.queryBehaviorPattern0
            ],
            [1])()
        marked = self.this.mark_entity(
            list(map(lambda x: x[0], pattern)),
            list(map(lambda x: x[1], pattern)), mode)
        return marked


class Stock(Base):
    def __init__(self, this, R, F, X):
        super().__init__(this, R, F, X)

    def randomData(self):
        try:
            data = [self.R['stockMap']['metric']]
            self.randomMaps = self.F.randomMap(data, groupCnt=1, itemCnt=1, groupRate=[
                1], itemRate=[1])
            return self
        except:
            print("????????????", self.R['stockMap']['metric'])

    def subStockAmount(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(结果|数值|数量)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.payNumFun(n=1), 0.5, self.conditionPayNumFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.1, ""),
                "groupName": name,
                "rate": rate
                }

    def subQueryStockAmount(self, name, rate):
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(结果|数值|数量)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.queryPayNumFun(), 0.5, self.conditionPayNumFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X("(是|为)"), 0.1, ""),
                "groupName": name,
                "rate": rate
                }

    def stockPattern0(self):
        actMap = [
            self.subStockAmount(name="stockAmount", rate=1),
            self.subCompany(name="company", rate=1),
            self.subDecorate(name="_decorate", rate=0.05),
            self.subPlanCommand(name="planCommand", rate=0.5),
            self.subStime(name="stime", rate=0.5),
            self.subEtime(name="etime", rate=0.5),
            self.subAgg(name="agg", rate=0.5),
            self.subGroupby(name="groupby", rate=0.5),
            self.subStopword(name="_stopword", rate=0.1),
        ]
        subactNum = [3, 9]  # 9

        core = self.genMainActPattern(
            self.randomMaps, actRate=1, derRate=0.7,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: "",
            itemName="stock#name", actName="stock#act")
        return core

    def queryStockPattern0(self):
        mainCompareRate = 0
        timeCompareRate = 0
        compareFlag = self.F.random_int(0, 3)
        if compareFlag == 0:
            mainCompareRate = 0
            timeCompareRate = 0
        elif compareFlag == 1:
            mainCompareRate = 1
            timeCompareRate = 0
        else:
            mainCompareRate = 0
            timeCompareRate = 1
        actMap = [self.subAgg(name="agg", rate=0.5),
                  self.subQueryCommand(name="queryCommand", rate=0.5),
                  self.subCompany(name="company", rate=1),
                  self.subGroupby(name="groupby", rate=0.5),
                  self.subQueryStockAmount(name="queryStockAmount", rate=1),
                  self.subDecorate(name="_decorate", rate=0.05),
                  self.subStime(name="stime", rate=0.5,
                                compareRate=timeCompareRate),
                  self.subEtime(name="etime", rate=0.5),
                  #self.subFuzzyTime(name="fuzzyTime", rate=0.03),
                  self.subStopword(name="_stopword", rate=0.1),
                  #self.subQueryTime(name="queryTime", rate=0.5),
                  ]
        subactNum = [3, 10]  # [9, 14]
        core = self.genMainActPattern(
            self.randomMaps, actRate=1, derRate=0.7,
            mainCompareRate=mainCompareRate,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: "",
            itemName="query#stock#name", actName="query#stock#act")
        return core

    def mark(self, mode):
        pattern = self.F.random_element3(
            [
                self.stockPattern0
            ],
            [1])()
        try:
            marked = self.this.mark_entity(
                list(map(lambda x: x[0], pattern)),
                list(map(lambda x: x[1], pattern)), mode)
        except:
            print("pattern:", pattern)
        return marked

    def queryMark(self, mode):
        pattern = self.F.random_element3(
            [
                self.queryStockPattern0
            ],
            [1])()
        try:
            marked = self.this.mark_entity(
                list(map(lambda x: x[0], pattern)),
                list(map(lambda x: x[1], pattern)), mode)
        except:
            print("pattern:", pattern)
        return marked


class Feel(Base):
    def __init__(self, this, R, F, X):
        super().__init__(this, R, F, X)

    def randomData(self):
        try:
            self.feelState = "(有点|特别|很|十分|非常|稍微有点|特|奇|太|剧烈)?(钝|绞|胀|刺|隐隐|酸)?(肿|麻|痛|疼|痒|闷|快|慢|眩晕|晕|热|烫|冷|凉|不舒服)((的|得|地)(厉害|很))?"
            return self
        except:
            print("????????????", "Fell error")

    def subFeelCommand(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.X(self.feelCommand),
                "actFun": lambda x, pos: "",
                "groupName": name,
                "rate": rate,
                "nocomma": self.F.maybe(True, 0.5),
                "noeach": True
                }

    def subFeelState(self, name, rate):
        return {"nameFun": lambda x, pos: "",
                "valueFun": lambda x, pos: self.X(self.feelState),
                "actFun": lambda x, pos: "",
                "groupName": name,
                "rate": rate,
                "pos": [2]
                }

    def subFeelDuration(self, name, rate, n=None):
        #n = self.F.random_int(1, 3)
        return {"nameFun": lambda x, pos: self.F.maybe(self.X("(时间|光景)"), 0.1, ""),
                "valueFun": lambda x, pos: self.F.maybe(self.durationFun(n=n), 0.5, self.conditionDurationFun()),
                "actFun": lambda x, pos: self.F.maybe(self.X("(持续了|疼了|痛了|痒了)"), 0.1, ""),
                "nocomma": self.F.maybe(True, 0.5),
                "groupName": name,
                "rate": rate,
                "pos": [2]
                }

    def feelPattern0(self):
        actMap = [
            self.subDecorate(name="_decorate", rate=0.05),
            self.subFeelCommand(name="feelCommand", rate=0.5),
            self.subFeelState(name="feelState", rate=1),
            self.subFeelDuration(name="duration", rate=0.5),
            self.subStime(name="stime", rate=0.5),
            self.subEtime(name="etime", rate=0.5),
            self.subStopword(name="_stopword", rate=0.1),
        ]
        subactNum = None  # 9

        core = self.genMainActPattern(
            {"group": [1]}, actRate=1, derRate=0.7,
            subactNum=subactNum,
            subactMap=actMap,
            itemFun=lambda x: self.X(self.body),
            actFun=lambda x: "",
            itemName="feel#name", actName="feel#act")
        return core

    def mark(self, mode):
        pattern = self.F.random_element3(
            [
                self.feelPattern0
            ],
            [1])()
        try:
            marked = self.this.mark_entity(
                list(map(lambda x: x[0], pattern)),
                list(map(lambda x: x[1], pattern)), mode)
        except:
            print("pattern:", pattern)
        return marked

    def queryMark(self, mode):
        pattern = self.F.random_element3(
            [
                self.feelPattern0
            ],
            [1])()
        try:
            marked = self.this.mark_entity(
                list(map(lambda x: x[0], pattern)),
                list(map(lambda x: x[1], pattern)), mode)
        except:
            print("pattern:", pattern)
        return marked


class NewParser(Base):
    def __init__(self, this, R, F, X):
        super().__init__(this, R, F, X)

    def randomData(self):
        data = list(map(lambda key: self.R['foodMap'][key], self.F.random_choices(
            list(self.R['foodMap'].keys()), 25)))
        self.randomMaps = self.F.randomMap(data, groupCnt=5, itemCnt=5, groupRate=[
            0.5, 0.3, 0.1, 0.05, 0.05], itemRate=[0.3, 0.3, 0.3, 0.1, 0.1])
        return self

    def getPattern0(self):
        core = self.genMainActPatternNew(
            self.randomMaps, actRate=1, derRate=0.7,
            subactNum=3,
            subactMap=[
                {"nameFun": lambda x, pos: self.X("(结果|数值|数量)?"),
                    "valueFun": lambda x, pos: self.numUnitFun(),
                    "actFun": lambda x, pos: self.X(self.be),
                    "groupName": "amount",
                    "rate": 1
                 },
                {"nameFun": lambda x, pos: self.X("(时间|光景)?"),
                    "valueFun": lambda x, pos: self.durationFun(),
                    "actFun": lambda x, pos: self.X(self.spendAct),
                    "groupName": "duration",
                    "rate": 1
                 },
                {"nameFun": lambda x, pos: self.X("钱?"),
                    "valueFun": lambda x, pos: self.payNumFun(),
                    "actFun": lambda x, pos: self.X(self.payAct),
                    "groupName": "pay",
                    "rate": 0.6
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="name", actName="act")
        pattern = self.assembleCore(core)
        return pattern

    def mark(self, mode):
        pattern = self.F.random_element3(
            [
                self.getPattern0
            ],
            [1])()
        marked = self.this.mark_entity(
            list(map(lambda x: x[0], pattern)),
            list(map(lambda x: x[1], pattern)), mode)
        return marked


class Query(Base):
    def __init__(self, this, R, F, X):
        super().__init__(this, R, F, X)

    def randomData(self):
        data1 = list(map(lambda key: self.R['foodMap'][key], self.F.random_sample(
            list(self.R['foodMap'].keys()), 2)))
        self.randomMaps1 = self.F.randomMap(data1, groupCnt=1, itemCnt=2, groupRate=[
            1], itemRate=[0.5, 0.5])

        dataMap2 = {"nutrion": {
            "act": "(吃|吸收|摄入)",
            "name": "(卡路里|能量|热量|蛋白质|脂肪|碳水|碳水化合物|盐|钠)",
            "subclass": "metric"
        },
            "water": {
            "act": "(喝|饮用|吸收|摄入)",
            "name": "(水)",
            "subclass": "metric"
        },
            "vetamin": {
            "act": "(吃|吸收|摄入)",
            "name": "((维生素|维他命)[A-Ka-k]|[vV维][A-Ka-k]|[vV维][Bb][1-9][1-9]?|[bB]族)",
            "subclass": "metric"
        }

        }
        data2 = list(map(lambda key: dataMap2[key], self.F.random_sample(
            list(dataMap2.keys()), 3)))
        self.randomMaps2 = self.F.randomMap(data2, groupCnt=2, itemCnt=2, groupRate=[
            0, 1], itemRate=[1, 0])

        dataMap3 = {"o1": {
            "act": "(跑)",
            "name": "(步)",
            "subclass": "sport"
        }
        }
        data3 = list(map(lambda key: dataMap3[key], self.F.random_sample(
            list(dataMap3.keys()), 1)))
        self.randomMaps3 = self.F.randomMap(data3, groupCnt=1, itemCnt=1, groupRate=[
            1], itemRate=[1])
        dataMap4 = {"o1": {
            "act": "(买|购买|消费)",
            "name": "(蛋糕|香蕉|包子)",
            "subclass": "buy"
        },
            "o2": {
            "act": "(买|购买|消费)",
            "name": "(旺旺雪饼|西北拉面)",
            "subclass": "buy"
        }
        }
        data4 = list(map(lambda key: dataMap4[key], self.F.random_sample(
            list(dataMap4.keys()), 1)))
        self.randomMaps4 = self.F.randomMap(data4, groupCnt=1, itemCnt=1, groupRate=[
            1], itemRate=[1])

        return self

    def queryPattern0(self):
        core = self.genMainActPattern(
            self.randomMaps1,  actRate=1, derRate=0.7,
            subactNum=1,
            subactMap=[

                {"nameFun": lambda x: self.X(x[0]['name']),
                    # "valueFun": lambda x: self.queryNumUnitFun(),
                    ############################
                    "valueFun": lambda x: (self.genMainActPattern(
                        self.randomMaps2, actRate=1, derRate=0.7,
                        subactNum=1,
                        actMap=[
                            {"nameFun": lambda x, pos: "",
                             # "valueFun": lambda x: self.queryNumUnitFun(),
                             # *************************
                             "valueFun": lambda x, pos: self.genMainActPattern(
                                 self.randomMaps3, actRate=1, derRate=0.7,
                                 subactNum=1,
                                 actMap=[
                                     {"nameFun": lambda x, pos: "",
                                         # "valueFun": lambda x,pos: self.queryNumUnitFun(),
                                         "valueFun": lambda x, pos: self.queryNumUnitFun(),
                                         "actFun": lambda x, pos: "",
                                         "groupName": "L2_Q_NU_",
                                         "rate": 1,
                                         "pos": [],
                                      }
                                 ],
                                 itemFun=lambda x: self.X(x['name']),
                                 actFun=lambda x: self.X(f"{x[0]['act']}"),
                                 itemName="L2_Name", actName="L2_Act"),
                             # ***************************
                             "actFun": lambda x, pos: "",
                             "groupName": "L1_Q_NU_",
                             "rate": 1
                             }
                        ],
                        itemFun=lambda x: self.X(x['name']),
                        actFun=lambda x: self.X(f"{x[0]['act']}"),
                        itemName="L1_Name", actName="L1_Act"))+[[self.conditionNumUnitFun(), "L1_conditon"]]+[["那天", "L1_groupby"]],
                    ######################
                    "actFun": lambda x: self.X(f"{x[0]['act']}"),
                    "groupName": "L0_Q_NU_",
                    "rate": 1
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="L0_Name", actName="L0_Act")
        pattern = self.assembleCore(core)
        return pattern

    def queryPattern1(self):
        core = self.genMainActPattern(
            self.randomMaps1,  actRate=1, derRate=0.5,
            subactNum=1,
            subactMap=[
                {"nameFun": lambda x, pos, data=self.randomMaps2['group0']: self.X(data[0]['name']),
                 "valueFun": lambda x, pos: self.queryNumUnitFun(),
                    "actFun": lambda x, pos, data=self.randomMaps2['group0']: self.X(data[0]['act']),
                    "groupName": "L0_condition",
                    "rate": 1
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="L0_Name", actName="L0_Act")
        pattern = self.assembleCore(core)
        return pattern

    def queryPattern2(self):
        core1 = self.genMainActPattern(
            self.randomMaps1, actRate=1, derRate=0.5,
            subactNum=1,
            subactMap=[
                {"nameFun": lambda x, pos, data=self.randomMaps2['group0']: self.X(data[0]['name']),
                    "valueFun": lambda x, pos: self.conditionNumUnitFun(),
                    "actFun": lambda x, pos, data=self.randomMaps2['group0']: self.X(data[0]['act']),
                    "groupName": "L0_condition",
                    "rate": 1
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="L0_Name", actName="L0_Act")
        core2 = self.genMainActPattern(
            self.randomMaps3, actRate=1, derRate=0.5,
            subactNum=1,
            subactMap=[
                {"nameFun": lambda x, pos: "",
                 "valueFun": lambda x, pos: self.queryNumUnitFun(),
                    "actFun": lambda x, pos: "",
                    "groupName": "L1_condition",
                    "rate": 1
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="L0_Name", actName="L0_Act")
        core = core1 + \
            [["的", "_der"], [self.F.maybe(
                self.X(self.that), 1, ""), "L0_groupby"]] + core2
        pattern = self.assembleCore(core)
        return pattern

    def queryPattern3(self):
        core1 = self.genMainActPattern(
            self.randomMaps1, actRate=1, derRate=0.5,
            subactNum=1,
            subactMap=[
                {"nameFun": lambda x, pos, data=self.randomMaps2['group0']: self.X(data[0]['name']),
                    "valueFun": lambda x, pos: self.conditionNumUnitFun(),
                    "actFun": lambda x, pos, data=self.randomMaps2['group0']: self.X(data[0]['act']),
                    "groupName": "L0_condition",
                    "rate": 1
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="L0_Name", actName="L0_Act")
        core2 = self.genMainActPattern(
            self.randomMaps4, actRate=1, derRate=0.5,
            subactNum=1,
            subactMap=[
                {"nameFun": lambda x, pos, data=self.randomMaps2['group0']:self.X(data[0]['name']),
                    "valueFun": lambda x, pos: self.conditionNumUnitFun(),
                    "actFun": lambda x, pos, data=self.randomMaps2['group0']: self.X(data[0]['act']),
                    "groupName": "L1_condition",
                    "rate": 1
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="L1_Name", actName="L1_Act")
        core3 = self.genMainActPattern(
            self.randomMaps4, actRate=1, derRate=0.5,
            subactNum=1,
            subactMap=[
                {"nameFun": lambda x, pos: "数量",
                 "valueFun": lambda x, pos: self.queryNumUnitFun(),
                    "actFun": lambda x, pos: "吃",
                    "groupName": "L2_condition",
                    "rate": 1
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="L2_Name", actName="L2_Act")

        core = core1 + \
            [["的", "_der"], [self.F.maybe(
                self.X(self.that), 1, ""), "L0_groupby"]] + core2
        core = core + \
            [["的", "_der"], [self.F.maybe(
                self.X(self.that), 1, ""), "L1_groupby"]] + core3
        pattern = self.assembleCore(core)
        return pattern

    def queryPattern4(self):
        core = self.genMainActPattern(
            self.randomMaps2, actRate=1, derRate=0.5,
            subactNum=None, isRelation=True,
            subactMap=[
                {"nameFun": lambda x, pos: "",
                    "valueFun": lambda x, pos: self.F.maybe(self.queryNumUnitFun(), 0, self.conditionNumUnitFun()),
                    "actFun": lambda x, pos: "",
                    "groupName": "L0_condition",
                    "rate": 1,
                    "nocomma": True,
                    "pos": [2]
                 },
                {"nameFun": lambda x, pos: "",
                    "valueFun": lambda x, pos: self.X(self.agg),
                    "actFun": lambda x, pos: "",
                    "groupName": "L0_agg",
                    "rate": 0.5,
                    "nocomma": True,
                    "pos": []
                 }
            ],
            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x: self.X(f"{x[0]['act']}"),
            itemName="L0_Name", actName="L0_Act")

        pattern = self.assembleCore(core)
        return core

    def queryPatternA(self):
        dietData = list(map(lambda key: self.R['foodMap'][key], self.F.random_sample(
            list(self.R['foodMap'].keys()), 5)))
        sportData = list(map(lambda key: self.R['sportMap'][key], self.F.random_sample(
            list(self.R['sportMap'].keys()), 5)))
        buyData = list(map(lambda key: self.R['buyMap'][key], self.F.random_sample(
            list(self.R['buyMap'].keys()), 5)))
        behaviorData = list(map(lambda key: self.R['behaviorMap'][key], self.F.random_sample(
            list(self.R['behaviorMap'].keys()), 5)))
        signData = list(map(lambda key: self.R['signMap'][key], self.F.random_sample(
            list(self.R['signMap'].keys()), 5)))
        data = {}
        groupCnt = self.F.random_int(1, 5)
        for i in range(groupCnt):
            type = self.F.random_element(
                ["diet", "sport", "buy", "behavior", "sign"])
            typeData = self.F.decode(type, ["diet", "sport", "buy", "behavior", "sign"], [
                                     dietData, sportData, buyData, behaviorData, signData])
            data[f"{type}_{i}"] = self.F.random_sample(
                typeData, self.F.random_int(1, 5))
        print(data)
        core = self.genMainActPattern(
            data, actRate=1, derRate=0.5,
            subactNum=None, isRelation=False,
            subactMap=[
                {"nameFun": lambda x, pos: self.F.maybe(self.X("(数值|数量)"), 0.1, ""),
                    "valueFun": lambda x, pos: self.numUnitFun(),
                    "actFun": lambda x, pos: "",
                    "groupName": "behaviorAmount",
                    "nocomma": self.F.maybe(True, 1),
                    "rate": 0.7,
                    "pos": []
                 },
                {"nameFun": lambda x, pos: self.F.maybe(self.X("(时间|光景)"), 0.1, ""),
                    "valueFun": lambda x, pos: self.durationFun(),
                    "actFun": lambda x, pos: self.F.maybe(self.X(self.spendAct), 0.1, ""),
                    "nocomma": self.F.maybe(True, 0.5),
                    "groupName": "behaviorDuration",
                    "rate": 0.5
                 },
                {"nameFun": lambda x, pos: self.F.maybe(self.X("钱"), 0.1, ""),
                    "valueFun": lambda x, pos: self.payNumFun(),
                    "actFun": lambda x, pos: self.F.maybe(self.X(self.payAct), 0.1, ""),
                    "nocomma": self.F.maybe(True, 0.5),
                    "groupName": "behaviorPay",
                    "rate": 0.6
                 },
                {"nameFun": lambda x, pos: self.F.maybe(self.X("热量|能量"), 0.1, ""),
                 "valueFun": lambda x, pos: self.energyFun(),
                 "actFun": lambda x, pos: self.F.maybe(self.X(self.energyAct), 0.1, ""),
                 "groupName": "dietPay",
                 "nocomma": self.F.maybe(True, 0.5),
                 "rate": 0.5,
                 "pos": []
                 },

                {"nameFun": lambda x, pos: "",
                 "valueFun": lambda x, pos: self.X(self.decorate),
                 "actFun": lambda x, pos: "",
                 "nocomma": self.F.maybe(True, 1),
                 "groupName": "_decorate",
                 "rate": 0.05,
                 "pos": []
                 }
            ],            itemFun=lambda x: self.X(x['name']),
            actFun=lambda x, groupName: self.X(f"{x[0]['act']}"),
            itemName="L0_Name_", actName="L0_Act_")

        pattern = self.assembleCore(core)
        return core

    def mark(self, mode):
        pattern = self.F.random_element3(
            [
                self.queryPattern0,
                self.queryPattern1,
                self.queryPattern2,
                self.queryPattern3,
                self.queryPattern4,
                self.queryPatternA
            ],
            [0, 0, 0, 0, 0, 1])()
        marked = self.this.mark_entity(
            list(map(lambda x: x[0], pattern)),
            list(map(lambda x: x[1], pattern)), mode)
        return marked


diet = Diet(self, R, F, X)
sport = Sport(self, R, F, X)
sign = Sign(self, R, F, X)
buy = Buy(self, R, F, X)
behavior = Behavior(self, R, F, X)
query = Query(self, R, F, X)
newParser = NewParser(self, R, F, X)
stock = Stock(self, R, F, X)
feel = Feel(self, R, F, X)

__return__ = {"diet": diet, "sport": sport,
              "sign": sign, "buy": buy, "behavior": behavior,
              "stock": stock, "feel": feel,
              "query": query, "newParser": newParser, "base": Base}
