# coding=utf-8
from lib2to3.pgen2.literals import evalString
import sys
from datetime import datetime
import math
import random
import time
import re
import requests
import json
import itertools
from functools import reduce
from faker import Faker
from faker.providers import BaseProvider
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from pymongo import MongoClient
from flask import Flask, jsonify, request
from requests.models import Response
from collections import OrderedDict
from xeger import Xeger
import cn2an
import lunar_python
from eprogress import LineProgress, CircleProgress, MultiProgressManager

faker = Faker(locale="zh_CN")
app = Flask(__name__)
# 创建自定义的provider


class MyProvider(BaseProvider):
    xeger = Xeger().xeger
    lunar = lunar_python.Lunar
    solar = lunar_python.Solar

    def cname(self, type=None):
        if type == 1:
            return faker.name()[0]+faker.first_name_male()
        elif type == 0:
            return faker.name()[0]+faker.first_name_female()
        else:
            return faker.name()

    def _str2date(self, s):
        if re.match("[\d]{8}", s):
            return datetime.strptime(s, '%Y%m%d')
        if re.match(".*?\-.*?\-", s):
            return datetime.strptime(s, '%Y-%m-%d')
        if re.match(".*?\/.*?\/", s):
            return datetime.strptime(s, '%Y/%m/%d')
        return s

    def datestr_between(self, start, end, format='%Y-%m-%d'):
        # 支持start,end 日期型,日期类型的字符串,"today","+3d","-2h"等类似写法
        start = self._str2date(start)
        end = self._str2date(end)
        return faker.date_between(start, end).strftime(format)

    def _str2time(self, s):
        if re.match(".*?\:.*?\:", s):
            return datetime.strptime(s, '%H:%M:%S')
        if re.match(".*?\:.*?", s):
            return datetime.strptime(s, '%H:%M')
        return s

    def timestr_between(self, start, end=None, format='%H:%M:%S'):
        if type(start) == tuple:
            format = end if end != None else '%H:%M:%S'
            end = start[1]
            start = start[0]
        start = self._str2time(start)
        end = self._str2time(end)
        return faker.date_time_between(start, end).strftime(format)

    def random_date(self, start, end):
        return faker.date_between(start, end)

    def timestr_series(self, start, end, interval='+1d', format='%Y-%m-%d'):
        start = self._str2date(start)
        end = self._str2date(end)
        k = map(lambda x: x[0].strftime(format),
                faker.time_series(start, end, interval))
        return k

    def random_float(self, start, end):
        v = str(start).split(".")
        l1 = 0 if len(v) == 1 else len(v[1])
        v = str(end).split(".")
        l2 = 0 if len(v) == 1 else len(v[1])
        l = l1 if l1 > l2 else l2
        return float(f'%.{l}f' % random.uniform(start, end))

    def random_beta(self, arr, alpha=None, beta=None):
        if not alpha:
            alpha = len(arr)
        if not beta:
            beta = len(arr)
        # 'beta': random.betavariate, # alpha, beta
        return arr[int(random.betavariate(alpha, beta)*len(arr))]

    def random_element1(self, elements):
        newElement = list(map(lambda x: json.dumps(x), elements))
        item = super().random_element(newElement)
        return json.loads(item)

    def random_element2(self, elements, element2=None):
        # 接受三种传参
        # 1. elements 为[[A,r1],[B,r2],[C,r3]...],其中r1,r2,r3,...为相应权重
        # 2. element 为[A,B,C...] , element2为[r1,r2,r3...]
        # 3. element为二维tuple ([A,B,C...],[r1,r2,r3...])
        if type(elements) == tuple:
            newElement = OrderedDict(map(lambda x:
                                         [json.dumps(x[0]), x[1]], zip(elements[0], elements[1])))
        elif (element2 != None and len(element2) == len(elements)):
            newElement = OrderedDict(map(lambda x:
                                     [json.dumps(x[0]), x[1]], zip(elements, element2)))
        else:
            newElement = OrderedDict(map(lambda x:
                                     [json.dumps(x[0]), x[1]], elements))
        print(newElement)
        item = super().random_element(newElement)
        return json.loads(item)

    def random_element3(self, source0, rate=None):
        if type(source0) == tuple:
            rate = source0[1]
            source = source0[0]
        else:
            source = source0
        if rate:
            if len(source) != len(rate):
                raise Exception(
                    "len of rate must equal source")
            # 重算rate
            sum = reduce(lambda x, y: x+y, rate)
            rate = list(map(lambda x: x/sum, rate))
            x = random.random()
            cumulative = 0.0
            for item, p in zip(source, rate):
                cumulative += p
                if x < cumulative:
                    break
            return item
        else:
            return random.choice(source)

    def random_int_range(self, source0, rate=None):
        if type(source0) == tuple:
            rate = source0[1]
            source = source0[0]
        else:
            source = source0

        # 格式Exp. random_int_range([[1,2],[3,5],[6,10]],[0.6,0.3,0.1])
        if rate:
            if len(source) != len(rate):
                raise Exception(
                    "len of rate must equal source")
            # 重算rate
            sum = reduce(lambda x, y: x+y, rate)
            rate = list(map(lambda x: x/sum, rate))
            x = random.random()
            cumulative = 0.0
            for item, p in zip(source, rate):
                cumulative += p
                if x < cumulative:
                    break
            return self.random_int(item[0], item[1])
        else:
            ir = random.choice(source)
            return self.random_int(ir[0], ir[1])

    def random_float_range(self, source0, rate=None):
        if type(source0) == tuple:
            rate = source0[1]
            source = source0[0]
        else:
            source = source0

        # 格式Exp. random_float_range([[1.0,2.01],[3.00,4.99],[6.02,10.18]],[0.6,0.3,0.1])
        if rate:
            if len(source) != len(rate):
                raise Exception(
                    "len of rate must equal source")
            # 重算rate
            sum = reduce(lambda x, y: x+y, rate)
            rate = list(map(lambda x: x/sum, rate))

            x = random.random()
            cumulative = 0.0
            for item, p in zip(source, rate):
                cumulative += p
                if x < cumulative:
                    break
            return self.random_float(item[0], item[1])
        else:
            ir = random.choice(source)
            return self.random_float(ir[0], ir[1])

    def random_PDF(self, mode, alpha, beta):
        if mode == 'beta':
            # 'beta': random.betavariate, # alpha, beta
            return random.betavariate(alpha, beta)
        elif mode == 'gamma':
            # 'gamma': random.gammavariate, # alpha, beta
            return random.gammavariate(alpha, beta)
        elif mode == 'gauss':
            # 'gauss': random.gauss, # mu, sigma
            return random.gauss(alpha, beta)*len(arr)
        elif mode == 'lognorm':
            # 'lognorm': random.lognormvariate, # mu, sigma
            return random.lognormvariate(alpha, beta)
        elif mode == 'normal':
            # 'normal': random.normalvariate, # mu, sigma
            return random.normalvariate(alpha, beta)
        elif mode == 'vonmises':
            # 'vonmises': random.vonmisesvariate, # mu, kappa
            return random.vonmisesvariate(alpha, beta)
        elif mode == 'weibull':
            # 'weibull': random.weibullvariate # alpha, beta
            return random.weibullvariate(alpha, beta)

    def geo_lat(self):
        return float(faker.latitude())

    def geo_lng(self):
        return float(faker.longitude())

    def geo_address(self, lat, lng=None):
        if type(lat) in [tuple, list]:
            lat = lng[0]
            lng = lng[1]
        url = f'http://api.map.baidu.com/geocoder?location={lat},{lng}&output=json&key=f247cdb592eb43ebac6ccd27f796e2d2'
        res = requests.get(url)
        return res.json()

    def geo_location(self, address):
        url = f'http://api.map.baidu.com/geocoder?address={address}&output=json&key=f247cdb592eb43ebac6ccd27f796e2d2'
        res = requests.get(url)
        loc = res.json()
        return (loc["result"]["location"]["lat"], loc["result"]["location"]["lng"])

    def geo_random_address(self, location, radius=0.001):
        # radius默认10公里
        lat = float(faker.coordinate(center=location[0], radius=radius))
        lng = float(faker.coordinate(center=location[1], radius=radius))
        url = f'http://api.map.baidu.com/geocoder?location={lat},{lng}&output=json&key=f247cdb592eb43ebac6ccd27f796e2d2'
        res = requests.get(url)
        return res.json()

    def geo_random_location(self, address, radius=0.001):
        # radius默认10公里
        url = f'http://api.map.baidu.com/geocoder?address={address}&output=json&key=f247cdb592eb43ebac6ccd27f796e2d2'
        res = requests.get(url)
        loc = res.json()
        lng = float(faker.coordinate(
            center=loc["result"]["location"]["lng"], radius=radius))
        lat = float(faker.coordinate(
            center=loc["result"]["location"]["lat"], radius=radius))
        return (lat, lng)

    # 根据id获取出生日期，性别，地区
    def getBirthday(self, s, f="%Y-%m-%d"):
        b = s[6:10]+"-"+s[10:12]+"-"+s[12:14]
        return b

    def getSex(self, s):
        b = int(s[16:17])
        if b % 2 == 0:
            return 0
        return 1

    def getSF(self, s):
        area = {
            '11': '北京市', '12': '天津市', '13': '河北省', '14': '山西省', '15': '内蒙古自治区',
            '21': '辽宁省', '22': '吉林省', '23': '黑龙江省',
            '31': '上海市', '32': '江苏省', '33': '浙江省', '34': '安徽省', '35': '福建省', '36': '江西省', '37': '山东省',
            '41': '河南省', '42': '湖北省', '43': '湖南省',
            '44': '广东省', '45': '广西壮族自治区', '46': '海南省',
            '50': '重庆市', '51': '四川省', '52': '贵州省', '53': '云南省', '54': '西藏自治区',
            '61': '陕西省', '62': '甘肃省', '63': '青海省', '64': '宁夏回族自治区', '65': '新疆维吾尔族自治区',
            '81': '香港特别行政区', '82': '澳门特别行政区', '83': '台湾地区'
        }  # 全国省的字典
        return area[s[0:2]]

    def getDQ(self, s):
        year = s[6:10]
        city = s[0:4]
        qx = s[4:6]
        dqcode = request.get(
            "http://icfs:8081/icfs/bafym3jqbeaefb4zsefl2yfiqbrv2odozadywvow36sbtivbb5erapi3xr7pfm")
        dqcode = json.loads(dqcode.text)
        area = dq[year]  # 地市县的字典
        return area[s[0:4]]

    # 数值转中文，mode支持"low","up","rmb"和normal，默认mode=low。如果mode=normal则为逐字翻译到中文
    def num2cn(self, num, mode="low"):
        code = {"0": "零", "1": "一", "2": "二", "3": "三", "4": "四", "5": "五",
                "6": "六", "7": "七", "8": "八", "9": "九", "-": "负", ".": "点"}
        s = ""
        if (mode == "normal"):
            for i, t in enumerate(f"{num}"):
                s += code[t]
        else:
            s = cn2an.an2cn(num, mode)
        # 随机地将"二" 改为 "两"
        cnnum = s.replace("二", self.maybe("两", 0.5, "二"))
        return cnnum
    # 中文转数值，mode支持"strict","normal","smart"，默认mode=smart

    def cn2num(self, cnStr, mode="smart"):
        return cn2an.cn2an(cnStr, mode)

    # 生成随机整数或小数，精度为0到start/end的最大精度之间随机，默认类型为阿拉伯数。如果中文形式mode="cn",中文逐字形式mode="cnn",任意(非中文逐字)形式mode="any",任意(中文逐字)形式mode="anyn"
    def random_number2(self, start, end, mode="an"):
        f1 = self.random_float(start,
                               end)
        v = str(start).split(".")
        l1 = 0 if len(v) == 1 else len(v[1])
        v = str(end).split(".")
        l2 = 0 if len(v) == 1 else len(v[1])
        l = l1 if l1 > l2 else l2

        acc = self.random_int(0, l)
        if acc == 0:
            f2 = round(f1)
        else:
            f2 = round(f1, acc)
        if (mode == "any"):
            mode = self.random_element(["an", "cn"])
        if (mode == "anyn"):
            mode = self.random_element(["an", "cnn"])

        if (mode == "an"):
            return f"{f2}"
        elif (mode == "cn"):
            cnnum = f"{self.num2cn(f2)}"
            return cnnum
        else:
            return f"{self.num2cn(f2,'normal')}"
    # random_number2的升级版，根据rangeRate的比例选取rangeArr数组，在根据选定的range执行rangdom_number2操作
    # sample:  random_number3([[0,20],[20.01,29.99],[29.99,100]],[0.1,0.8,0.1],"an") 系统将有80%的数据在20.01～29.99之间，其他区段各10%。精度由29.99指定最高小数后2位，仅生成数值形式

    def random_number3(self, rangeArr, rangeRate, mode="an"):
        if len(rangeArr) != len(rangeRate):
            raise Exception("len of rate must equal source")
        # 重算rate
        sum = reduce(lambda x, y: x+y, rangeRate)
        rate = list(map(lambda x: x/sum, rangeRate))
        x = random.random()
        cumulative = 0.0
        for item, p in zip(rangeArr, rate):
            cumulative += p
            if x < cumulative:
                break
        return self.random_number2(item[0], item[1], mode)
    # 根据指定rate参数决定是否返回s，不命中的情况如果没有指定other则返回s类型的空，否则返回other

    def maybe(self, s, rate=0.5, other=None):
        if (random.random() <= rate):
            return s
        else:
            if other != None:
                return other
            else:
                return type(s)()

    # 生成随机百分比，默认精度为None则随机保留0～4位，可取值范围0-4，默认类型为阿拉伯数。如果中文形式mode="cn",任意mode="any"
    def random_percent(self, acc=None, mode="an"):
        f1 = self.random_float(0.0000, 99.9999)
        if (acc == None):
            acc = self.random_int(0, 4)
        if acc == 0:
            f2 = round(f1)
        else:
            f2 = round(f1, acc)
        if (mode == "any"):
            mode = self.random_element(["an", "cn"])
        if (mode == "an"):
            return f"{f2}%"
        else:
            return f"百分之{self.num2cn(f2)}"

    # 生成随机分数字符串，默认分子1～50，默认分母1～100，默认类型阿拉伯数。分母必须大于分子。如果中文形式mode="cn",任意mode="any"
    def random_frac(self, fenzi=50, fenmu=100, mode="an"):
        assert(fenmu >= fenzi+1)
        f1 = self.random_int(1, fenzi)
        f2 = self.random_int(f1+1, fenmu)
        if (mode == "any"):
            mode = self.random_element(["an", "cn"])
        if (mode == "an"):
            return f"{f1}/{f2}"
        else:
            return f"{self.num2cn(f2)}分之{self.num2cn(f1)}"
    # 根据日期获得阴历对象

    def getLunar(self, dateStr):
        d1 = self._str2date(dateStr)
        return self.lunar.fromDate(d1)
    # 根据日期获得阳历对象

    def getSolar(self, dateStr):
        d1 = self._str2date(dateStr)
        return self.solar.fromDate(d1)

    # 将传入的data随机映射到随机的group组中，每组随机个item。其中group不超过groupCnt，每组item不超过itemcnt个。
    # groupRate指定group可能个数的分布，itemRate指定item可能个数的分布
    def randomMap(self, data, groupCnt=5, itemCnt=5, groupRate=[0.4, 0.4, 0.1, 0.07, 0.03], itemRate=[0.4, 0.4, 0.1, 0.07, 0.03]):
        if groupCnt == None:
            groupCnt = len(groupRate)
        if itemCnt == None:
            itemCnt = len(itemRate)
        assert(groupCnt == len(groupRate))
        assert(itemCnt == len(itemRate))
        m = self.random_element3(
            list(range(1, groupCnt+1)), groupRate)
        nameMap = {}
        for i in range(m):
            groupName = "group"+str(i)
            if len(data) == 0:
                break
            nameMap[groupName] = []
            n = self.random_element3(
                list(range(1, itemCnt+1)), itemRate)
            for j in range(n):
                try:
                    item = data.pop()
                    nameMap[groupName].append(item)
                except Exception as e:
                    break
        return nameMap

    def random_select(self, lst, lstRate, min=None, max=None):
        # 根据lst数组对应的lstRate数组选取可能的元素，返回一个新的数组。同时要确保返回的数组至少有min个数据，min为None则表示不限制最少值，因此可能返回空数组
        # lstRate对应为1的数据为一定被选取的数据，而为0的则一定不被选去
        # 如果min=None则最小出现的个数为rate为1的个数,如果min小于rate为1的个数则min也为rate为1的个数
        # 如果max=None则最大出现的个数为lst数组的个数
        assert(len(lst) == len(lstRate))
        choice = []
        rest = []
        choiceLast = []
        if min == None:
            min = 0
        if min > len(lst):
            min = len(lst)
        if max == None or max > len(lst):
            max = len(lst)
        for i, item in enumerate(lst):
            ok = self.maybe(True, lstRate[i], False)
            if ok:
                if lstRate[i] == 1:
                    choiceLast.append(item)
                else:
                    choice.append(item)
            else:
                rest.append(item)
        if len(choiceLast) > min:
            min = len(choiceLast)
        if min > max:
            max = min
        # 合并 choice
        choice.extend(rest)
        # 确保min
        minrest = min - len(choiceLast)
        if minrest > 0:
            choiceLast.extend(choice[0: minrest])
            start = minrest
        # 处理max
        maxrest = max - len(choiceLast)
        if maxrest > 0:
            choiceLast.extend(
                choice[minrest:minrest+self.random_int(0, maxrest)])
        # 乱序返回
        random.shuffle(choiceLast)
        return choiceLast

    def random_split(self, lst, groupNum, pos=None):
        # 根据lst数组随机分解到groupNum组中。结果是一个元素为groupNum的二维数组，每个元素是一个原数组lst的子集，可能为空数组。
        # pos是可选的二维数组，标识lst数组中每个对应的元素可以放置的位置。比如[[0,2],[1],[0,1,2]],表明第一个元素可以放在0，2组中，第二个元素只能放在1组中，第三个元素可以放在0，1，2中
        # pos如果指定，其长度应与lst长度一致;pos中元素如果为None或为空数组则表示不限定放在哪个组
        assert(groupNum >= 0)
        assert(pos == None or len(lst) == len(pos))
        groups = list(map(lambda x: [], range(groupNum)))
        for i, item in enumerate(lst):
            if pos == None or pos[i] == None or len(pos[i]) == 0:
                j = self.random_int(0, groupNum-1)
            else:
                j = self.random_element(pos[i])
            groups[j].append(item)
        return groups
    
    def decode(self,data,list1,list2):
        #将data的值从list1映射到list2
        assert(len(list1)==len(list2))
        for i,item in enumerate(list1):
            if data==item :
                return list2[i]
        return None
# 添加一个provider
faker.add_provider(MyProvider)


class Demo:
    def __init__(self, conf=None):
        self.version = "1.4.1"
        self.faker = faker
        self.xeger = Xeger().xeger
        self.lunar = lunar_python.Lunar
        self.solar = lunar_python.Solar
        self.iterator = {}
        self.iterator_run = {}
        self.resource = {}
        self.connector = {}
        self.stage = []
        self.config = {}
        self.keys = set()
        self.documents = []  # 给落盘回调使用
        self.docs = {}  # 代表每个stage的所有数据，包括_开头字段
        self.rows = []  # 代表本stage已生的数据，包括_开头的字段
        self.row = {}  # 代表本stage当前行数据，包括_开头的字段
        self.idx = 0
        self.debug = True
        self.autoconf(conf)
        self.unitConvert = {
            "kCal": {"kCal": 1, "kcal": 1, "Kcal": 1, "KJ": 4.18585},
            "千卡": {
                "千卡": 1,
                "大卡": 1,
                "千焦": 4.18585,
                "卡": 1000,
                "KJ": 4.18585,
                "kJ": 4.18585,
                "kcal": 1,
                "Kcal": 1,
                "kCal": 1,
            },
            "g": {"g": 1, "mg": 1000, "ug": 1000000},
            "mg": {"g": 0.001, "mg": 1, "ug": 1000},
            "ug": {"g": 0.000001, "mg": 0.001, "ug": 1, "μg": 1},
            "μg": {"g": 0.000001, "mg": 0.001, "ug": 1, "μg": 1},
            "克": {
                "克": 1,
                "毫克": 1000,
                "微克": 1000000,
                "匙": 10,
                "大匙": 15,
                "小匙": 5,
                "少许": 5,
                "适量": 10,
                "口": 15,
                "小口": 5,
                "大口": 15,
                "斤": 1 / 500,
                "公斤": 1 / 1000,
                "千克": 1 / 1000,
                "两": 1 / 50,
                "钱": 1 / 5
            },
            "ml": {"ml": 1, "l": 0.001},
            "公斤": {"公斤": 1, "斤": 2, "克": 1000},
            "毫升": {"毫升": 1, "升": 0.001, "两": 0.5, "斤": 0.05, "公斤": 0.001},
            "m": {"m": 1, "cm": 100, "mm": 1000, "km": 0.001},
            "厘米": {"厘米": 1, "米": 0.01},
            "米": {
                "米": 1,
                "厘米": 100,
                "毫米": 1000,
                "千米": 0.001,
                "公里": 0.001,
                "里": 0.002,
                "meter": 1,
                "cm": 100,
                "m": 1,
                "尺": 3,
                "寸": 30,
                "英尺": 3.281,
                "英寸": 39.37,
                "步": 1.667
            },
            "分钟": {
                "分钟": 1,
                "分": 1,
                "秒": 60,
                "秒钟": 60,
                "小时": 1 / 60,
                "钟头": 1 / 60,
                "钟": 1 / 60,
                "天": 1 / 60 / 24,
                "seconds": 60,
                "刻": 1 / 15,
                "刻钟": 1 / 15,
                "字": 1 / 5,
                "年": 1 / 60 / 24 / 365,
                "月": 1 / 60 / 24 / 30,
                "季度": 1 / 60 / 24 / 90,
                "周": 1 / 60 / 24 / 7,
                "星期": 1 / 60 / 24 / 7,
                "礼拜": 1 / 60 / 24 / 7,
            },
            "小时": {
                "分钟": 60,
                "秒": 3600,
                "小时": 1,
                "钟头": 1,
                "钟": 1,
                "分": 60,
                "秒钟": 3600,
                "天": 1 / 24,
                "刻": 15 / 60,
                "刻钟": 15 / 60,
                "字": 5 / 60
            },
            "元": {"元": 1, "块": 1, "角": 10, "毛": 10, "分钱": 100},
            "次/分钟": {"次/分钟": 1, "下/分钟": 1, "次": 1, "下": 1},
            "摄氏度": {"摄氏度": 1, "度": 1, "华氏度": 33.8},
            "毫摩尔/升": {"毫摩尔/升": 1, "毫摩尔每升": 1, "mmol/L": 1},
            "毫米汞柱": {"毫米汞柱": 1, "mmHg": 1}
        }

    def convert(self, baseUnit, fromValue, fromUnit, toUnit=None):
        if (toUnit == None):
            toUnit = baseUnit
        temp = self.unitConvert.get(baseUnit)
        if (temp.get(fromUnit) != None):
            if (toUnit == baseUnit):
                return fromValue / temp.get(fromUnit)
        else:
            if (temp.get(toUnit) != None):
                return fromValue / temp.get(fromUnit) * temp.get(toUnit)
            else:
                return None
        return None

    def autoconf(self, conf):
        if not conf:
            return
        if conf.get("connector"):
            for key, value in conf.get("connector").items():
                self.addConnector(key, value)
        if conf.get("resource"):
            for key, value in conf.get("resource").items():
                temp = value.get("data", "[]")
                if type(temp) == str:
                    try:
                        data = eval(temp, {"self": self,
                                    "R": self.resource, "S": self.stage,
                                           "F": self.faker, "X": self.xeger})
                    except Exception as e:
                        data = temp
                else:
                    data = temp
                self.addResource(key, data, url=value.get(
                    "url"), fetch=value.get("fetch"), file=value.get("file"))
        if conf.get("iterator"):
            for key, value in conf.get("iterator").items():
                temp = eval(value.get("data", "iter([])"), {"self": self,
                                                            "R": self.resource, "S": self.stage,
                                                            "F": self.faker, "X": self.xeger})
                try:
                    data = temp
                except Exception as e:
                    raise Exception(
                        f"设置iterator错误:{e.args},key=[{key}],temp=[{temp}]")
                self.addIterator(key, data, cycle=value.get(
                    "cycle", False), rowScope=value.get("rowScope", False))
        if conf.get("stage"):
            for key, value in conf.get("stage").items():
                self.addStage(key, rows=value.get("rows", 1), interval=value.get(
                    "interval"), total=value.get("total"), callback=value.get("callback"))
        if conf.get("config"):
            for key, values in conf.get("config").items():
                for value in values:
                    self.addConfig(key, value)

    def addConnector(self, type, arg):
        if type == "ES":
            self.connector["ES"] = Elasticsearch(arg, timeout=3600)
        if type == "MONGO":
            self.connector["MONGO"] = MongoClient(arg)
            print(self.connector["MONGO"]["food"])
        return self

    def addResource(self, name, data=None, url=None, fetch=None, file=None):
        try:
            value = None
            if data:
                value = data
            if url:
                res = requests.get(url)
                value = json.loads(res.text)
            if fetch:
                value = list(fetch)
            if file:
                try:
                    with open(file, 'r', encoding='utf8') as f:
                        code = f.read()
                    # 如果是可以转换为json的字串，则资源设置为这个json，否则把这个字串当作code来返回执行结果
                    value = json.loads(code)
                except Exception as e:
                    local_variable = {
                        "self": self,
                        "R": self.resource, "S": self.stage,
                        "X": self.xeger, "F": self.faker,
                    }
                    exec(code, {}, local_variable)
                    value = local_variable.get('__return__')
            self.resource.update({name: value})
            return self
        except Exception as e:
            raise Exception(f"配置resource错误:{e.args},name={name}")

    def addIterator(self, name, data, cycle=False, rowScope=False):
        # 根据cycle的类型，将每个iterator定义为一个迭代器和范围的元组
        if cycle:
            self.iterator.update({name: (itertools.cycle(data), rowScope)})
        else:
            self.iterator.update({name: (data, rowScope)})
        return self

    def next(self, key):
        try:
            if type(self.iterator_run[key]) == tuple:
                return self.iterator_run[key][self.idx].__next__()
            else:
                return self.iterator_run[key].__next__()
        except:
            return None

    def addStage(self, name, rows=1, interval=None, total=None, callback=None):
        self.stage.append({"name": name, "rows": rows,
                          "callback": callback, "interval": interval, "total": total})

    def addConfig(self, stage_name, arg):
        '''
        **  type:可以是text,keyword,date,array,geo_point三种类型
        **  func:faker下的各种生成函数，可以是_["<field>"]代表某个已经生成的filed的值，self.next("<resource>")代表某个resource的序列
        **  depend: 逗号分隔的字段名称
        **  list  : 生成的数据list数组内数据
        **
        '''
        if type(arg) == dict:
            if not arg.get("key"):
                raise Exception("must define key!")
            if len(self.stage) == 0:
                self.addStage({"name": stage_name, "rows": 1, "callback": None,
                              "args": None, "interval": None, "total": None})
            if not self.config.get(stage_name):
                self.config[stage_name] = []
            self.config[stage_name].append(arg)
            self.keys.add(arg.get("key"))
        return self

    def generate(self, repeat=1, retain=False, debug=True):
        '''
        row=self.rows
        docs=self.docs
        faker=self.faker
        connector=self.connector
        resource=self.resource
        iterator=self.iterator
        '''
        self.debug = debug
        for rp in range(repeat):
            debug and print("*"*20, "group", rp, "*"*20)
            for stage in self.stage:
                debug and print("【stage】", stage)
                if retain and (not self.docs.get(stage["name"])):
                    self.docs[stage["name"]] = []
                else:
                    self.docs[stage["name"]] = []
                if  stage["total"]==None:
                    if stage["interval"]==None:
                        total = 1
                    else:
                        total = 10
                else:
                    total = stage["total"]
                loop = 0
                stage["current"] = loop
                config1 = list(filter(lambda x: not x.get(
                    "depend"), self.config[stage["name"]]))
                config2 = list(filter(lambda x: set(x.get("depend", "").split(
                    ",")).issubset(self.keys), self.config[stage["name"]]))
                # print("c1:",config1)
                # print("c2:",config2)
                # self.iterator
                for key in self.iterator:
                    if self.iterator[key][1]:  # is row scopt
                        self.iterator_run[key] = itertools.tee(self.iterator[key][0], stage["rows"][1] if type(
                            stage["rows"]) in (list, tuple) else rows)
                    else:
                        self.iterator_run[key] = self.iterator[key][0]
                while loop < total:
                    loop += 1
                    stage["current"] = loop
                    rows = stage["rows"]
                    rows = random.randint(rows[0], rows[1]) if type(
                        rows) in (list, tuple) else rows
                    debug and print(
                        f"{stage['name']}-loop{loop}-t{total}-r{rows}", "*"*10, self.solar.fromDate(datetime.today()).toFullString())
                    if stage["interval"]:
                        interval = stage["interval"]
                        time.sleep(random.randint(interval[0], interval[1]) if type(
                            interval) in (list, tuple) else interval)

                    self.documents = []  # 代表本stage已生的数据，不包括_开头的字
                    self.rows = []
                    line_progress = LineProgress(
                        total=100, width=60, title=stage['name'])
                    for idx in range(rows):
                        self.row = _ = res = {}
                        self.idx = idx
                        line_progress.update((idx+1)/rows*100)
                        for item in config1:
                            # force
                            force = item.get("force", False)

                            # maybe
                            temp = item.get("maybe", "1")
                            if (type(temp) == bool):
                                maybe = 1 if temp else 0
                            elif (type(temp) == int or type(temp) == float):
                                maybe = temp
                            else:
                                try:
                                    maybe = eval(temp, {"self": self, "_": _,
                                                        "R": self.resource, "S": self.stage,
                                                        "F": self.faker, "X": self.xeger})
                                except Exception as e:
                                    raise Exception(
                                        f"执行maybe字句错误:{e.args},maybe=[{temp}]")
                            if (type(maybe) == bool):
                                maybe = 1 if maybe else 0
                            if (random.random() > maybe):
                                continue
                            # key是否为函数,如果为函数必须符合func=***的格式
                            if item["key"][0:5] == "func=":
                                temp = item["key"][5:]
                                try:
                                    key = str(eval(temp, {"self": self, "_": _,
                                                          "R": self.resource, "S": self.stage,
                                                          "F": self.faker, "X": self.xeger}))
                                except Exception as e:
                                    if (force):
                                        raise Exception(
                                            f"执行key:func字句错误:{e.args},args=[{temp}]")
                                    else:
                                        key = temp
                            else:
                                key = item["key"]
                            if item.get("type") == "array":
                                item["nums"] = item.get("nums", "[0,5]")
                                if type(item["nums"]) == str:
                                    try:
                                        item["nums"] = eval(item["nums"], {"self": self, "_": _,
                                                                           "R": self.resource, "S": self.stage,
                                                                           "F": self.faker, "X": self.xeger})
                                    except Exception as e:
                                        item["nums"] = f"执行type:array字句错误:{e.args},args=[{item['nums']}]"
                                if type(item.get("nums")) in [int, float]:
                                    item["nums"] = [0, int(item.get("nums"))]
                                if type(item.get("nums")) in [list] and len(item.get("nums")) == 1:
                                    item["nums"] = [
                                        0, int(item.get("nums")[0])]
                                times = self.faker.random_int(
                                    item["nums"][0], item["nums"][1])
                                if times != 0:
                                    res[key] = []
                                    for tick in range(times):
                                        try:
                                            res[key].append(eval(item["func"], {"self": self, "_": _, "tick": tick,
                                                                                "R": self.resource, "S": self.stage,
                                                                                "F": self.faker, "X": self.xeger}))
                                        except Exception as e:
                                            if (force):
                                                raise Exception(
                                                    f"执行func字句错误:{e.args},key=[{key}]args=[{item['func']}]")
                                            else:
                                                res[key].append(item['func'])
                                else:
                                    res[key] = []
                            else:
                                if item.get("list"):
                                    res[key] = self.faker.random_number(
                                        item["list"])
                                elif item.get("func"):
                                    try:
                                        # 测试eval带环境变量
                                        res[key] = eval(
                                            item["func"], {
                                                "self": self, "_": _,
                                                "R": self.resource, "S": self.stage,
                                                "F": self.faker, "X": self.xeger,
                                            })
                                    except Exception as e:
                                        if (force):
                                            raise Exception(
                                                f"执行func字句错误:{e.args},stage:[{stage['name']}],key:[{key}],args=[{item['func']}]")
                                        else:
                                            res[key] = item['func']
                                    # res[key]=self.execute(item["func"])
                                elif item.get("file"):
                                    local_variable = {
                                        "self": self, "_": _,
                                        "R": self.resource, "S": self.stage,
                                        "F": self.faker, "X": self.xeger,
                                    }
                                    try:
                                        if type(item["file"]) == str:
                                            with open(item["file"]) as f:
                                                item["file"] = compile(
                                                    f.read(), '', 'exec')
                                        exec(item["file"], {}, local_variable)
                                        res[key] = local_variable.get(
                                            '__return__')
                                    except Exception as e:
                                        if (force):
                                            raise Exception(
                                                f"执行file字句错误:{e.args},stage:[{stage['name']}],key:[{key}],args=[{item['file']}]")
                                        else:
                                            res[key] = item['file']
                        for item in config2:
                            # force
                            force = item.get("force", False)

                            # maybe
                            temp = item.get("maybe", "1")
                            if (type(temp) == bool):
                                maybe = 1 if temp else 0
                            elif (type(temp) == int or type(temp) == float):
                                maybe = temp
                            else:
                                try:
                                    maybe = eval(temp, {"self": self, "_": _,
                                                        "R": self.resource, "S": self.stage,
                                                        "F": self.faker, "X": self.xeger})
                                except Exception as e:
                                    raise Exception(
                                        f"执行maybe字句错误:{e.args},maybe=[{temp}]")
                            if (type(maybe) == bool):
                                maybe = 1 if maybe else 0
                            if (random.random() > maybe):
                                continue
                            # key是否为函数,如果为函数必须符合func=***的格式
                            if item["key"][0:5] == "func=":
                                try:
                                    key = str(eval(item["key"][5:], {"self": self, "_": _,
                                                                     "R": self.resource, "S": self.stage,
                                                                     "F": self.faker, "X": self.xeger}))
                                except Exception as e:
                                    if (force):
                                        raise Exception(
                                            f"执行key:func字句错误:{e.args},args=[{item['key']}]")
                                    else:
                                        key = item["key"][5:]
                            else:
                                key = item["key"]
                            if item.get("type") == "array":
                                item["nums"] = item.get("nums", "[0,5]")
                                if type(item["nums"]) == str:
                                    try:
                                        item["nums"] = eval(item["nums"], {"self": self, "_": _,
                                                                           "R": self.resource, "S": self.stage,
                                                                           "F": self.faker, "X": self.xeger})
                                    except Exception as e:
                                        item["nums"] = f"执行type:array字句错误:{e.args},args=[{item['nums']}]"
                                if type(item.get("nums")) in [int, float]:
                                    item["nums"] = [0, int(item.get("nums"))]
                                if type(item.get("nums")) in [list] and len(item.get("nums")) == 1:
                                    item["nums"] = [
                                        0, int(item.get("nums")[0])]
                                times = self.faker.random_int(
                                    item["nums"][0], item["nums"][1])
                                if times != 0:
                                    res[key] = []
                                    for tick in range(times):
                                        try:
                                            res[key].append(eval(item["func"], {"self": self, "_": _, "tick": tick,
                                                                                "R": self.resource, "S": self.stage,
                                                                                "F": self.faker, "X": self.xeger}))
                                        except Exception as e:
                                            if (force):
                                                raise Exception(
                                                    f"执行func字句错误:{e.args},stage:[{stage['name']}],key=[{key}],args=[{item['func']}]")
                                            else:
                                                res[key].append(item['func'])
                                else:
                                    res[key] = []
                            else:
                                if item.get("list"):
                                    res[key] = self.faker.random_number(
                                        item["list"])
                                elif item.get("func"):
                                    try:
                                        res[key] = eval(item["func"], {"self": self, "_": _,
                                                                       "R": self.resource, "S": self.stage,
                                                                       "F": self.faker, "X": self.xeger})
                                    except Exception as e:
                                        if (force):
                                            raise Exception(
                                                f"执行func字句错误:{e.args},stage:[{stage['name']}],key=[{key}],args=[{item['func']}]")
                                        else:
                                            res[key] = item['func']
                                    # res[key]=self.execute(item["func"])
                                elif item.get("file"):
                                    local_variable = {
                                        "self": self, "_": _,
                                        "R": self.resource, "S": self.stage,
                                        "F": self.faker, "X": self.xeger,
                                    }
                                    try:
                                        if type(item["file"]) == str:
                                            with open(item["file"]) as f:
                                                item["file"] = compile(
                                                    f.read(), '', 'exec')
                                        exec(item["file"], {}, local_variable)
                                        res[key] = local_variable.get(
                                            '__return__')
                                    except Exception as e:
                                        if (force):
                                            raise Exception(
                                                f"执行file字句错误:{e.args},stage:[{stage['name']}],key=[{key}],args=[{item['file']}]")
                                        else:
                                            res[key] = item['file']
                        self.rows.append(res)
                        trueres = {}
                        list(
                            map(lambda x: None if x[0] == "_" else trueres.update({x: res[x]}), res))
                        self.documents.append(trueres)
                        self.docs[stage["name"]].append(res)
                    debug and print(f"\nhad generated {len(self.documents)} documents!",
                                    "*"*10, self.solar.fromDate(datetime.today()).toFullString())
                    print(stage['callback'])
                    stage["callback"] and eval(stage['callback'], {"self": self,
                                                                   "R": self.resource, "S": self.stage,
                                                                   "F": self.faker, "X": self.xeger})

    def show(self, stage, rows=10, showAll=False, indent=False):
        docs = self.docs[stage][0:rows]
        # print(docs)
        if not showAll:
            shouldShow = list(map(lambda y: y['key'], list(filter(lambda x: x.get('key')[0] != "_" or x.get('show')
                                                                  == True, self.config[stage]))))
            # print("=======>", shouldShow)
            temp = map(lambda x: list(
                filter(lambda y: y[0] in shouldShow, x.items())), docs)
            p = []
            for item in temp:
                q = {}
                list(map(lambda x: q.update({x[0]: x[1]}), item))
                p.append(q)
            if indent:
                print(json.dumps(p, indent=4, ensure_ascii=False))
            else:
                print(p)
        else:
            if indent:
                print(json.dumps(docs, indent=4, ensure_ascii=False))
            else:
                print(docs)

    def getTimestamp(self):
        return int(time.time()*1000)

    def merge(self, *a):
        b = {}
        for i in a:
            b.update(i),
        return b

    def mark_entity(self, a, b, mode="json"):
        # a数组是要组成的实体序列，b数据是标注的实体名称。
        # mode默认为json，系统将返回实体标注的结构。mode=normal则返回entity的逐字格式,mode=text则返回逐字的文本格式,mode=study则为全句标注模式
        assert(len(a) == len(b))
        s = ""
        entities = []
        if (mode == "json"):
            for i, item in enumerate(a):
                item = str(item)
                start = len(s)
                end = start+len(item)
                entities.append({"start": start, "end": end,
                                "entity": b[i], "value": item})
                s += item
            return {"text": s, "entities": list(filter(lambda x: x["value"] != "" and x["entity"][0] != "_", entities))}
        elif (mode == "normal"):
            for i, item in enumerate(a):
                item = str(item)
                s += item
                for j, char in enumerate(item):
                    if b[i][0] != "_":
                        if j == 0:
                            entities.append(f"{char} B-{b[i]}")
                        else:
                            entities.append(f"{char} I-{b[i]}")
                    else:
                        entities.append(f"{char} O")
            return {"text": s, "entities": entities}
        elif (mode == "txt"):
            for i, item in enumerate(a):
                item = str(item)
                s += item
                for j, char in enumerate(item):
                    if b[i][0] != "_":
                        if j == 0:
                            entities.append(f"{char} B-{b[i]}")
                        else:
                            entities.append(f"{char} I-{b[i]}")
                    else:
                        entities.append(f"{char} O")
            return {"text": s, "entities": "\n".join(entities)+"\n\n"}
        else:
            for i, item in enumerate(a):
                item = str(item)
                s += item
                if b[i][0] == "_" or item == "":
                    entities.append(f"{item}")
                else:
                    entities.append("{"+f"{item}::{b[i]}"+"}")
            return {"text": s, "entities": "".join(entities)+"\n"}

    def httpGet(self, url):
        res = requests.get(url)
        if res.status_code == 200:
            resJson = json.loads(res.text)
            return resJson
        else:
            return None

    def httpPost(self, url, json):
        res = requests.pos(url, json=json)
        if res.status_code == 200:
            return res.text
        else:
            return None

    def saveByAPI(self, stage_name, url):
        # https://qjz.qjz.world:18088/api/foodLib/foodLibQueryV2?name=苹果&num=1&unit=个&pageNo=1&pageSize=2
        # https://qjz.qjz.world:18088/api/setmeal/getRandomSetmeallist?range=1
        # https://qjz.qjz.world:18088/api/qjzdata/listQuery?account=82u3rn2u3r2n3ur9&type=diet&subtype=food&dateOrder=-1
        documents = self.documents
        for item in iter(documents):
            res = requests.post(url, json=item)
            if (res.status_code != 200):
                self.debug and print(res.text, "===>", item)
            else:
                self.debug and print("ok", "===>", item)

    def saveFile(self, fileName, fieldName=None):
        with open(fileName, 'w', encoding="utf8") as f:
            if (fieldName != None):
                # f.writelines(map(lambda x: x[fieldName], self.documents))
                f.writelines(map(lambda x: x[fieldName] if type(x[fieldName]) == str else json.dumps(
                    x[fieldName], ensure_ascii=False, indent=4), self.documents))
            else:
                f.write(json.dumps(self.documents, ensure_ascii=False, indent=4))
            self.debug and print(f"已保存至文件:{fileName}")

    def saveES(self, stage_name, args):
        print(stage_name, args)
        table = args.get("table")
        documents = args.get("documents", None)
        refresh = args.get("refresh", False)
        if not documents:
            documents = self.documents
        index = table.split("/")[0]
        es = self.connector["ES"]
        has = es.indices.exists(index)
        if not has:
            refresh = True
        if refresh:
            if has:
                es.indices.delete(index)
            mappings = {
                "mappings": {
                    "properties": {
                    }
                }
            }
            list(map(lambda x: None if x.get("type") not in ["text", "keyword", "date", "integer", "float", "geo_point"] or x.get(
                "key", " ")[0] == "_" else mappings["mappings"]["properties"].update({x["key"]: {"type": x["type"]}}), self.config[stage_name]))
            es.indices.create(index=index, body=mappings)

        data = map(lambda x: {"_index": index, "_source": x}, documents)
        bulk(es, list(data))
    # 存储到mongo数据库

    def saveMONGO(self, stage_name, args):
        table = args.get("table")
        documents = args.get("documents", None)
        refresh = args.get("refresh", False)
        if not documents:
            documents = self.documents
        database = table.split("/")[0]
        collection = table.split("/")[1]
        db = self.connector["MONGO"][database]
        if refresh:
            db[collection].drop()

        db[collection].insert_many(documents)


# post中的body是一个demoData规范的配置json

# @app.route('/test', methods=['POST'])
# def test():
#     conf = request.get_json()
#     print(conf)
#     return jsonify('ok')


@app.route('/generate', methods=['POST'])
def generate():
    conf = request.get_json()
    demo = Demo(conf)
    demo.generate()
    return jsonify(demo.docs)
    # return json.dumps(demo.docs,indent=4,ensure_ascii=False)

# just a get test


@app.route('/sample1/config', methods=['GET'])
def sample1_config():
    # conf  = request.get_json()
    conf = {
        "stage": {
            "stage1": {"rows": [5, 10]}
        },
        "config": {
            "stage1": [
                {"key": "id", "func": "self.faker.pystr(12,12)"},
                {"key": "name", "func": "self.faker.cname()"},
                {"key": "grade", "func": "self.faker.random_int(1,8)"},
                {"key": "score",
                    "func": "self.faker.random_float(40.01,99.99)"},
                {"key": "num",
                 "func": "self.faker.random_float_range([[0.01,49.99],[50.01,99.99]],[0.2,0.8])"}
            ]
        }
    }
    # return jsonify(conf)
    return "<pre>"+json.dumps(conf, indent=4, ensure_ascii=False)+"</pre>"


@app.route('/sample1/generate', methods=['GET'])
def sample1_generate():
    # conf  = request.get_json()
    conf = {
        "stage": {
            "stage1": {"rows": [5, 10]}
        },
        "config": {
            "stage1": [
                {"key": "id", "func": "self.faker.pystr(12,12)"},
                {"key": "name", "func": "self.faker.cname()"},
                {"key": "grade", "func": "self.faker.random_int(1,8)"},
                {"key": "score",
                    "func": "self.faker.random_float(40.01,99.99)"},
                {"key": "num",
                 "func": "self.faker.random_float_range([[0.01,49.99],[50.01,99.99]],[0.2,0.8])"}
            ]
        }
    }
    demo = Demo(conf)
    demo.generate()
    # return jsonify(demo.docs)
    return "<pre>"+json.dumps(demo.docs, indent=4, ensure_ascii=False)+"</pre>"


if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], encoding="utf8") as f:
            conf = json.load(f)
            # print(conf)
            demo = Demo(conf)
            demo.generate(1)
            print("demoData generate done!")
    else:
        print(
            "you can also use Command Line,the usage is : python3 demoData.py <config.json>")
        app.run(host="0.0.0.0", port=5000)
